<?php
/**
 * xemlab functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package xemlab
 */


// Remove all default WP template redirects/lookups
remove_action('template_redirect', 'redirect_canonical');

// Redirect all requests to index.php so the Vue app is loaded and 404s aren't thrown
function remove_redirects() {
    add_rewrite_rule('^/(.+)/?', 'index.php', 'top');
}
add_action('init', 'remove_redirects');

if ( ! function_exists( 'xemlab_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */


    function rt_rest_theme_scripts() {

        wp_enqueue_style( 'style', get_stylesheet_uri() );

        $base_url  = esc_url_raw( home_url() );
        $base_path = rtrim( parse_url( $base_url, PHP_URL_PATH ), '/' );

        if ( defined( 'RT_VUE_DEV' ) && RT_VUE_DEV ) {

            //wp_enqueue_script( 'rest-theme-vue', 'http://localhost:8080/dist/scripts/app.js', array( 'jquery' ), '1.0.0', true );

        } else {

           // wp_enqueue_script( 'rest-theme-vue', get_template_directory_uri() . '/dist/scripts/app.js', array( 'jquery' ), '1.0.0', true );

        }

        wp_localize_script( 'rest-theme-vue', 'rtwp', array(
            'root'      => esc_url_raw( rest_url() ),
            'base_url'  => $base_url,
            'base_path' => $base_path ? $base_path . '/' : '/',
            'nonce'     => wp_create_nonce( 'wp_rest' ),
            'site_name' => get_bloginfo( 'name' ),
        ) );
    }

    add_action( 'wp_enqueue_scripts', 'rt_rest_theme_scripts' );









	function xemlab_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on xemlab, use a find and replace
		 * to change 'xemlab' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'xemlab', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		//add_theme_support( 'automatic-feed-links' );


        add_filter('show_admin_bar', '__return_false');
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
        remove_action( 'wp_head', 'wp_oembed_add_host_js' );
        remove_action('wp_head', 'wp_generator');
        remove_action( 'wp_head', 'wlwmanifest_link' );
        remove_action( 'wp_head', 'rsd_link' );
        remove_action('wp_head','adjacent_posts_rel_link_wp_head');

        add_theme_support('category-thumbnails');


        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'top' => esc_html__( 'Primary', 'xemlab' ),
			'cat' => esc_html__( 'Product_cat', 'cat' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'xemlab_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'xemlab_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function xemlab_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'xemlab_content_width', 640 );
}
add_action( 'after_setup_theme', 'xemlab_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function xemlab_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'xemlab' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'xemlab' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'xemlab_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function xemlab_scripts() {

    // TODO all styles compiled



//	wp_enqueue_style( 'xemlab-style',  get_template_directory_uri().'/dist/styles/app.css');


	/*
	 *
	 */

	//wp_enqueue_script( 'xemlab-jquery',  '/assets/js/jquery.min.js', array(), '3.2.1', true );


	// TODO all scripts




	//wp_enqueue_script( 'xemlab-app',  get_template_directory_uri() . '/dist/scripts/app.js', array(), '0.0.1', true );




	//
//	wp_enqueue_script( 'xemlab-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
//
//	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
//		wp_enqueue_script( 'comment-reply' );
//	}





}
add_action( 'wp_enqueue_scripts', 'xemlab_scripts' );

function load_vue_scripts() {
    wp_enqueue_style( 'xemlab-bootstrap',  get_template_directory_uri(). '/dist/styles/bootstrap/bootstrap.min.css');
    wp_enqueue_style('blankslate/app.css', get_template_directory_uri() . '/dist/styles/app.css', false, null);
    wp_enqueue_script('blankslate/manifest.js', get_template_directory_uri() . '/dist/scripts/manifest.js', null, null, true);
    wp_enqueue_script('blankslate/vendor.js', get_template_directory_uri() . '/dist/scripts/vendor.js', null, null, true);
    wp_enqueue_script('blankslate/app.js', get_template_directory_uri() . '/dist/scripts/app.js', null, null, true);
}
add_action('wp_enqueue_scripts', 'load_vue_scripts', 100);

add_filter('show_admin_bar', '__return_false');




/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



function mode_theme_update_mini_cart() {
    echo wc_get_template( 'shop/mini-cart.php' );
    die();
}
add_filter( 'wp_ajax_nopriv_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
add_filter( 'wp_ajax_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );

/*function initBlogTaxonomy()
{
    register_taxonomy(
        'blog',
        'article',
        array(
            'labels' => array(
                'name' => "Категорії Блогу",
                'add_new_item' => "Добавити статю",
                'new_item_name' => "Добавити нова стаття"
            ),
            'show_ui' => true,
            'show_tagcloud' => true,
            'hierarchical' => true,
            'rewrite'               => array( 'slug' => 'blogs' ),
            'show_in_rest'          => true,
            'rest_base'             => 'blogs',
            'rest_controller_class' => 'WP_REST_Terms_Controller',
        )
    );
    register_post_type('article',
        array(
            'labels' => array(
                'name' => "Блог",
                'singular_name' => "Статті",
                'add_new' => 'Добавити статю',
                'add_new_item' => "Добавити нову статтю",
                'edit' => "Редагувати",
                'edit_item' => 'Редактировать статтю',
                'new_item' => 'Новиа стаття',
                'view' => 'Дивитися',
                'view_item' => 'Дивитися статті',
                'search_items' => 'Пошук статтів',
                'not_found' => 'Обзор статтів',
                'not_found_in_trash' => 'Немає статтів',
                'parent' => 'Батьківська стаття'
            ),
            'public' => true,
            'menu_position' => 7,
            'supports' => array('title', 'editor', 'author', 'comments', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields'),
            'taxonomies' => array('blog'),
            'menu_icon' => null,
            'capability_type'    => 'post',
            'taxonomies'  => array( 'blog'),
            'has_archive' => true,
            'rewrite'            => array( 'slug' => 'article' ),
            'show_in_rest'       => true,
            'rest_base'          => 'article',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
        )
    );
    flush_rewrite_rules();
}

add_action('init', 'initBlogTaxonomy');
function get_post_views($post_id){
    global $wpdb;
    return $wpdb->get_row("SELECT count FROM wp_post_views WHERE id = $post_id
    AND period='total'");
}

function prepare_rest($data, $post, $request){
    $_data = $data->data;

    $thumbnail_id = get_post_thumbnail_id($post->ID);

    $thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
    $thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
    $thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');

    $_data['images']['medium'] = $thumbnail_medium[0];
    $_data['images']['large'] = $thumbnail_large[0];
    $_data['images']['thumbnail'] = $thumbnail_thumbnail[0];
    $blog_name = wp_get_object_terms($post->ID, 'blog');
    $_data['blog_name'] = $blog_name[0]->name;
    $filds = get_post_custom($post->ID);
    $_data['filds'] = $filds;
    $autorb = ( isset($filds['article_author'][0]) && trim($filds['article_author'][0])) ? $filds['article_author'][0] : 'Адміністратор';

    $_data['autorb'] = $autorb;
    $post_views = get_post_views($post->ID);

    $_data['post_views'] = (int) $post_views->count;

    $comments_count = wp_count_comments($post->ID);
    $_data['count_comments'] = (int) $comments_count->total_comments;

    $data->data = $_data;

    return $data;
}
add_filter('rest_prepare_post', 'prepare_rest',10, 3);
add_filter('rest_prepare_article', 'prepare_rest',10, 3);
add_filter('rest_prepare_vacance', 'prepare_rest',10, 3);*/



function get_post_views($post_id){
    global $wpdb;
    return $wpdb->get_row("SELECT count FROM wp_post_views WHERE id = $post_id
    AND period='total'");
}

function prepare_rest($data, $post, $request){
    $_data = $data->data;

    $thumbnail_id = get_post_thumbnail_id($post->ID);

    $thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
    $thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
    $thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');

    $_data['images']['medium'] = $thumbnail_medium[0];
    $_data['images']['large'] = $thumbnail_large[0];
    $_data['images']['thumbnail'] = $thumbnail_thumbnail[0];
    $blog_name = wp_get_object_terms($post->ID, 'category');
    $_data['blog_name'] = $blog_name[0]->name;
    $filds = get_post_custom($post->ID);
    $_data['filds'] = $filds;
    $autorb = ( isset($filds['article_author'][0]) && trim($filds['article_author'][0])) ? $filds['article_author'][0] : 'Адміністратор';

    $_data['autorb'] = $autorb;
    $post_views = get_post_views($post->ID);

    $_data['post_views'] = (int) $post_views->count;

    $comments_count = wp_count_comments($post->ID);
    $_data['count_comments'] = (int) $comments_count->total_comments;

    $data->data = $_data;

    return $data;
}
add_filter('rest_prepare_post', 'prepare_rest',10, 3);