<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package xemlab
 */
// Вася  Лунь
$custom_logo_id = get_theme_mod( 'custom_logo' );
$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
//echo $image[0];
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head(); ?>
    <script>
        var siteData = {
            logo: '<?=$image[0]?>'
        }
    </script>
</head>
<body>
<div id="app" class="site">
    <app-Loader loadingProgress="0"></app-Loader>
    <app-Header></app-Header>
    <div id="contentw" class="site-contentw">





