import api from '../../api'
import * as types from '../mutation-types'

// initial state
const state = {
    added: [],

    loaded: false
}

// getters
const getters = {
    allProducts: state => state.all, // would need action/mutation if data fetched async
    getNumberOfProducts: state => (state.added) ? state.added.length : 0,
    cartProducts: state => {
        return state.added
    }
}

// actions
const actions = {
    addToCart({ commit }, product){
        commit(types.ADD_TO_CART, {
            product: product
        })
    },


    setCart({ commit }, products){
        commit(types.SET_CART, {
            products: products
        })
    },




}

// mutations
const mutations = {

    [types.ADD_TO_CART] (state, { product }) {
        let i;
        const record = state.added.find((p, index) => {
            if (p.id === product.id ) {
                i = index;
                return true;
            }
        })

        if (!record) {
            state.added.push(product)
        } else {
            state.added.splice(i, 1)
        }


    },

    [types.SET_CART] (state, { products }) {
        //SET_CART
        state.added = products;

    }
}
export default {
    state,
    getters,
    actions,
    mutations
}
