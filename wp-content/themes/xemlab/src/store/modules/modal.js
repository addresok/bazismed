import api from '../../api'
import * as types from '../mutation-types'

// initial state
const state = {
    show: false
}

// getters
const getters = {

    modalShow: state => {
        return state.show
    }
}

// actions
const actions = {



    setShow({ commit }, show){
        commit(types.SET_MODAL_SHOW, {
            show: show
        })
    },



}

// mutations
const mutations = {



    [types.SET_MODAL_SHOW] (state, { show }) {

        state.show = show;

    }
}
export default {
    state,
    getters,
    actions,
    mutations
}
