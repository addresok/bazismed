import Vue from 'vue'
import Vuex from 'vuex'
import createPersist from 'vuex-localstorage'
import * as actions from './actions'
import * as getters from './getters'
import hub from './modules/hub'
import user from './modules/user'
import post from './modules/post'
import page from './modules/page'
import categories from './modules/categories'
import modal from './modules/modal'
import cart from './modules/cart'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

let localStorage = createPersist({
    namespace: 'XEMLAB_APP_NAMESPACE',
    initialState: {},
    expires: 1.21e+9 // Two Weeks
})

export default new Vuex.Store({
  actions,
  getters,
  modules: {
    hub,
    user,
    post,
    page,
      modal,
      cart,
    categories
  },
  strict: debug,
  plugins: [],

    mutations: {
        rtChangeTitle( state, value ) {
            // mutate state
            state.title = value;
            //document.title = ( state.title ? state.title + ' - ' : '' ) + rtwp.site_name;
            document.title = ( state.title ? state.title : '' );
        }
    }

})
