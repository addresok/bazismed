
window.SETTINGS = {
    // How many different dispatched actions determine loading progress
    // This is likely determined by how many dispatched actions you have below
    // in the created() method
    LOADING_SEGMENTS: 2,
    API_BASE_PATH: '/wp-json/wp/v2/'
}

require('./bootstrap')

import 'babel-polyfill'
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLazyload from 'vue-lazyload'

// import BootstrapVue from 'bootstrap-vue'
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'

import router from './router'

var VueResource = require('vue-resource');

Vue.use(VueResource);

const moment = require('moment')
const resizeMixin = require('vue-resize-mixin');
require('moment/locale/uk')

Vue.use(require('vue-moment'), {
    moment
})

import VueSession from 'vue-session'
Vue.use(VueSession)


var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);

// import { auth, database } from './firebase.config.js' //- Uncomment if you need firebase

import App from './App.vue'

import store from './store/index'
import * as types from './store/mutation-types'



import Header from './components/partials/Header'
import HeaderFxed from './components/partials/HeaderFxed'
import HeaderMobile from './components/partials/HeaderMobile'
import Preloader from './components/partials/Preloader'
import Footer from './components/partials/Footer'
import FooterMobile from './components/partials/FooterMobile'
import OurForm from './components/partials/OurForm'




import Loader from './components/partials/Loader'
import MiniCart from './components/partials/MiniCart'
import FormMini from './components/partials/FormMini'
import Modal from './components/partials/Modal'
import Kl from './components/partials/Kl'

Vue.component('appHeader', Header);
Vue.component('appHeaderFxed', HeaderFxed);
Vue.component('appHeaderMobile', HeaderMobile);
Vue.component('appPreloader', Preloader);
Vue.component('appFooter' ,Footer);
Vue.component('appFooterMobile' ,FooterMobile);
Vue.component('appLoader' ,Loader);
Vue.component('appMiniCart' ,MiniCart);
Vue.component('appFormMini' ,FormMini);
Vue.component('appModal' ,Modal);
Vue.component('appKl' ,Kl);
Vue.component('appOurForm' ,OurForm);

import Vuetify from 'vuetify'

Vue.use(Vuetify)

Vue.use(VueLazyload)
//Vue.use(BootstrapVue)

router.afterEach((to, from) => {
    // Add a body class specific to the route we're viewing
    $("body").removeClass (function (index, className) {
        return (className.match (/(^|\s)vue--page--\S+/g) || []).join(' ');
    });
    $("body").addClass("vue--page--"+_.toLower(to.name))
})



import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBvWE_sIwKbWkiuJQOf8gSk9qzpO96fhfY',
        libraries: 'places,drawing,visualization', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)
    }
})



Vue.directive('mask', function (maskval) {
    $(this.el).mask(maskval);
});

Vue.filter('phoneformat', function (phone) {
    return phone.replace(/[^0-9]/g, '')
        .replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
});
Vue.filter('two_digits', function (value) {
    if(value.toString().length <= 1)
    {
        return "0"+value.toString();
    }
    return value.toString();
});

Vue.filter('json', function (value)  {
    return JSON.stringify(value, null, 2)
});
Vue.filter('formatDate', function (val) {
    var date = new Date(val);
    var diff = new Date() - date;
    if (diff < 1000) {
        return 'щойно';
    }

    var sec = Math.floor(diff / 1000);
    if (sec < 60) {
        return sec + ' сек. назад';
    }

    var min = Math.floor(diff / 60000);
    if (min < 60) {
        return min + ' хвилин тому';
    }

    var chas = Math.floor(diff / 3600000);
    if (chas < 24) {
        return chas + ' годин тому';
    }

    var dey = Math.floor((diff / 86400000));
    if (dey < 5) {
        return dey + ' дня тому';
    }

    var d = date;
    d = [
        '0' + d.getDate(),
        '0' + (d.getMonth() + 1),
        '' + d.getFullYear(),
        '0' + d.getHours(),
        '0' + d.getMinutes()
    ];

    for (var i = 0; i < d.length; i++) {
        d[i] = d[i].slice(-2);
    }

    return d.slice(0, 3).join('.') + ' ' + d.slice(3).join(':');
});




new Vue({
    el: '#app',

    data() {
        return {
          pageY: 0,
          maxPageY: 49,
          siteData: siteData
        }
    },
    store,
    router,
    render: h => h(App),
    created () {
        this.$store.commit(types.RESET_LOADING_PROGRESS)
        this.$store.dispatch('getAllCategories')
        this.$store.dispatch('getAllPages')

        // Once user is signed in/out, uncomment if you need Firebase authentication
        // auth.onAuthStateChanged(user => {
        //   if (user) {
        //     this.$store.commit(types.LOGIN_USER)
        //     this.$store.commit(types.STORE_FETCHED_USER, user)
        //   } else {
        //     this.$store.commit(types.LOGOUT_USER)
        //   }
        // })
    },


        methods: {

/*
        handleScroll(event) {


            if (event){
                this.pageY = window.scrollY;
                    if(this.pageY >= this.maxPageY){
                        $("body").addClass('fixed');
                    } else {
                       if( (this.pageY - 300) == this.maxPageY )  $('html, body').animate({
                           scrollTop: 0
                       }, 500);
                        $("body").removeClass('fixed');
                    }
            } else {
                // this.pageY = 0,
                  console.log(111);
            }
        },*/
    },






});



