import Vue from 'vue'
import Router from 'vue-router'

// Components
import Home from '../components/Home'
import PricesCategories from '../components/pages/prices/Categories'
import PricesProduct from '../components/pages/prices/Product'
import Doctors from '../components/pages/prices/Doctors'
import Doctor from '../components/pages/prices/Doctor'
import Complex from '../components/pages/prices/Complex'
import Compl from '../components/pages/prices/Compl'
import Sale from '../components/pages/Sale'
import Contact from '../components/pages/Contact'
import About from '../components/pages/About'
import Services from '../components/pages/Services'
import Size from '../components/pages/Size'
import Blog from '../components/pages/blog/Blog'
import Post from '../components/pages/blog/Post'
import Page from '../components/pages/Page'
import Checkout from '../components/pages/Checkout'
import Vacanc from '../components/pages/Vacanc'

Vue.use(Router)




const router = new Router({

  routes: [
      {
        path: '/',
        name: 'Home',
        component: Home,
        // props: { pageContentID: 383 }
      },
      {
        path: '/prices',
        name: 'Categories',
        component: PricesCategories,
          props: true
      },
      {
        path: '/prices/:slug',
        name: 'Category',
        component: PricesCategories,
          props: true
      },
      {
        path: '/price/:priceSlug',
        name: 'Product',
        component: PricesProduct,
          props: true
      },
      {
          path: '/doctor',
          name: 'Doctors',
          component: Doctors,
          props: false
      },
      {
          path: '/vacance',
          name: 'Vacances',
          component: Vacanc,
          props: false
      },
      {
          path: '/complex',
          name: 'Complex',
          component: Complex,
          props: false
      },
      {
        path: '/complex/:complexSlug',
        name: 'Compl',
        component: Compl,
          props: true
      },
      {
        path: '/vacance/:vacanceSlug',
        name: 'Vacanc',
        component: Vacanc,
          props: true
      },
      {
        path: '/doctor/:doctorSlug',
        name: 'Doctor',
        component: Doctor,
          props: true
      },
      {
        path: '/sale',
        name: 'Sale',
        component: Sale,
        // props: { pageContentID: 383 }
      },
      {
        path: '/contact',
        name: 'Contact',
        component: Contact,
        // props: { pageContentID: 383 }
      },
      {
          path: '/checkout',
          name: 'Checkout',
          component: Checkout,
          props: { slug: false }
      },
      {
        path: '/about-us',
        name: 'About Us',
        component: About,
        // props: { pageContentID: 383 }
      },
      {
        path: '/services',
        name: 'Services',
        component: Services,
        // props: { pageContentID: 383 }
      },
      {
        path: '/size',
        name: 'Size selection',
        component: Size,
        // props: { pageContentID: 383 }
      },
      {
        path: '/blog',
        name: 'Blog',
        component: Blog,
        props: { slug: false }
      },
      {
        path: '/blog/:slug',
        name: 'Blog Category',
        component: Blog,
        props: true

      },
      {
        path: '/post/:slug',
        name: 'Blog Post',
        component: Post,
        props: true

      },
      {
          path: '/page/:name',
          name: 'page',
          component: Page
      },



  ],
  mode: 'history',
  base: '',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
        console.log(savedPosition)
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },


})

// console.log(router)

export default router


