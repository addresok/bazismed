<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package xemlab
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function xemlab_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'xemlab_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function xemlab_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'xemlab_pingback_header' );





function prepare_rest_post($data, $post, $request){
    $_data = $data->data;

    $thumbnail_id = get_post_thumbnail_id($post->ID);

    $thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
    $thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
    $thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');

    $_data['images']['medium'] = $thumbnail_medium[0];
    $_data['images']['large'] = $thumbnail_large[0];
    $_data['images']['thumbnail'] = $thumbnail_thumbnail[0];




    $data->data = $_data;

    return $data;
}
add_filter('rest_prepare_post', 'prepare_rest_post',10, 3);





function rt_custom_rewrite_rule() {

    global $wp_rewrite;

    $wp_rewrite->front               = $wp_rewrite->root;

    //$wp_rewrite->set_permalink_structure( 'blog/%postname%/' );

    $wp_rewrite->page_structure      = $wp_rewrite->root . '/%pagename%';

    $wp_rewrite->author_base         = 'author';
    $wp_rewrite->author_structure    = '/' . $wp_rewrite->author_base . '/%author%';

    $wp_rewrite->set_category_base( 'category' );
    $wp_rewrite->set_tag_base( 'tag' );

    $wp_rewrite->add_rule( '^blog$', 'index.php', 'top' );

}
add_action( 'init', 'rt_custom_rewrite_rule' );

//Forcing permalink structure
add_action( 'permalink_structure_changed', 'rt_forcee_perma_struct', 10, 2 );

function rt_forcee_perma_struct( $old, $new ) {

    //update_option( 'permalink_structure', 'blog/%postname%' );

}
// Polyfill for wp_title()
add_filter( 'wp_title','rt_vue_title', 10, 3 );

function rt_vue_title( $title, $sep, $seplocation ) {

    if ( false !== strpos( $title, __( 'Page not found' ) ) ) {

        $replacement = ucwords( str_replace( '/', ' ', $_SERVER['REQUEST_URI'] ) );
        $title       = str_replace( __( 'Page not found' ), $replacement, $title );

    }

    return $title;
}

// Extend rest response
add_action( 'rest_api_init', 'rt_extend_rest_post_response' );

function rt_extend_rest_post_response() {

    // Add featured image
    register_rest_field( 'post',
        'featured_image_src', //NAME OF THE NEW FIELD TO BE ADDED - you can call this anything
        array(
            'get_callback'    => 'get_image_src',
            'update_callback' => null,
            'schema'          => null,
        )
    );

    register_rest_field( 'post',
        'cat_name', //NAME OF THE NEW FIELD TO BE ADDED - you can call this anything
        array(
            'get_callback'    => 'rt_get_cat_name',
            'update_callback' => null,
            'schema'          => null,
        )
    );

    register_rest_field( 'post',
        'tag_name',
        array(
            'get_callback'    => 'rt_get_tag_name',
            'update_callback' => null,
            'schema'          => null,
        )
    );

}
// Get featured image
function get_image_src( $object, $field_name, $request ) {

    $feat_img_array['full'] = wp_get_attachment_image_src( $object['featured_media'], 'full', false );
    $feat_img_array['thumbnail'] = wp_get_attachment_image_src( $object['featured_media'], 'thumbnail', false );
    $feat_img_array['srcset'] = wp_get_attachment_image_srcset( $object['featured_media'] );
    $image = is_array( $feat_img_array ) ? $feat_img_array : 'false';
    return $image;

}

function rt_get_cat_name( $object, $field_name, $request ) {

    $cats = $object['categories'];
    $res = [];
    $ob = [];
    foreach ( $cats as $x ) {
        $cat_id = (int) $x;
        $cat = get_category( $cat_id );
        if ( is_wp_error( $cat ) ) {
            $res[] = '';
        } else {
            $ob['name'] = isset( $cat->name ) ? $cat->name : '';
            $ob['id']   = isset( $cat->term_id ) ? $cat->term_id : '';
            $ob['slug'] = isset( $cat->slug ) ? $cat->slug : '';
            $res[] = $ob;
        }
    }
    return $res;

}

function rt_get_tag_name( $object, $field_name, $request ) {

    $tags = $object['tags'];
    $res = [];
    $ob = [];

    foreach ( $tags as $x ) {
        $tag_id = (int) $x;
        $tag = get_tag( $tag_id );
        if ( is_wp_error( $tag ) ) {
            $res[] = '';
        } else {
            $ob['name'] = isset( $tag->name ) ? $tag->name : '';
            $ob['id'] = isset( $tag->term_id ) ? $tag->term_id : '';
            $ob['slug'] = isset( $tag->slug ) ? $tag->slug : '';
            $res[] = $ob;
        }
    }
    return $res;
}







//
//function prepare_rest_product($data, $post, $request){
//    $_data = $data->data;
//
//    $thumbnail_id = get_post_thumbnail_id($post->ID);
//
//    $thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
//    $thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
//    $thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');
//
//    $_data['images']['medium'] = $thumbnail_medium[0];
//    $_data['images']['large'] = $thumbnail_large[0];
//    $_data['images']['thumbnail'] = $thumbnail_thumbnail[0];
//
//
//    $terms = get_the_terms( $post->ID, 'product_cat' );
//    $nterms = get_the_terms( $post->ID, 'product_tag'  );
//
//
//
//    $_data['category'] = $terms;
//    $_data['tag'] = $nterms;
//
//    $product = wc_get_product( $post->ID );
//
//    $_data['price'] = $product->get_price_html();
//
//
////    $blog_name = wp_get_object_terms($post->ID, 'portfolio');
////    $_data['portfolio_cat_name'] = $blog_name[0]->name;
//
//
//    $data->data = $_data;
//
//    return $data;
//}
//add_filter('rest_prepare_product', 'prepare_rest_product',10, 3);
//
//
//
//


