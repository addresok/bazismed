
<!--?php  /* Template Name: Varikoz */ ?-->

<?php get_header(); ?>
<script type="application/ld+json">
{
  "@context":"http://schema.org",
  "@id":"https://bazismed.com/varikoz/",
  "@type":"ItemPage",
  "copyrightHolder":{"@id":"#MedicalOrganization"},
  "publisher":{"@id":"#MedicalOrganization"},
  "mainEntity":{
    "@id":"#Product",
    "@type":"Product",
    "name":"варикозу",
    "url":"https://bazismed.com/varikoz/",
    "offers":{
      "@id":"#Offer",
      "@type":"Offer",
      "priceCurrency":"UAH",
      "availability":"http://schema.org/InStock",
      "itemOffered":{"@id":"#Product"},
        "offeredBy":{
          "@id":"#Organization",
          "@type":"Organization",
          "name":"варикозу",
          "url":"https://bazismed.com/"
        }
    }
  }
}
</script>
<style>
.usd.bk-right::before {
    opacity: .8;
}
.bk-form {
    height: 390px;
}
</style>	
<div class="varicos_page">
<div class="varicos varicoz_on_mob">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-sm-6 col-md-6 col-xs-5"><div class="head-one"><?php echo __('Worried about varicose veins?'); ?></div></div>
			<div class="col-lg-6 col-sm-6 col-md-6"><div class="head-two"><span style=" position: relative; z-index: 3;"><?php echo __('will be happy to help you!'); ?></span></div></div>
		</div>
	</div>
</div>			
<div class="usd bk-right" style="background-image: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>');">
	
<div class="container">
	<div class="row">
		<div class="col-lg-12">
				<div class="block-info var">
		<h1 class="title var some-dekstop"><?php // echo __('The unique method'); ?> Сучасне та ефективне лікування варикозу <br/>
<span><?php // echo __('Laser Treatment'); ?></span><br/>
</h1>
<div class="title some-mob" style="text-transform:lowercase; line-height:19px;"><?php echo __('Without anesthesia and scarring; An hour'); ?>
</div>
		<a href="#varikoz" class="butons "><?php // echo __('Register for reception'); ?> Записатись на безкоштовну консультацію</a>
		<div class="clr"></div>
					<a href="/price?data=587" class="price f"><?php echo __('See prices'); ?></a>
	</div>
		</div>
	</div>
</div>

</div>



<div class="stages-wrap" style="border-top:none; margin-bottom:0;">
	<div class="container">
		<div class="row">	
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="titk">Ми лікуємо варикоз <?php //echo __('Laser Treatment'); ?></div>
			</div>
			<div class="stage-container col-lg-7 col-md-10 col-sm-12 col-xs-12 col-sentered">
				<div class="row">
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-6">
						<div class="circle">
							<div class="numbers">1</div>
						</div>
						<div class="text-info" style="margin-top:5px;"><?php // echo __('Without pain'); ?>склеротерапія</div>
					</div>	
					<div class="col-lg-3 col-sm-3 col-md-3 col-xs-6">
						<div class="circle">
							<div class="numbers">2</div>
						</div>
						<div class="text-info" style="margin-top:5px;"><?php //echo __('Without scarring'); ?>мініфлебектомія</div>
					</div>
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
						<div class="circle"> 
							<div class="numbers">3</div>
						</div>
						<div class="text-info" style="margin-top:5px;"><?php //echo __('Without cuts'); ?> EVLC ендовазальна лазерна коагуляція</div>
					</div>

				</div>	
			</div>
		</div>
	</div>
</div>

<div class="bk-uzd">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-sm-12 col-md-12 col-sx-12 col-centerd">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 ">
						<div class="tit-surgery line"><?php echo get_field('varicoz1', $post->ID); ?>
							<div class="ars ars1 active "></div>
						</div>
						<div class="other-text-surgery one"><?php echo get_field('varicoz_1', $post->ID); ?></div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 ">
						<div class="tit-surgery line"><?php echo get_field('varicoz2', $post->ID); ?>
							<div class="ars ars2"></div>
						</div>
						<div class="other-text-surgery closed2 two"><?php echo get_field('varicoz_2', $post->ID); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
    <div class="row">
    <div class="test">

    <?php
    $showposts = -1; // -1 shows all posts
    $do_not_show_stickies = 1; // 0 to show stickies
    $args=array('category_name' => 'tests');
    $my_query = new WP_Query($args);
    ?>
    <?php if( $my_query->have_posts() ) { ?>
        <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
            <?php
            //necessary to show the tags
            global $wp_query;
            $wp_query->in_the_loop = true;
            ?>
            <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                <h2> <?php the_title_attribute(); ?></h2>

                <div class="entry">
                    <?php the_content('Read the rest of this entry »'); ?>
                </div>
            </div>

        <?php  endwhile;} ?>
</div>
</div>
</div>
<div class="hirurg">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 col-centered">
				<div class="hir">	
					<?php $args = array( 'cat' => 6, 'nopaging' => true );
					$lastposts = new WP_Query($args);
					while ($lastposts->have_posts()): $lastposts->the_post();
						$checked = get_field('view_on_varikoz');
						if($checked){
					?>
						<div class="box">
							<div class="photo">
								<img src="<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>">
								<div class="name"><?php the_title(); ?></div>
								<div class="post"><?php echo get_field('specialization', $post->ID); ?></div>
							</div>
							<div class="text-ino-h"><?php the_content(); ?></div>
						</div>
					<?php } endwhile;
						wp_reset_query();
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="varikoz" class="bk-form">
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="block pos-auto-page">
					<div class="title"><?php //echo __('Leave request title text'); ?>Залишились питання?</div>
					<div class="descr"><?php //echo __('Leave request desc text'); ?>Записуйтесь на <b>БЕЗКОШТОВНУ</b> консультацію флеболога</div>
                    <?php
                    $currentLang = qtrans_getLanguage();
                    if ($currentLang == 'ru'){
                        echo do_shortcode('[contact-form-7 id="884" title="Варикоз"]');
                    } elseif ($currentLang == 'ua'){
                        echo do_shortcode('[contact-form-7 id="206" title="Варікоз"]');
                    }
                    ?>
				</div>
			</div>
		</div>
	</div>

</div>

<div id="homePage">
<div id="blogHome" class="info-block most-blocks" style="display: none;">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="title">Наш блог</div></div>
        <div class="block-title">
            <h2>Най</h2>
            <span class="sort" v-bind:class="(blogItemsNai.orderby == 'comment_count') ? 'active': ''" @click="sortPageNai('comment_count')">коментоване</span>
            <span class="sort" v-bind:class="(blogItemsNai.orderby == 'post_views') ? 'active': ''" @click="sortPageNai('post_views')">популярне</span>
            <span class="sort"  v-bind:class="(blogItemsNai.orderby == 'date') ? 'active': ''" @click="sortPageNai('date')">нове</span>
        </div>
        <div class="row news_blog">
            <div class="link_news_blog">
                <span class="arrow-ser-left" v-on:click="getPrevPageNai" v-bind:class="(blogItemsNai.page == 1) ? 'arrow-disab': ''"></span>
                <span class="arrow-ser-right" v-on:click="getNextPageNai" v-bind:class="(blogItemsNai.page == blogItemsNai.totalBlogItems) ? 'arrow-disab': ''"></span>
            </div>
            <div class="col-md-4 block" v-for="(item, index) in blogItemsNai.post">
                <div class="block-image"
                     v-bind:style="{ backgroundImage: 'url(' + item.images.medium + ')' }">
                    <a class="overlay" v-bind:href="item.link"></a>
                    <div class="attributes">
                        <div class="cat left">
                            <span  v-if="item.blog_name" class="category">{{item.blog_name}}</span>
                        </div>
                        <div class="times right"><span class="time"><span class="icon"></span> {{item.date | formatDate}} </span></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="author-block">
                    <span class="author-name left"> {{ item.autorb }} </span>
                    <div class="info right">
                        <span class="icon-views">{{ (item.post_views) ? item.post_views : 0 }}</span>
                        <a v-bind:href="(item.link + '#comments')" class="icon-comment">{{ (item.count_comments) ? item.count_comments : 'Залишити відгук' }}</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <a v-bind:href="item.link" class="article-name" v-html="item.title.rendered"></a>
                <div class="description" v-html="item.excerpt.rendered"></div>
            </div>
            <div class="loader-tush" v-if="blogItemsNai.animate">
                <div class="loader">
                    <div></div>
                    <svg  xmlns="http://www.w3.org/2000/svg" version="1.1">
                        <defs>
                            <filter id="goo">
                                <fegaussianblur in="SourceGraphic" stddeviation="15" result="blur"></fegaussianblur>
                                <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 26 -7" result="goo"></fecolormatrix>
                                <feblend in="SourceGraphic" in2="goo"></feblend>
                            </filter>
                        </defs>
                    </svg>
                </div>
            </div>

        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="btn-detal">
                <a href="/blog">Перейти на сторінку блогу ><span class="slide-arr-more "></span></a>
            </div>
        </div>
    </div>

</div>

    <div v-model="firstPost" v-if="firstPost" class="container-fluid current-article current-article-favorite"
         v-bind:style="{ backgroundImage: 'url(' +  firstPost.images.medium + ')' }">
        <div class="overlay"></div>
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <span class="pin"></span>
                        <div class="info table">
                            <div class="table-cell">
                                <span  v-if="firstPost.blog_name" class="category">{{firstPost.blog_name}}</span>
                                <a v-bind:href="firstPost.link" class="article-name" v-html="firstPost.title.rendered"></a>
                            </div>
                        </div>
                        <div class="footer">
                            <span class="author-name">{{ firstPost.autorb }}</span>
                            <div class="info">
                                <span class="icon-views">{{ (firstPost.post_views) ? firstPost.post_views : 0 }}</span>
                                <!--<span class="icon-like">23</span>-->
                                <a v-bind:href="(firstPost.link + '#comments')" class="icon-comment">{{ (firstPost.count_comments) ? firstPost.count_comments : 0 }}</a>
                            </div>
                            <span class="time"><span class="icon"></span> {{firstPost.date | formatDate}}</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <div class="subscribe favorite-post-subscribe">
                            <p class="title"><span class="bold">Подписаться</span></p>
							<?php echo do_shortcode('[wysija_form id="4"]');?>
                            <p class="stat">Уже підписалися 2 чоловік</p>
                        </div>
                        <div class="subscribe favorite-post-success" style="height:150px; display:none;">
                            <p class="title"><span class="bold">Спасибо за подписку</span></p>
                            <div class="envelope_img"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="reviews" class="reviews" style="margin-top: 0;">
		<div class="slider">
			<div id="owl-demo">
			
				<?php get_post();
					if (query_posts('cat=4')): while (have_posts()): the_post(); ?>					
						<div>
							<div class="first">
								<div class="face">
									<img src="<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>">
								</div>
								<div class="name"><?php the_title(); ?></div>
								<div class="desc">(<?php echo get_field('position', $post->ID); ?>)</div>
							</div>
							<div class="second">
							<?php the_content(); ?>
							</div>
						</div>
				<?php endwhile; endif; 
					wp_reset_query();
				?>
			</div>
		</div>
</div>

	<div class="rewies-mobile">
		<h6><?php echo __('Reviews'); ?></h6>
		<div class="rew-wrap">
			<a href="javascript:void(0);" class="contrl prev">
				<span></span>
			</a>
			<div class="rew-cont col-xs-12">
			<?php get_post();
					if (query_posts('cat=4')): while (have_posts()): the_post(); ?>	
				<div class="rew-box ">
					<h5>
						<?php the_title(); ?>
						<span>
							(<?php echo get_field('position', $post->ID); ?>)
						</span>
 					</h5>
 					<p>
 						<?php the_content(); ?>
 					</p>
				</div>
			<?php endwhile; endif; 
					wp_reset_query();
				?>
			</div>
			<a  href="javascript:void(0);" class="contrl nextss">
				<span></span>
			</a>
		</div>
	</div>
<div class="str-mob">
	<div class="news-mob">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 col-centered">
					<div class="news">
						<div class="title active js-btn" data-tab="unikal_one"><?php echo __('News'); ?></div>
						<div class="title_two js-btn" data-tab="unikal_two"><?php echo __('Actions'); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="news-mob-container" style="background:#fff;">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-centered">
				<div id="unikal_one" class="block active">
					<div class="owl-slider-mobile">
					<?php get_post();
					if (query_posts('cat=2')): while (have_posts()): the_post(); ?>
						<div class="fuc-block">
							<a href="<?php echo get_permalink() ?>" class="img" style="background:url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover; background-repeat:no-repeat; width:100%; height:164px; display:block;"></a>
							<div class="time">
								<div class="data"><?php echo the_date('Y.m.d'); ?></div>
								<a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
								<span class="det-arrow"></span>
								</a>
							</div>
							<a href="<?php echo get_permalink() ?>" class="descript"><?php the_excerpt('(читати далі)',TRUE); ?></a>
						</div>
					<?php endwhile; endif; 
						wp_reset_query();
					?>
					</div>
					<?php get_post();
					if (query_posts('cat=2')): while (have_posts()): the_post(); ?>
						<div class="new">
							<a href="<?php echo get_permalink() ?>" class="img" style="background: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover;"></a>
							<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="title"><?php the_title(); ?></a>
							<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="description"><?php the_excerpt('(читати далі)',TRUE); ?></a>
							<a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
							<span class="det-arrow"></span>
							</a>
						</div>
					<?php endwhile; endif; 
						wp_reset_query();
					?>
					<div class="clr"></div>
					<a href="<?php $category_id = 2; $category_link = get_category_link( $category_id );
						echo $category_link; ?>" class="all_new"><?php echo __('All news'); ?>
						<div class="arrow"></div>
					</a>
				</div>

				<div id="unikal_two" class="block">
					<div class="owl-slider-mobile">
					<?php get_post();
					if (query_posts('cat=3')): while (have_posts()): the_post(); ?>
						<div class="fuc-block">
							<a href="<?php echo get_permalink() ?>" class="img" style="background:url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover; background-repeat:no-repeat; width:100%; height:164px; display:block;"></a>
							<div class="time">
								<div class="data"><?php echo the_date('Y.m.d'); ?></div>
								<a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
								<span class="det-arrow"></span>
								</a>
							</div>
							<a href="<?php echo get_permalink() ?>" class="descript"><?php the_excerpt('(читати далі)',TRUE); ?></a>
						</div>
					<?php endwhile; endif; 
						wp_reset_query();
					?>
					</div>
					<?php get_post();
					if (query_posts('cat=3')): while (have_posts()): the_post(); ?>
						<div class="new">
							<a href="<?php echo get_permalink() ?>" class="img" style="background: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover;"></a>
							<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="title"><?php the_title(); ?></a>
							<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="description"><?php the_excerpt('(читати далі)',TRUE); ?></a>
							<a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
							<span class="det-arrow"></span>
							</a>
						</div>
					<?php endwhile; endif; 
						wp_reset_query();
					?>	
					<div class="clr"></div>
					<a href="<?php $category_id = 3; $category_link = get_category_link( $category_id );
						echo $category_link; ?>" class="all_new"><?php echo __('all shares'); ?>
						<div class="arrow"></div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<?php get_footer(); ?>	