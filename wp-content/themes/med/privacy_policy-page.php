<!--?php  /* Template Name: privacy policy */ ?-->
<?php get_header(); ?>
<section class="policy">
    <div class="container">
        <div class="row">
            <h1 class="title"><?php echo get_the_title();?></h1>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xl-offset-3 col-lg-offset-3 col-md-offset-3">
                <?php
                if (have_posts()) {
                    the_post();
                    $content = get_the_content();
                    ?>
                        <?php echo $content;?>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
