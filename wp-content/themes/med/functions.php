<?php
define("THEME_DIR", get_template_directory_uri());
remove_action('wp_head', 'wp_generator');

add_theme_support('menus');
add_theme_support('post-thumbnails');

register_nav_menus(array(
  'Головне меню' => 'Меню',
  'Мобильные меню' => 'Мобильные меню',
));

add_filter('show_admin_bar', '__return_false');
/*wp_enqueue_style('bootstrap_cdn','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
wp_enqueue_style('fontawesome_cdn', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
wp_enqueue_style('jquery_ui', THEME_DIR . '/css/jquery-ui.css');
wp_enqueue_style('owl_carousel', THEME_DIR . '/css/owl.carousel.css');
wp_enqueue_style('owl_theme', THEME_DIR . '/css/owl.theme.css');
wp_enqueue_style('owl_transitions', THEME_DIR . '/css/owl.transitions.css');
wp_enqueue_style('style', THEME_DIR . '/style.css','', '1.3');*/

function add_project_scripts() {
    wp_enqueue_script('jquery_js', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js', array('jquery'), '', true);
    wp_enqueue_script('jquery_ui_js', THEME_DIR . '/js/jquery-ui.min.js', array('jquery'), '', true);
    wp_enqueue_script('less_compiler', THEME_DIR . '/less.min.js', array('jquery'), '', true);
    wp_enqueue_script('owl_carousel_js', THEME_DIR . '/js/owl.carousel.min.js', array('jquery'), '', true);
    wp_enqueue_script('fancybox', THEME_DIR . '/js/jquery.fancybox.pack.js', array('jquery'), '', true);
    wp_enqueue_script('cookie', THEME_DIR . '/js/jquery.cookie.js', array('jquery'), '', true);
    wp_enqueue_script('session', THEME_DIR . '/js/jquery.session.js', array('jquery'), '', true);

    wp_enqueue_script('main', THEME_DIR . '/js/main.js', array('jquery'), '1.4', true);
    
	// m
	
	wp_enqueue_script('vue', THEME_DIR . '/js/vue.min.js', array('jquery'), '', true);
	wp_enqueue_script('vue-router', THEME_DIR . '/js/vue-router.js', array('jquery'), '', true);
	wp_enqueue_script('vue-resource', THEME_DIR . '/js/vue-resource.min.js', array('jquery'), '', true);
	wp_enqueue_script('app', THEME_DIR . '/js/app.js', array('jquery'), '', true);
	
 
}

add_action( 'wp_enqueue_scripts', 'add_project_scripts' );

add_theme_support( 'post-thumbnails' );
function setColForArticles($iterator){
    if($iterator == true){
        return 'big col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12';
    }else{
        return 'smaller col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12';
    }
}
function initBlogTaxonomy()
{
    register_taxonomy(
        'blog',
        'article',
        array(
            'labels' => array(
                'name' => "Категорії Блогу",
                'add_new_item' => "Добавити статю",
                'new_item_name' => "Добавити нова стаття"
            ),
            'show_ui' => true,
            'show_tagcloud' => true,
            'hierarchical' => true,
	        'rewrite'               => array( 'slug' => 'blogs' ),
	        'show_in_rest'          => true,
	        'rest_base'             => 'blogs',
	        'rest_controller_class' => 'WP_REST_Terms_Controller',
        )
    );
    register_post_type('article',
        array(
            'labels' => array(
                'name' => "Блог",
                'singular_name' => "Статті",
                'add_new' => 'Добавити статю',
                'add_new_item' => "Добавити нову статтю",
                'edit' => "Редагувати",
                'edit_item' => 'Редактировать статтю',
                'new_item' => 'Новиа стаття',
                'view' => 'Дивитися',
                'view_item' => 'Дивитися статті',
                'search_items' => 'Пошук статтів',
                'not_found' => 'Обзор статтів',
                'not_found_in_trash' => 'Немає статтів',
                'parent' => 'Батьківська стаття'
            ),
            'public' => true,
            'menu_position' => 7,
            'supports' => array('title', 'editor', 'author', 'comments', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields'),
            'taxonomies' => array('blog'),
            'menu_icon' => null,
	        'capability_type'    => 'post',
	        'taxonomies'  => array( 'blog'),
            'has_archive' => true,
	        'rewrite'            => array( 'slug' => 'article' ),
	        'show_in_rest'       => true,
	        'rest_base'          => 'article',
	        'rest_controller_class' => 'WP_REST_Posts_Controller',
        )
    );
    flush_rewrite_rules();
}

add_action('init', 'initBlogTaxonomy');

function customize_register($wp_customize){
	$wp_customize->add_section('soc', array(
		'title' => 'Соц. сеть',
		'capability' => 'edit_theme_options',
		'description' => ''
	));	
	$wp_customize->add_setting('vk_url', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('vk', array(
		'type' => 'text',
		'label' => 'Url на сайт vkontakte',
		'section' => 'soc',
		'settings' => 'vk_url'
	));	
	$wp_customize->add_setting('fb_url', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('fb', array(
		'type' => 'text',
		'label' => 'Url на сайт facebook',
		'section' => 'soc',
		'settings' => 'fb_url'
	));	

	$wp_customize->add_setting('twitter_url', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('tw', array(
		'type' => 'text',
		'label' => 'Url на сайт twitter',
		'section' => 'soc',
		'settings' => 'twitter_url'
	));	

	$wp_customize->add_setting('odnoklassniki_url', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('ond', array(
		'type' => 'text',
		'label' => 'Url на сайт odnoklassniki',
		'section' => 'soc',
		'settings' => 'odnoklassniki_url'
	));	

	$wp_customize->add_section('tel', array(
		'title' => 'Телефон',
		'capability' => 'edit_theme_options',
		'description' => ''
	));	
	$wp_customize->add_setting('telephone', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('tl', array(
		'type' => 'text',
		'label' => 'Телефон 1',
		'section' => 'tel',
		'settings' => 'telephone'
	));	

	$wp_customize->add_setting('telephone1', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('tl1', array(
		'type' => 'text',
		'label' => 'Телефон 2',
		'section' => 'tel',
		'settings' => 'telephone1'
	));	


	$wp_customize->add_section('mail', array(
		'title' => 'Почта',
		'capability' => 'edit_theme_options',
		'description' => ''
	));	
	$wp_customize->add_setting('mailto', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('ml', array(
		'type' => 'text',
		'label' => 'Почта',
		'section' => 'mail',
		'settings' => 'mailto'
	));



	$wp_customize->add_section('address', array(
		'title' => 'Адрес',
		'capability' => 'edit_theme_options',
		'description' => ''
	));	
	$wp_customize->add_setting('addressto', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('adr', array(
		'type' => 'text',
		'label' => 'Адрес',
		'section' => 'address',
		'settings' => 'addressto'
	));

	$wp_customize->add_section('form', array(
		'title' => 'Шордкод',
		'capability' => 'edit_theme_options',
		'description' => ''
	));	
	$wp_customize->add_setting('formto', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('frm', array(
		'type' => 'text',
		'label' => 'Шордкод',
		'section' => 'form',
		'settings' => 'formto'
	));

		$wp_customize->add_section('form2', array(
		'title' => 'Шордкод1',
		'capability' => 'edit_theme_options',
		'description' => ''
	));	
	$wp_customize->add_setting('formto2', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('frms', array(
		'type' => 'text',
		'label' => 'Шордкод1',
		'section' => 'form2',
		'settings' => 'formto2'
	));

	$wp_customize->add_section('map', array(
		'title' => 'Карта',
		'capability' => 'edit_theme_options',
		'description' => ''
	));	
	$wp_customize->add_setting('mapto', array(
		'default' => '',
		'type' => 'option'
	));
	$wp_customize->add_control('maps', array(
		'type' => 'text',
		'label' => 'Карта',
		'section' => 'map',
		'settings' => 'mapto'
	));
}
add_action('customize_register', 'customize_register');

function wp_corenavi() {
  global $wp_query;
  $pages = '';
  $max = $wp_query->max_num_pages;
  if (!$current = get_query_var('paged')) $current = 1;
  $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
  $a['total'] = $max;
  $a['current'] = $current;

  $total = 1; //1 - выводить текст "Страница N из N", 0 - не выводить
  $a['mid_size'] = 1; //сколько ссылок показывать слева и справа от текущей
  $a['end_size'] = 1; //сколько ссылок показывать в начале и в конце
  $a['prev_text'] = '<span class="arrow-left-pagin"></span>'; //текст ссылки "Предыдущая страница"
  $a['next_text'] = '<span class="arrow-right-pagin"></span>'; //текст ссылки "Следующая страница"

  if ($max > 1) echo '<div class="pagination">';
  if ($total == 1 && $max > 1) $pages =  $pages. '<div class="pagin">' . paginate_links($a). '</div>';
  echo $pages ;
  if ($max > 1) echo '</div>';
}

function new_excerpt_more($more) {
      global $post;
    return ' ...';
}

add_filter('excerpt_more', 'new_excerpt_more');



function dimox_breadcrumbs() {

    $currentLang = qtrans_getLanguage();
    if ($currentLang == 'ru'){
        $text['home'] = 'Главная';
    } elseif ($currentLang == 'ua'){
        $text['home'] = 'Головна ';
    }
    $wrap_before = '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">';
    $wrap_after = '</div><!-- .breadcrumbs -->';
    $sep = '›';
    $sep_before = '<span class="sep">';
    $sep_after = '</span>';
    $show_home_link = 1;
    $show_on_home = 0;
    $show_current = 1;
    $before = '<span class="current">';
    $after = '</span>';


    global $post;
    $home_url = home_url('/');
    $link_before = '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
    $link_after = '</span>';
    $link_attr = ' itemprop="item"';
    $link_in_before = '<span itemprop="name">';
    $link_in_after = '</span>';
    $link = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
    $frontpage_id = get_option('page_on_front');
    $parent_id = ($post) ? $post->post_parent : '';
    $sep = ' ' . $sep_before . $sep . $sep_after . ' ';
    $home_link = $link_before . '<a href="' . $home_url . '"' . $link_attr . ' class="home">' . $link_in_before . $text['home'] . $link_in_after . '</a>' . $link_after;

    if (is_home() || is_front_page()) {

        if ($show_on_home) echo $wrap_before . $home_link . $wrap_after;

    } else {

        echo $wrap_before;
        if ($show_home_link) echo $home_link;

        if ( is_category() ) {
            $cat = get_category(get_query_var('cat'), false);
            if ($cat->parent != 0) {
                $cats = get_category_parents($cat->parent, TRUE, $sep);
                $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
                if ($show_home_link) echo $sep;
                echo $cats;
            }
            if ( get_query_var('paged') ) {
                $cat = $cat->cat_ID;
                echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
            }

        }   elseif ( is_single() && !is_attachment() ) {
            if ($show_home_link) echo $sep;
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                printf($link, $home_url . $slug['slug'] . '/', $post_type->labels->singular_name);
                if ($show_current) echo $sep . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $sep);
                if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
                echo $cats;
                if ( get_query_var('cpage') ) {
                    echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
                } else {
                    if ($show_current) echo $before . get_the_title() . $after;
                }
            }

            // custom post type
        } elseif ( is_page() && !$parent_id ) {
            if ($show_current) echo $sep . $before . get_the_title() . $after;

        } elseif ( is_page() && $parent_id ) {
            if ($show_home_link) echo $sep;
            if ($parent_id != $frontpage_id) {
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    if ($parent_id != $frontpage_id) {
                        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                    }
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                    echo $breadcrumbs[$i];
                    if ($i != count($breadcrumbs)-1) echo $sep;
                }
            }
            if ($show_current) echo $sep . $before . get_the_title() . $after;

        }

        echo $wrap_after;

    }
}





/*
 *  WP_REST
 */


function get_post_views($post_id){
	global $wpdb;
	return $wpdb->get_row("SELECT count FROM wp_post_views WHERE id = $post_id
    AND period='total'");
}

function prepare_rest($data, $post, $request){
	$_data = $data->data;
	
	$thumbnail_id = get_post_thumbnail_id($post->ID);
	
	$thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
	$thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
	$thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');
	
	$_data['images']['medium'] = $thumbnail_medium[0];
	$_data['images']['large'] = $thumbnail_large[0];
	$_data['images']['thumbnail'] = $thumbnail_thumbnail[0];
	$blog_name = wp_get_object_terms($post->ID, 'blog');
	$_data['blog_name'] = $blog_name[0]->name;
	$filds = get_post_custom($post->ID);
	$_data['filds'] = $filds;
	$autorb = ( isset($filds['article_author'][0]) && trim($filds['article_author'][0])) ? $filds['article_author'][0] : 'Адміністратор';
	
	$_data['autorb'] = $autorb;
	$post_views = get_post_views($post->ID);
	
	$_data['post_views'] = (int) $post_views->count;
	
	$comments_count = wp_count_comments($post->ID);
	$_data['count_comments'] = (int) $comments_count->total_comments;

	$data->data = $_data;
	
	return $data;
}
add_filter('rest_prepare_post', 'prepare_rest',10, 3);
add_filter('rest_prepare_article', 'prepare_rest',10, 3);
add_filter('rest_prepare_vacance', 'prepare_rest',10, 3);








?>