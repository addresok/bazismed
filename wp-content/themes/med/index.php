<!--?php  /* Template Name: Main */ ?-->
<?php get_header();
the_post();
?>
<div id="homePage">
<div class="main_slider">
    <?php $args = array('cat' => 11, 'nopaging' => false);
    $lastposts = new WP_Query($args);
    while ($lastposts->have_posts()): $lastposts->the_post();
        $position = get_field('text_position', $post->ID); ?>
        <div class="item">
            <div class="slider_description" style="color: <?php echo get_field('color_text', $post->ID); ?>;<?php if($position == true) echo "right: 5%;";
            else if($position == false) echo "left: 5%;"; ?>"><?php the_content();?></div>
            <img class="img-responsive" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID));?>"/>
        </div>
        <?php
    endwhile;
    wp_reset_query();
    ?>
</div>

    <div id="about" class="about" style="background: none;height: 250px;padding-top: 27px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="conta1">
                        <div class="title"><?php echo __('about us'); ?></div>
                        <div class="col-md-8 text"><?php the_content(); ?></div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="section section-hours fp-completely" id="hoursHome">
        <div class="full-size"></div>
        <div class="content">
            <!-- Begin of Left Content -->
            <section class="c-left anim">
                <div class="wrapper fit">

                    <!-- Logo Instead of clock -->
                    <!--<div class="home-logo ">
						<img class="" src="img/logo.png" alt="Logo">
					</div>-->

                    <!-- Coutdown Clock -->
                    <div class="clock clock-countdown">
                        <div class="site-config" data-date="01/01/2018 00:00:00" data-date-timezone="+0"></div>
                        <div class="clock-wrapper">
                            <div class="tile tile-years">
                                <span class="years"></span>
                                <span class="txt">років</span>
                            </div>
                            <div class="tile tile-days">
                                <span class="days"></span>
                                <span class="txt">днів</span>
                            </div>
                            <div class="clock-hms">
                                <div class="tile tile-hours">
                                    <span class="hours"></span>
                                    <span class="txt">годин</span>
                                </div>
                                <div class="tile tile-minutes">
                                    <span class="minutes"></span>
                                    <span class="txt">хвилин</span>
                                </div>
                                <div class="tile tile-seconds">
                                    <span class="seconds"></span>
                                    <span class="txt">секунд</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- End of Left Content -->



            <!-- Begin of Right Content -->
            <section class="c-right anim">
                <div class="wrapper">

                    <!-- Title and description -->
                    <div class="title-desc">
                        <div class="t-wrapper">
                            <!-- Logo -->
                            <!--<div class="logo home-logo ">
								<img class="" src="img/logo.png" alt="Logo">
							</div>-->
                            <!-- Title -->
                            <header class="title">
                                <span class="anim-wrapper"><h2><strong><?= get_field('clients', $post->ID);?></strong> <br> Клієнтів</h2></span>
                                <span class="anim-wrapper"><h3>Нам є чим пишатись! </h3></span>
                            </header>
                            <!-- desc -->
                            <div class="desc">
                                <span class="anim-wrapper"><p>
                                        У багато чому Ми найкращі, і вдається нам це тільки завдяки тому що ми вірні нашим цінностям і принципам!
                                    </p></span>
                            </div>
                        </div>
                    </div>

                    <!-- Action button -->
                    <div class="cta-btns">
                            <span class="anim-wrapper"><a class="btn arrow-circ-btn" href="#" onclick="window.scrollTo(0,0)">
                                <span class="txt">Про нас</span>
                                <span class="arrow-icon"></span>
                            </a></span>

                        <span class="anim-wrapper"><a class="btn arrow-circ-btn" target="_blank" href="/services-and-prices">
                                <span class="txt">Послуги та ціни</span>
                                <span class="arrow-icon"></span>
                            </a></span>
                    </div>

                </div>
            </section>
            <!-- End of Right Content -->
        </div>




    </div>




    <!--<div id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="conta1">
                    <div class="title"><?php /*echo __('about us'); */?></div>
                    <div class="col-md-8 text"><?php /*the_content(); */?></div>
                    <div class="btns">
                        <a class="link">
                            <div class="hov-efect"><?php /*echo __('Video'); */?></div>
                            <div class="youtube">
                                <div class="ico"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="conta2">
                    <iframe width="560" height="315" src="<?php /*echo get_field('link_video', $post->ID); */?>" frameborder="0"
                            allowfullscreen></iframe>
                    <div class="btns">
                        <a class="link">
                            <div class="hov-efect"><?php /*echo __('about us'); */?></div>
                            <div class="ab">
                                <div class="ico"></div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>-->


<div class="advantages-wrap-mobile">
    <div class="advantages-cont">
        <h5><?php echo __('Why order service'); ?></h5>
        <div class="tab-cont">
            <div class="tab-box">
                <span class="tab">
                    <img src="/wp-content/themes/med/img/med-file.png" alt="ico">
                    <span><?php echo get_field('text_1', $post->ID); ?></span>
                    <!--<i></i>-->
                </span>
                <!--<div class="tab-wrap">
                    <?php /*echo get_field('descr1', $post->ID); */?>
                </div>-->
            </div>
            <div class="tab-box">
                <span class="tab">
                    <img src="/wp-content/themes/med/img/cardiogram.png" alt="ico">
                    <span><?php echo get_field('text_2', $post->ID); ?></span>
                    <!--<i></i>-->
                </span>
                <!--<div class="tab-wrap">
                    <?php /*echo get_field('descr2', $post->ID); */?>
                </div>-->
            </div>
            <div class="tab-box">
                <span class="tab">
                    <img src="/wp-content/themes/med/img/med-spec.png" alt="ico">
                    <span><?php echo get_field('text_3', $post->ID); ?></span>
                    <!--<i></i>-->
                </span>
                <!--<div class="tab-wrap">
                    <?php /*echo get_field('descr3', $post->ID); */?>
                </div>-->
            </div>
            <div class="tab-box">
                <span class="tab">
                    <img src="/wp-content/themes/med/img/heart-mini.png" alt="ico">
                    <span><?php echo get_field('text_4', $post->ID); ?></span>
                    <!--<i></i>-->
                </span>
                <!--<div class="tab-wrap">
                    <?php /*echo get_field('descr4', $post->ID); */?>
                </div>-->
            </div>
        </div>
    </div>
</div>

<!--
<div class="about-two">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="title" style="font-size: 18px;"><?php /*//echo __('Why order service'); */?>Чому нам довіряють тисячі пацієнтів</div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-senter">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="block no-psev1">
                            <div class="ball"></div>
                            <div class="photo">
                                <img src="/wp-content/themes/med/img/img1.png" alt="icon">
                            </div>
                            <div class="text"><?php /*echo get_field('text_1', $post->ID); */?></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="block">
                            <div class="ball"></div>
                            <div class="photo">
                                <img src="/wp-content/themes/med/img/img2.png" alt="icon">
                            </div>
                            <div class="text"><?php /*echo get_field('text_2', $post->ID); */?></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="block">
                            <div class="ball"></div>
                            <div class="photo">
                                <img src="/wp-content/themes/med/img/img3.png" alt="icon">
                            </div>
                            <div class="text"><?php /*echo get_field('text_3', $post->ID); */?></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="block no-psev2">
                            <div class="ball"></div>
                            <div class="photo">
                                <img src="/wp-content/themes/med/img/img4.png" alt="icon">
                            </div>
                            <div class="text"><?php /*echo get_field('text_4', $post->ID); */?></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
-->
<div id="services" class="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <div class="title"><?php echo __('Our services'); ?></div>
            </div>
            <div id="serv" class="slider col-senter col-sm-10">
                <?php $args = array('cat' => 5, 'nopaging' => true);
                $lastposts = new WP_Query($args);
                while ($lastposts->have_posts()): $lastposts->the_post(); ?>
                        <div class="block">
                            <div class="photo">
                                <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>"
                                     alt="<?php the_title(); ?> image">
                               <!-- <div class="photo-bk">
                                    <div class="title"><?php the_title(); ?></div>
                                    <div class="detail">
                                        <?php the_content(); ?>
                                    </div>
                                </div>-->
                            </div>
                            <div class="block-down">
                                <div class="pos-text"><?php the_title(); ?></div>
                                <a href="<?php echo get_field('view_page', $post->ID); ?>" class="block-hov">
                                    <div class="name"><?php echo __('About service'); ?></div>
                                    <div class="arrow"></div>
                                </a>
                            </div>
                            <div class="btn-detal about-service">
                                <a href="<?php echo get_field('view_page', $post->ID); ?>">
                                    <?php echo __('About service'); ?><span class="arr-chev-r slide-arr-more"></span>
                                </a>

                            </div>
                        </div>
                    <?php endwhile;
                wp_reset_query();
                ?>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <div class="btn-detal">
                    <a href="/services-and-prices">
                        <?php echo __('See prices'); ?>
                        <span class="slide-arr-more "></span></a>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="varicos">
    <div class="container">
        <div class="row">
            <!--            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-5">
                <div class="head-one"><?php echo __('Worried about varicose veins?'); ?></div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6">
                <div class="head-two"><span style=" position: relative; z-index: 3;">б<?php echo __('will be happy to help you!'); ?></span>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="border">
                    <div class="col-lg-offset-4 col-sm-offset-5 col-lg-5 col-sm-7 col-xs-12">
                        <div class="text">«<?php /*echo get_field('text_varicoz', $post->ID); */?>»
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-10 ">
                        <div class="btn-box">
                            <a href="<?php /*echo get_field('url_var', $post->ID); */?>" class="detail"><?php echo __('More'); ?></a>
                            <div class="clr"></div>
                            <a href="<?php /*echo get_site_url(); */?>/?page_id=256" class="price"><?php echo __('See prices'); ?></a>
                        </div>
                    </div>
                    <div class="bk"></div>
                </div>
            </div>-->
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 varikoz_banner">
                <img class="img-responsive" src="/wp-content/themes/med/img/varikoz_banner.jpg">
                <div class="btn-box">
                    <a href="<?php echo get_field('url_var', $post->ID); ?>" class="detail"><?php echo __('More'); ?></a>
                    <div class="clr"></div>
                    <a href="#" class="review price">Записатись на безкоштовну консультацію</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="vacancy">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="block">
                    <div class="title"><?php echo __('Leave request title text'); ?></div>
                    <div class="descr"><?php echo __('Leave request desc text'); ?></div>
                    <?php
                    $currentLang = qtrans_getLanguage();
                    if ($currentLang == 'ru'){
                        echo do_shortcode('[contact-form-7 id="892" title="обращение"]');
                    } elseif ($currentLang == 'ua'){
                        echo do_shortcode('[contact-form-7 id="134" title="Заявка"]');
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="nurs" class="nurs tabs">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="tit"><?php echo __('Our nurs'); ?></div>
            </div>
            <div class="col-lg-9 col-xs-12 col-senter">
                <div class="row owl-carousel">
                    <?php $args = array('cat' => 6, 'nopaging' => true);
                    $lastposts = new WP_Query($args);
                    while ($lastposts->have_posts()): $lastposts->the_post();
                        ?>
                        <div class="col-lg-4 col-sm-4 col-xs-6 ">
                            <div class="block item">
                                <a href="#" data-tab="<?php the_ID(); ?>" class="photo">
                                    <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>"
                                         alt="<?php the_title(); ?> photo">
                                </a>
                                <a href="#" data-tab="<?php the_ID(); ?>" class="name"
                                   style="display:table"><?php the_title(); ?></a>
                                <a href="#" data-tab="<?php the_ID(); ?>" class="tab open">
                                    <span class="img"></span>
                                </a>
                            </div>
                        </div>
                    <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
                <div class="row">
                    <?php $args = array('cat' => 6, 'nopaging' => true);
                    $lastposts = new WP_Query($args);
                    while ($lastposts->have_posts()): $lastposts->the_post(); ?>
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <div id="<?php the_ID(); ?>" class="tab-content animated fadeIn box">
                                <div class="info"><?php echo __('Practice'); ?></div><?php echo get_field('practics', $post->ID); ?>
                                <div class="clr"></div>
                                <div class="info"><?php echo __('Study'); ?></div><?php echo get_field('teach', $post->ID); ?>

                                <div class="clr"></div>
                                <div class="info"><?php echo __('Specialization'); ?>
                                </div><?php echo get_field('specialization', $post->ID); ?>
                                <div class="clr"></div>
                            </div>
                        </div>
                    <?php endwhile;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="container-fluid question-send">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <p class="title">Бажаєш працювати в нашій команді?<br>
                           Перегляньте актуальні вакансії та надішліть своє резюме</p>
                    </div>
                    <div class="col-md-4 send-form">
                        <a href="/vacancies/" class="linktovac">Переглянути вакансії ></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

<div  itemprop="review" itemscope itemtype="http://schema.org/Review" id="reviews" class="reviews">
    <div class="slider">
        <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" id="owl-demo">

            <?php get_post();
            if (query_posts('cat=4')): while (have_posts()): the_post(); ?>
                <div>
                    <div class="first">
                        <div class="face">
                            <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>">
                        </div>
                        <div class="name" itemprop="name" ><?php the_title(); ?></div>
                        <div class="desc">(<?php echo get_field('position', $post->ID); ?>)</div>
                    </div>
                    <div class="second" itemprop="reviewBody">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endwhile; endif;
            wp_reset_query();
            ?>
        </div>
    </div>
</div>
<div id="reviews-mob" class="rewies-mobile"  itemprop="review" itemscope itemtype="http://schema.org/Review">
    <h6><?php echo __('User reviews'); ?></h6>
    <div class="rew-wrap">
        <a href="javascript:void(0);" class="contrl prev">
            <span></span>
        </a>
        <div class="rew-cont col-xs-12">
            <?php get_post();
            if (query_posts('cat=4')): while (have_posts()): the_post(); ?>
                <div class="rew-box ">
                    <h5>
                      <span itemprop="name" >  <?php the_title(); ?>
                        <span>
							(<?php echo get_field('position', $post->ID); ?>)
						</span>
                          <span>
                    </h5>
                    <p><span itemprop="name" itemprop="reviewBody" >

                        <?php the_content(); ?>
                        </span>
                    </p>
                </div>
            <?php endwhile; endif;
            wp_reset_query();
            ?>
        </div>
        <a href="javascript:void(0);" class="contrl nextss">
            <span></span>
        </a>
    </div>
</div>




<div id="blogHome" class="info-block most-blocks">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="title">Наш блог</div></div>
        <div class="block-title">
            <h2>Най</h2>
            <span class="sort" v-bind:class="(blogItemsNai.orderby == 'comment_count') ? 'active': ''" @click="sortPageNai('comment_count')">коментоване</span>
            <span class="sort" v-bind:class="(blogItemsNai.orderby == 'post_views') ? 'active': ''" @click="sortPageNai('post_views')">популярне</span>
            <span class="sort"  v-bind:class="(blogItemsNai.orderby == 'date') ? 'active': ''" @click="sortPageNai('date')">нове</span>
        </div>
        <div class="row news_blog">
            <div class="link_news_blog">
                <span class="arrow-ser-left" v-on:click="getPrevPageNai" v-bind:class="(blogItemsNai.page == 1) ? 'arrow-disab': ''"></span>
                <span class="arrow-ser-right" v-on:click="getNextPageNai" v-bind:class="(blogItemsNai.page == blogItemsNai.totalBlogItems) ? 'arrow-disab': ''"></span>
            </div>
            <div class="col-md-4 block" v-for="(item, index) in blogItemsNai.post">
                <div class="block-image"
                     v-bind:style="{ backgroundImage: 'url(' + item.images.medium + ')' }">
                    <a class="overlay" v-bind:href="item.link"></a>
                    <div class="attributes">
                        <div class="cat left">
                            <span  v-if="item.blog_name" class="category">{{item.blog_name}}</span>
                        </div>
                        <div class="times right"><span class="time"><span class="icon"></span> {{item.date | formatDate}} </span></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="author-block">
                    <span class="author-name left"> {{ item.autorb }} </span>
                    <div class="info right">
                        <span class="icon-views">{{ (item.post_views) ? item.post_views : 0 }}</span>
                        <a v-bind:href="(item.link + '#comments')" class="icon-comment">{{ (item.count_comments) ? item.count_comments : 'Залишити відгук' }}</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <a v-bind:href="item.link" class="article-name" v-html="item.title.rendered"></a>
                <div class="description" v-html="item.excerpt.rendered"></div>
            </div>
            <div class="loader-tush" v-if="blogItemsNai.animate">
                <div class="loader">
                    <div></div>
                    <svg  xmlns="http://www.w3.org/2000/svg" version="1.1">
                        <defs>
                            <filter id="goo">
                                <fegaussianblur in="SourceGraphic" stddeviation="15" result="blur"></fegaussianblur>
                                <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 26 -7" result="goo"></fecolormatrix>
                                <feblend in="SourceGraphic" in2="goo"></feblend>
                            </filter>
                        </defs>
                    </svg>
                </div>
            </div>

        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="btn-detal">
                <a href="/blog">Перейти на сторінку блогу ><span class="slide-arr-more "></span></a>
            </div>
        </div>
    </div>

</div>

    <div v-model="firstPost" v-if="firstPost" class="container-fluid current-article current-article-favorite"
         v-bind:style="{ backgroundImage: 'url(' +  firstPost.images.large + ')' }">
        <div class="overlay"></div>
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <span class="pin"></span>
                        <div class="info table">
                            <div class="table-cell">
                                <span  v-if="firstPost.blog_name" class="category">{{firstPost.blog_name}}</span>
                                <a v-bind:href="firstPost.link" class="article-name" v-html="firstPost.title.rendered"></a>
                            </div>
                        </div>
                        <div class="footer">
                            <span class="author-name">{{ firstPost.autorb }}</span>
                            <div class="info">
                                <span class="icon-views">{{ (firstPost.post_views) ? firstPost.post_views : 0 }}</span>
                                <!--<span class="icon-like">23</span>-->
                                <a v-bind:href="(firstPost.link + '#comments')" class="icon-comment">{{ (firstPost.count_comments) ? firstPost.count_comments : 0 }}</a>
                            </div>
                            <span class="time"><span class="icon"></span> {{firstPost.date | formatDate}}</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <div class="subscribe favorite-post-subscribe">
                            <p class="title"><span class="bold">Подписаться</span></p>
							<?php echo do_shortcode('[wysija_form id="3"]');?>
                            <p class="stat">Уже підписалися 2 чоловік</p>
                        </div>
                        <div class="subscribe favorite-post-success" style="height:150px; display:none;">
                            <p class="title"><span class="bold">Спасибо за подписку</span></p>
                            <div class="envelope_img"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    

<?php
global $wpdb;
$sql = 'SELECT * FROM wp_bazismed_partners';
$partners = $wpdb->get_results($sql);
if(!empty($partners)):
    ?>
    <div id="partners" class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-sx-12">
                <div class="partner-title"><?php echo __('Our partners'); ?></div>
            </div>
        </div>
        <div class="row patern">
            <div class="wrap-s">
                <div id="partners-owl">
                    <?php
                    foreach ($partners as $partner):
                        ?>
                        <a href="<?=$partner->logo_url?>" target="_blank" class="block">
                            <img src="<?=$partner->logo_file?>" alt="Partners">
                        </a>
                        <?php
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>




</div>
<?php




//$date = date("Y, m, d, H, i, s", strtotime(get_field('clock', $post->ID).' ' .get_field('hours', $post->ID) .'0000'));

//echo $date;



?>
<script>
    var dateCreatedOrg = new Date("<?php echo date("F d, Y G:i:s",strtotime(get_field('clock', $post->ID).' ' .get_field('hours', $post->ID) .'0000')); ?>");;

</script>

<?php get_footer(); ?>