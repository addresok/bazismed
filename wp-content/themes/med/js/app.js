/**
 * Created by sonet on 16.08.17.
 */


var blogPageEl = document.getElementById('blogPage');
var vacanciesPageEl = document.getElementById('vacanciesPage');
var homePageeEl = document.getElementById('homePage');
var pricePageEl = document.getElementById('pricePage');
var hoursHomeEl = document.getElementById('hoursHome');

var httpP = {
                root: '/',
                headers: {
                    Authorization: 'Basic YXBpOnBhc3N3b3Jk'
                }
            };

Vue.directive('mask', function (maskval) {
    $(this.el).mask(maskval);
});

Vue.filter('phoneformat', function (phone) {
    return phone.replace(/[^0-9]/g, '')
        .replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
});
Vue.filter('two_digits', function (value) {
    if(value.toString().length <= 1)
    {
        return "0"+value.toString();
    }
    return value.toString();
});

Vue.filter('json', function (value)  {
        return JSON.stringify(value, null, 2)
});
Vue.filter('formatDate', function (val) {
    var date = new Date(val);
    var diff = new Date() - date;
    if (diff < 1000) {
        return 'щойно';
    }

    var sec = Math.floor(diff / 1000);
    if (sec < 60) {
        return sec + ' сек. назад';
    }

    var min = Math.floor(diff / 60000);
    if (min < 60) {
        return min + ' хвилин тому';
    }

    var chas = Math.floor(diff / 3600000);
    if (chas < 24) {
        return chas + ' годин тому';
    }

    var dey = Math.floor((diff / 86400000));
    if (dey < 5) {
        return dey + ' дня тому';
    }

    var d = date;
    d = [
        '0' + d.getDate(),
        '0' + (d.getMonth() + 1),
        '' + d.getFullYear(),
        '0' + d.getHours(),
        '0' + d.getMinutes()
    ];

    for (var i = 0; i < d.length; i++) {
        d[i] = d[i].slice(-2);
    }

    return d.slice(0, 3).join('.') + ' ' + d.slice(3).join(':');
});

var Backfon = {
    template: '<div class="backfon"></div>'
}
var ContentInfo = {
    template: '<div class="ContentInfo"><slot name="header"></slot><slot name="content"></slot><slot name="footer"></slot></div>'
}
var Success = {
    props: ['successOk'],
    template: '<div class="success"><div class="title">Замовлення прийнято!</div><div class="text">Наш менеджер свяжется с Вами в ближайшее время</div><slot name="footer"></slot></div>'
}

Vue.use(VueResource);
Vue.use(VueRouter)




if(hoursHomeEl) var hoursHome = new Vue({
    el: '#hoursHome',

    data: {

        date:  Math.trunc((dateCreatedOrg).getTime() / 1000),
        now: Math.trunc((new Date()).getTime() / 1000),
        sec: 0

    },


    mounted: function () {
        this.start();
        console.log(this.date);
        console.log(this.now);


    },

    methods: {
        start: function () {
            var that = this;
            window.setInterval(function () {
                that.now = Math.trunc((new Date()).getTime() / 1000);


                that.sec = that.seconds;
                jQuery('.seconds').html(that.two_digits(that.seconds));
                jQuery('.minutes').html(that.two_digits(that.minutes));
                jQuery('.hours').html(that.two_digits(that.hours));
                jQuery('.days').html(that.two_digits(that.days));
                jQuery('.years').html(that.years);



            },1000);
        },

        two_digits: function (value) {
            if(value.toString().length <= 1)
            {
                return "0"+value.toString();
            }
            return value.toString();
        }



    },
    computed: {
        seconds: function seconds() {
            return (this.now -  this.date) % 60;
        },

        minutes: function minutes() {
            return Math.trunc(( this.now - this.date) / 60) % 60;
        },

        hours: function hours() {
            return Math.trunc((this.now - this.date ) / 60 / 60) % 24;
        },

        days: function days() {
            return Math.trunc((  this.now - this.date) / 60 / 60 / 24);
        },

        years: function years() {
            return  Math.trunc(((  this.now - this.date) / 31536000000) * 1000) ;
        },


    },




});








if(pricePageEl) var pricePage = new Vue({
    el: '#pricePage',
    http: httpP,
    data: {
        CatPrices: [],
        subCat: [],
        list: [],
        search:'',
        searchText:'',
        selectCat: 0,
        selectSubCat: 0,

        total: 0,
        totalPrice: 0,
        page: 1,
        selectInfo: false,

        cart:[],
        totalPriceCart: 0,
        cartShow: false,

        backfon: false,
        checkoutShow: false,

        name: '',
        phone: '',
        mail: '',
        successOk: false,
        error: {
            name: false,
            phone: false,
        }
    },


    mounted: function () {
        this.getCatPrices();
        this.getList();
        this.getSubCatPrices();

        if (typeof $.session.get("cartcookie") != 'undefined') this.cart = $.parseJSON($.session.get("cartcookie"));
     },
    components:{
        backfoncomp: Backfon,
        success: Success,
        contentinfo: ContentInfo,
    },
    watch: {
        cartShow: function () {
            if ((this.cartShow === true) || (this.checkoutShow === true))  this.backfon = true;
            else this.backfon = false;

        },
        checkoutShow: function () {
            if ((this.checkoutShow === true) || (this.cartShow === true))  this.backfon = true;
            else this.backfon = false;

        },
        search: function () {
             if ( this.search.length > 2 ) {
                 var that = this;
                 this.selectCat = 0;
                 this.selectSubCat = 0;
                 this.getList();
            } else {
                 if(this.searchText !== '') this.getList();
                this.searchText = "";
                 this.getList();
            }
        },

        cart:function () {
            var that = this;
            this.totalPriceCart = 0;
            this.cart.forEach(function (val) {
                that.totalPriceCart += parseInt(val.price);
            });
            this.updateCart();
            if(!this.cartShow)
            $('.infoactiv').animate({
                opacity: 0.25,
                left: '+=40',
            }, 300, function() {

                $('.infoactiv').animate({
                    opacity: 1,
                    left: '-=40',
                }, 300, function() {
                });

            });
        },

        phone: function () {
            var phone = this.phone;
            if( phone.replace(/[^0-9]/g, '').length > 10 ){
                phone = phone.substring(0, phone.length - 1);
            }
            if( phone.replace(/[^0-9]/g, '').length === 6 ){
                phone = phone.replace(/[^0-9]/g, '')
                    .replace(/(\d{3})(\d{3})/, '$1-$2');
            } else
                if( phone.replace(/[^0-9]/g, '').length === 10 ) {
                    phone = phone.replace(/[^0-9]/g, '')
                    .replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
            }

            if (this.error.phone) this.error.phone = ( this.phone.length < 2 ) ? true : false;
            this.phone = phone;
        },
        name: function () {
            if (this.error.name)  this.error.name  = ( this.name.length < 2 ) ? true : false;
        }
    },
    methods: {
        selectInfoF: function (id) {
            this.selectInfo = false;
            this.selectInfo =  this.list[id];
        },
        clear: function () {
            this.cart         = [];
            this.error.name   = this.error.phone = name = phone = mail = '';
            this.cartShow     = false;
            this.checkoutShow = false;
        },
        checkout: function () {
            this.checkoutShow = !this.checkoutShow;
        },
        order: function (event) {
            if (event) event.preventDefault()
            this.error.name  = ( this.name.length < 2 ) ? true : false;
            this.error.phone = ( this.phone.length < 2 ) ? true : false;
            if (this.error.name || this.error.phone) return false
            var cart = [];
            this.cart.forEach(function (t) {
                var ob = {
                    id: t.id,
                    title: t.title.rendered,
                    price: t.price
                }
                cart.push(ob);
            });
            var that = this,
                data = {
                action: 'orderadd',
                name   : this.name,
                phone  : this.phone,
                mail   : this.mail,
                total  : this.totalPriceCart,

                cart   : cart
            };
            jQuery.post( '/wp-admin/admin-ajax.php', data, function(response) {
                console.log('Success!:', response);
                that.clear();
                that.successOk = true;
            });
        },
        updateCart: function () {
                $.session.set("cartcookie", JSON.stringify(this.cart));
        },
        getCatPrices: function () {
            this.$http.get('/wp-json/wp/v2/prices?parent=0&per_page=30&orderby=id').then(function (r) {
                this.CatPrices = r.body;
            }, function (r) {
                console.log(r);
            });
        },
        getSubCatPrices: function () {
            if (this.selectCat > 0)
            this.$http.get('/wp-json/wp/v2/prices?parent='+this.selectCat+'&per_page=50&orderby=id').then(function (r) {
                this.subCat = r.body;
            }, function (r) {
                console.log(r);
            });
        },
        getList: function () {
            this.list = [];
            var url = '/wp-json/wp/v2/price?per_page=16&page=' +  this.page ;
            if ( this.selectSubCat ) url += '&prices=' + this.selectSubCat;
                else  if ( this.selectCat ) url += '&prices=' + this.selectCat;

                if ( this.search.length > 2 ) {
                    url += '&search=' + this.search;
                }
            $('html, body').animate({ scrollTop: 0 }, 600);
            this.$http.get(url).then(function (r) {
               this.list = r.body;

                var allPostX = '';
                if (typeof r.headers.map['x-wp-totalpages'] == 'undefined') allPostX = r.headers.map['X-WP-TotalPages'][0];
                else allPostX = r.headers.map['x-wp-totalpages'][0];

                this.total = parseInt(allPostX);

                allPostX = '';
                if (typeof r.headers.map['x-wp-total'] == 'undefined') allPostX = r.headers.map['X-WP-Total'][0];
                else allPostX = r.headers.map['x-wp-total'][0];

                this.totalPrice = parseInt(allPostX);
                if ( this.search.length > 2 ) {
                this.searchText =  this.totalPrice;
                }

            }, function (r) {
                console.log(r);
            });
        },
        getListToCat: function (id, sub) {
            this.searchText = "";
            this.search = "";
            this.selectCat = id;
            this.selectSubCat = sub;
            this.page = 1;
            if ( sub == 0 ) this.subCat = [];
            this.list = [];
            this.getSubCatPrices();
            this.getList();
        },
        getNextPage: function () {
            if(this.page < this.total) {
                this.page++;
                this.getList();
            } else this.page = this.total;
        },
        getPrevPage: function () {
            if(this.page > 1) {
                this.page--;
                this.getList();
            } else this.page = 1;

        },

    },
});

if(homePageeEl) var homePage = new Vue({
    el: '#homePage',
    http: httpP,
    data: {
        message: 'Hello Vue!',
        firstPost: false,
        blogItemsNai: {
            post          :[],
            page          : 1,
            totalBlogItems: 1,
            animate       : false,
            orderby       : 'comment_count',
        },
        blogVideos: {
            video          :[],
            page          : 1,
            totalBlogItems: 1,
            animate       : false,
        },
    },


    mounted: function () {
        this.start();
        this.getBlogItemsNai();
        this.getBlogVideos();

    },

    methods: {
        start: function () {
            console.log('start');
        },
        getBlogItemsNai: function () {
            this.blogItemsNai.animate = true;
            this.$http.get('/wp-json/wp/v2/article?orderby='+ this.blogItemsNai.orderby +'&order=desc&per_page=6&page=' + this.blogItemsNai.page).then(function (r) {
                var allPostX = '';
                if (typeof r.headers.map['x-wp-totalpages'] == 'undefined') allPostX = r.headers.map['X-WP-TotalPages'][0];
                else allPostX = r.headers.map['x-wp-totalpages'][0];

                this.blogItemsNai.post = r.body;
                this.blogItemsNai.totalBlogItems = allPostX;
                this.firstPost =  this.blogItemsNai.post[0];
                var that = this;
                setTimeout(function () {
                    that.blogItemsNai.animate = false;
                }, 1000);
            }, function (r) {
                console.log(r);
            });
        },
        getNextPageNai: function () {
            if(this.blogItemsNai.page < this.blogItemsNai.totalBlogItems) {
                this.blogItemsNai.page++;
                this.getBlogItemsNai();
            } else this.blogItemsNai.page = this.blogItemsNai.totalBlogItems;
        },
        getPrevPageNai: function () {
            if(this.blogItemsNai.page > 1) {
                this.blogItemsNai.page--;
                this.getBlogItemsNai();
            } else this.blogItemsNai.page = 1;
        },
        sortPageNai: function (orderby) {
            this.blogItemsNai.orderby = orderby;
            this.getBlogItemsNai();
        },


        getNextBlogVideos: function () {
            if(this.blogVideos.page < this.blogItemsNai.totalBlogItems) {
                this.blogVideos.page++;
                this.getBlogVideos();
            } else this.blogVideos.page = this.blogItemsNai.totalBlogItems;
        },
        getPrevBogVideos: function () {
            if(this.blogVideos.page > 1) {
                this.blogVideos.page--;
                this.getBlogVideos();
            } else this.blogVideos.page = 1;
        },





        getBlogVideos: function () {
            this.blogVideos.animate = true;
            this.$http.get('/wp-json/wp/v2/video?per_page=3&page=' + this.blogVideos.page).then(function (r) {

                var allPostX = '';
                if (typeof r.headers.map['x-wp-totalpages'] == 'undefined') allPostX = r.headers.map['X-WP-TotalPages'][0];
                else allPostX = r.headers.map['x-wp-totalpages'][0];

                this.blogVideos.video = r.body;
                this.blogVideos.totalBlogItems = allPostX;
                console.log(this.blogVideos.video);
                var that = this;
                setTimeout(function () {
                    that.blogVideos.animate = false;
                }, 1000);
            }, function (r) {
                console.log(r);
            });
        },




    },



});




if(vacanciesPageEl) var vacanciesPage = new Vue({
    el: '#vacanciesPage',
    http: httpP,
    data: {
        message: 'Hello Vue!',
        vacanceList: [],
        activ: 0,
        body: {
            title: '',
            text : '',
            img  : ''
        },
        animate: false

    },


    mounted: function () {
        this.start();
        this.getVacancies();
    },

    methods: {
        start: function () {
            console.log('start');
        },
        getVacancies: function () {
            this.$http.get('/wp-json/wp/v2/vacance?orderby=date').then(function (r) {
                this.vacanceList = r.body;
                this.selectItem();
            }, function (r) {
                console.log(r);
            });

        },
        selectItem: function () {


            this.body.title = this.vacanceList[this.activ].title.rendered;
            this.body.text = this.vacanceList[this.activ].content.rendered;
            this.body.img = this.vacanceList[this.activ].images.thumbnail;
        },
        setActiv: function (id) {
            this.activ = id;
            this.animate = true;
            var that = this;
            setTimeout(function () {
                that.animate = false;
            }, 500);

            this.selectItem();
        },
    },




    filters: {


    },


});


if(blogPageEl) var app = new Vue({
    el: '#blogPage',
    http: httpP,
    data: {
        message: 'Hello Vue!',
        blogCat: [],
        blogItems: [],
        page   : 1,
        totalBlogItems: 1,
        animate: false,
        pageY : 0,
        blogItemsNai: {
            post          :[],
            page          : 1,
            totalBlogItems: 1,
            animate       : false,
            orderby       : 'comment_count',
        },
        blogVideos: {
            video          :[],
            page          : 1,
            totalBlogItems: 1,
            animate       : false,
        },
        blogAllCatItems: {
            CatItems: [],
            catCountCur: 2,
            catCoun: 0,
            catCount: 0,
            catPostCount: 3,
            rang: 0,
        }


    },
    components: {

    },


    mounted: function () {
        this.getBlogItems();
        this.getBlogCat();
        this.getBlogItemsNai();
        this.getBlogVideos();
        this.handleScroll();

        this.loadStartCat();
    },

    methods: {

        sortItemsToCat: function (catID, orderby) {


            this.blogAllCatItems.CatItems[catID]['orderby'] = orderby;
            this.blogAllCatItems.CatItems[catID].page = 1;
            this.blogAllCatItems.CatItems[catID]['animate']  = true;
            this.$http.get('/wp-json/wp/v2/article?blogs=' + this.blogCat[catID].id + '&per_page='+ this.blogAllCatItems.catPostCount +'&page='+this.blogAllCatItems.CatItems[catID].page+'&orderby='+this.blogAllCatItems.CatItems[catID]['orderby']+'&order=desc').then(function (r) {

                var that = this;
                setTimeout(function () {
                    that.blogAllCatItems.CatItems[catID]['posts']    = r.body;
                    that.blogAllCatItems.CatItems[catID]['animate'] = false;
                    that.blogAllCatItems.rang--;
                }, 1000);

                that.blogAllCatItems.rang++;
            }, function (r) {
                console.log(r);
            });


        },
        getNextBlogItemsToCat: function (catID) {

            if(this.blogAllCatItems.CatItems[catID].page < this.blogAllCatItems.CatItems[catID]['countAll']) {
                this.blogAllCatItems.CatItems[catID].page++;
                this.blogAllCatItems.CatItems[catID]['animate'] = true;
                this.$http.get('/wp-json/wp/v2/article?blogs=' + this.blogCat[catID].id + '&per_page=' + this.blogAllCatItems.catPostCount + '&page=' + this.blogAllCatItems.CatItems[catID].page + '&orderby=' + this.blogAllCatItems.CatItems[catID]['orderby'] + '&order=desc').then(function (r) {

                    var that = this;
                    setTimeout(function () {
                        that.blogAllCatItems.CatItems[catID]['posts'] = r.body;
                        that.blogAllCatItems.CatItems[catID]['animate'] = false;
                        that.blogAllCatItems.rang--;
                    }, 1000);

                    that.blogAllCatItems.rang++;
                }, function (r) {
                    console.log(r);
                });
            }
        },
        getPrevBlogItemsToCat: function (catID) {
            if(this.blogAllCatItems.CatItems[catID].page > 1) {
                this.blogAllCatItems.CatItems[catID].page--;
                this.blogAllCatItems.CatItems[catID]['animate'] = true;
                this.$http.get('/wp-json/wp/v2/article?blogs=' + this.blogCat[catID].id + '&per_page=' + this.blogAllCatItems.catPostCount + '&page=' + this.blogAllCatItems.CatItems[catID].page + '&orderby=' + this.blogAllCatItems.CatItems[catID]['orderby'] + '&order=desc').then(function (r) {

                    var that = this;
                    setTimeout(function () {
                        that.blogAllCatItems.CatItems[catID]['posts'] = r.body;
                        that.blogAllCatItems.CatItems[catID]['animate'] = false;
                        that.blogAllCatItems.rang--;
                    }, 1000);

                    that.blogAllCatItems.rang++;
                }, function (r) {
                    console.log(r);
                });

            }
        },


        loadStartCat: function () {
            var that = this;

            setTimeout(function () {
                that.blogAllCatItems.catCount = that.blogCat.length;
                that.getBlogItemsToCat();

            }, 3000);



        },


        getBlogItemsToCat: function () {


            if (this.blogCat[this.blogAllCatItems.catCoun] && (this.blogAllCatItems.catCoun < this.blogAllCatItems.catCountCur))

                this.$http.get('/wp-json/wp/v2/article?blogs=' + this.blogCat[this.blogAllCatItems.catCoun].id + '&per_page='+ this.blogAllCatItems.catPostCount +'&page=1&orderby=comment_count&order=desc').then(function (r) {
                    var ar = [];
                    var allPostX = '';
                    if (typeof r.headers.map['x-wp-totalpages'] == 'undefined') allPostX = r.headers.map['X-WP-TotalPages'][0];
                    else allPostX = r.headers.map['x-wp-totalpages'][0];

                    ar['catname']  = this.blogCat[this.blogAllCatItems.catCoun].name ;
                    ar['posts']    = r.body;
                    ar['countAll'] = allPostX;
                    ar['page']  = 1;
                    ar['orderby']  = 'comment_count';
                    ar['animate']  = false;

                    this.blogAllCatItems.CatItems.push(ar);

                    this.blogAllCatItems.catCoun++;


                       return this.getBlogItemsToCat();
                }, function (r) {
                    console.log(r);
                });

        },

        addBlogItemsToCat: function () {
            this.blogAllCatItems.catCountCur++;
            this.getBlogItemsToCat();
        },

        handleScroll: function (event) {
            if (event){
                //this.pageY = event.pageY,
               // console.log(event.target);
            } else {
               // this.pageY = 0,
                  //  console.log(111);
            }

        },

        come: function (elem) {
        var docViewTop = $(window).scrollTop(),
        docViewBottom = docViewTop + $(window).height(),
        elemTop = $(elem).offset().top,
        elemBottom = elemTop + $(elem).height();
          return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        },

        getNextBlogVideos: function () {
            if(this.blogVideos.page < this.blogItemsNai.totalBlogItems) {
                this.blogVideos.page++;
                this.getBlogVideos();
            } else this.blogVideos.page = this.blogItemsNai.totalBlogItems;
        },
        getPrevBogVideos: function () {
            if(this.blogVideos.page > 1) {
                this.blogVideos.page--;
                this.getBlogVideos();
            } else this.blogVideos.page = 1;
        },





        getBlogVideos: function () {
            this.blogVideos.animate = true;
            this.$http.get('/wp-json/wp/v2/video?per_page=3&page=' + this.blogVideos.page).then(function (r) {

                var allPostX = '';
                if (typeof r.headers.map['x-wp-totalpages'] == 'undefined') allPostX = r.headers.map['X-WP-TotalPages'][0];
                else allPostX = r.headers.map['x-wp-totalpages'][0];

                this.blogVideos.video = r.body;
                this.blogVideos.totalBlogItems = allPostX;
                var that = this;
                setTimeout(function () {
                    that.blogVideos.animate = false;
                }, 1000);
            }, function (r) {
                console.log(r);
            });
        },


        getBlogItemsNai: function () {
            this.blogItemsNai.animate = true;
            this.$http.get('/wp-json/wp/v2/article?orderby='+ this.blogItemsNai.orderby +'&order=desc&per_page=6&page=' + this.blogItemsNai.page).then(function (r) {
                var allPostX = '';
                if (typeof r.headers.map['x-wp-totalpages'] == 'undefined') allPostX = r.headers.map['X-WP-TotalPages'][0];
                else allPostX = r.headers.map['x-wp-totalpages'][0];

                this.blogItemsNai.post = r.body;
                this.blogItemsNai.totalBlogItems = allPostX;
                var that = this;
                setTimeout(function () {
                    that.blogItemsNai.animate = false;
                }, 1000);
            }, function (r) {
                console.log(r);
            });
        },
        getNextPageNai: function () {
            if(this.blogItemsNai.page < this.blogItemsNai.totalBlogItems) {
                this.blogItemsNai.page++;
                this.getBlogItemsNai();
            } else this.blogItemsNai.page = this.blogItemsNai.totalBlogItems;
        },
        getPrevPageNai: function () {
            if(this.blogItemsNai.page > 1) {
                this.blogItemsNai.page--;
                this.getBlogItemsNai();
            } else this.blogItemsNai.page = 1;
        },
       sortPageNai: function (orderby) {
            this.blogItemsNai.orderby = orderby;
            this.getBlogItemsNai();
       },


        getBlogCat: function () {

            this.$http.get('/wp-json/wp/v2/blogs?orderby=id').then(function (r) {
                this.blogCat = r.body;
                this.catCountCur = this.blogCat.length;

            }, function (r) {
                console.log(r);
            });

        },
        getBlogItems: function () {
            this.animate = true;
            this.$http.get('/wp-json/wp/v2/article?per_page=5&page=' + this.page).then(function (r) {

                var allPostX = '';
                if (typeof r.headers.map['x-wp-totalpages'] == 'undefined') allPostX = r.headers.map['X-WP-TotalPages'][0];
                else allPostX = r.headers.map['x-wp-totalpages'][0];

                this.blogItems = r.body;
                this.totalBlogItems = allPostX;
                var that = this;
                setTimeout(function () {
                    that.animate = false;
                }, 1000);
            }, function (r) {
                console.log(r);
            });
        },
        getNextPage: function () {
            if(this.page < this.totalBlogItems) {
                this.page++;
                this.getBlogItems();
            } else this.page = this.totalBlogItems;
        },
        getPrevPage: function () {
            if(this.page > 1) {
                this.page--;
                this.getBlogItems();
            } else this.page = 1;

        },
        isVisible: function (that) {

            console.log(that);
        }
    },




    filters: {
        json: function (value)  {
            return JSON.stringify(value, null, 2)
        },

    },
    created: function () {
        window.addEventListener('scroll', this.handleScroll);
    },
    destroyed: function () {
        window.removeEventListener('scroll', this.handleScroll);
    }

});





// else


/*     My Javascript      */
