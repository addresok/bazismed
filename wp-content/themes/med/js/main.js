!function(e,t){"use strict";"function"==typeof define&&define.amd?define(function(){return t(e)}):"object"==typeof exports?module.exports=t:e.scrollProgress=t(e)}(this,function(){"use strict";var e,t,o,n=document.body,i=0,r=!1,s={bottom:!0,color:"#000000",height:"5px",styles:!0,prefix:"progress",events:!0},l=function(){e=document.createElement("div"),t=document.createElement("div"),e.id=s.prefix+"-wrapper",t.id=s.prefix+"-element",e.appendChild(t),n.appendChild(e)},c=function(e){if("object"==typeof e)for(var t in s)"undefined"!=typeof e[t]&&(s[t]=e[t])},d=function(){t.style.width="0",e.style.width="100%",s.styles&&(t.style.backgroundColor=s.color,t.style.height=s.height,e.style.position="fixed",e.style.left="0",s.bottom?e.style.bottom="0":e.style.top="0")},f=function(e){if(r)throw new Error("scrollProgress has already been set!");e&&c(e),l(),d(),p(),s.events&&(window.onscroll=u,window.onresize=p),r=!0},u=function(){try{var e=window.scrollY||window.pageYOffset||document.documentElement.scrollTop;i=e/o*100,t.style.width=i+"%"}catch(n){console.error(n)}},p=function(){o=h(),u()},h=function(){return n.scrollHeight-(window.innerHeight||document.documentElement.clientHeight)};return{set:f,trigger:u,update:p}});
/*! WOW - v1.1.3 - 2016-05-06
 * Copyright (c) 2016 Matthieu Aussaguel;*/(function(){var a,b,c,d,e,f=function(a,b){return function(){return a.apply(b,arguments)}},g=[].indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(b in this&&this[b]===a)return b;return-1};b=function(){function a(){}return a.prototype.extend=function(a,b){var c,d;for(c in b)d=b[c],null==a[c]&&(a[c]=d);return a},a.prototype.isMobile=function(a){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a)},a.prototype.createEvent=function(a,b,c,d){var e;return null==b&&(b=!1),null==c&&(c=!1),null==d&&(d=null),null!=document.createEvent?(e=document.createEvent("CustomEvent"),e.initCustomEvent(a,b,c,d)):null!=document.createEventObject?(e=document.createEventObject(),e.eventType=a):e.eventName=a,e},a.prototype.emitEvent=function(a,b){return null!=a.dispatchEvent?a.dispatchEvent(b):b in(null!=a)?a[b]():"on"+b in(null!=a)?a["on"+b]():void 0},a.prototype.addEvent=function(a,b,c){return null!=a.addEventListener?a.addEventListener(b,c,!1):null!=a.attachEvent?a.attachEvent("on"+b,c):a[b]=c},a.prototype.removeEvent=function(a,b,c){return null!=a.removeEventListener?a.removeEventListener(b,c,!1):null!=a.detachEvent?a.detachEvent("on"+b,c):delete a[b]},a.prototype.innerHeight=function(){return"innerHeight"in window?window.innerHeight:document.documentElement.clientHeight},a}(),c=this.WeakMap||this.MozWeakMap||(c=function(){function a(){this.keys=[],this.values=[]}return a.prototype.get=function(a){var b,c,d,e,f;for(f=this.keys,b=d=0,e=f.length;e>d;b=++d)if(c=f[b],c===a)return this.values[b]},a.prototype.set=function(a,b){var c,d,e,f,g;for(g=this.keys,c=e=0,f=g.length;f>e;c=++e)if(d=g[c],d===a)return void(this.values[c]=b);return this.keys.push(a),this.values.push(b)},a}()),a=this.MutationObserver||this.WebkitMutationObserver||this.MozMutationObserver||(a=function(){function a(){"undefined"!=typeof console&&null!==console&&console.warn("MutationObserver is not supported by your browser."),"undefined"!=typeof console&&null!==console&&console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")}return a.notSupported=!0,a.prototype.observe=function(){},a}()),d=this.getComputedStyle||function(a,b){return this.getPropertyValue=function(b){var c;return"float"===b&&(b="styleFloat"),e.test(b)&&b.replace(e,function(a,b){return b.toUpperCase()}),(null!=(c=a.currentStyle)?c[b]:void 0)||null},this},e=/(\-([a-z]){1})/g,this.WOW=function(){function e(a){null==a&&(a={}),this.scrollCallback=f(this.scrollCallback,this),this.scrollHandler=f(this.scrollHandler,this),this.resetAnimation=f(this.resetAnimation,this),this.start=f(this.start,this),this.scrolled=!0,this.config=this.util().extend(a,this.defaults),null!=a.scrollContainer&&(this.config.scrollContainer=document.querySelector(a.scrollContainer)),this.animationNameCache=new c,this.wowEvent=this.util().createEvent(this.config.boxClass)}return e.prototype.defaults={boxClass:"wow",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:null,scrollContainer:null},e.prototype.init=function(){var a;return this.element=window.document.documentElement,"interactive"===(a=document.readyState)||"complete"===a?this.start():this.util().addEvent(document,"DOMContentLoaded",this.start),this.finished=[]},e.prototype.start=function(){var b,c,d,e;if(this.stopped=!1,this.boxes=function(){var a,c,d,e;for(d=this.element.querySelectorAll("."+this.config.boxClass),e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.all=function(){var a,c,d,e;for(d=this.boxes,e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.boxes.length)if(this.disabled())this.resetStyle();else for(e=this.boxes,c=0,d=e.length;d>c;c++)b=e[c],this.applyStyle(b,!0);return this.disabled()||(this.util().addEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().addEvent(window,"resize",this.scrollHandler),this.interval=setInterval(this.scrollCallback,50)),this.config.live?new a(function(a){return function(b){var c,d,e,f,g;for(g=[],c=0,d=b.length;d>c;c++)f=b[c],g.push(function(){var a,b,c,d;for(c=f.addedNodes||[],d=[],a=0,b=c.length;b>a;a++)e=c[a],d.push(this.doSync(e));return d}.call(a));return g}}(this)).observe(document.body,{childList:!0,subtree:!0}):void 0},e.prototype.stop=function(){return this.stopped=!0,this.util().removeEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().removeEvent(window,"resize",this.scrollHandler),null!=this.interval?clearInterval(this.interval):void 0},e.prototype.sync=function(b){return a.notSupported?this.doSync(this.element):void 0},e.prototype.doSync=function(a){var b,c,d,e,f;if(null==a&&(a=this.element),1===a.nodeType){for(a=a.parentNode||a,e=a.querySelectorAll("."+this.config.boxClass),f=[],c=0,d=e.length;d>c;c++)b=e[c],g.call(this.all,b)<0?(this.boxes.push(b),this.all.push(b),this.stopped||this.disabled()?this.resetStyle():this.applyStyle(b,!0),f.push(this.scrolled=!0)):f.push(void 0);return f}},e.prototype.show=function(a){return this.applyStyle(a),a.className=a.className+" "+this.config.animateClass,null!=this.config.callback&&this.config.callback(a),this.util().emitEvent(a,this.wowEvent),this.util().addEvent(a,"animationend",this.resetAnimation),this.util().addEvent(a,"oanimationend",this.resetAnimation),this.util().addEvent(a,"webkitAnimationEnd",this.resetAnimation),this.util().addEvent(a,"MSAnimationEnd",this.resetAnimation),a},e.prototype.applyStyle=function(a,b){var c,d,e;return d=a.getAttribute("data-wow-duration"),c=a.getAttribute("data-wow-delay"),e=a.getAttribute("data-wow-iteration"),this.animate(function(f){return function(){return f.customStyle(a,b,d,c,e)}}(this))},e.prototype.animate=function(){return"requestAnimationFrame"in window?function(a){return window.requestAnimationFrame(a)}:function(a){return a()}}(),e.prototype.resetStyle=function(){var a,b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],e.push(a.style.visibility="visible");return e},e.prototype.resetAnimation=function(a){var b;return a.type.toLowerCase().indexOf("animationend")>=0?(b=a.target||a.srcElement,b.className=b.className.replace(this.config.animateClass,"").trim()):void 0},e.prototype.customStyle=function(a,b,c,d,e){return b&&this.cacheAnimationName(a),a.style.visibility=b?"hidden":"visible",c&&this.vendorSet(a.style,{animationDuration:c}),d&&this.vendorSet(a.style,{animationDelay:d}),e&&this.vendorSet(a.style,{animationIterationCount:e}),this.vendorSet(a.style,{animationName:b?"none":this.cachedAnimationName(a)}),a},e.prototype.vendors=["moz","webkit"],e.prototype.vendorSet=function(a,b){var c,d,e,f;d=[];for(c in b)e=b[c],a[""+c]=e,d.push(function(){var b,d,g,h;for(g=this.vendors,h=[],b=0,d=g.length;d>b;b++)f=g[b],h.push(a[""+f+c.charAt(0).toUpperCase()+c.substr(1)]=e);return h}.call(this));return d},e.prototype.vendorCSS=function(a,b){var c,e,f,g,h,i;for(h=d(a),g=h.getPropertyCSSValue(b),f=this.vendors,c=0,e=f.length;e>c;c++)i=f[c],g=g||h.getPropertyCSSValue("-"+i+"-"+b);return g},e.prototype.animationName=function(a){var b;try{b=this.vendorCSS(a,"animation-name").cssText}catch(c){b=d(a).getPropertyValue("animation-name")}return"none"===b?"":b},e.prototype.cacheAnimationName=function(a){return this.animationNameCache.set(a,this.animationName(a))},e.prototype.cachedAnimationName=function(a){return this.animationNameCache.get(a)},e.prototype.scrollHandler=function(){return this.scrolled=!0},e.prototype.scrollCallback=function(){var a;return!this.scrolled||(this.scrolled=!1,this.boxes=function(){var b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],a&&(this.isVisible(a)?this.show(a):e.push(a));return e}.call(this),this.boxes.length||this.config.live)?void 0:this.stop()},e.prototype.offsetTop=function(a){for(var b;void 0===a.offsetTop;)a=a.parentNode;for(b=a.offsetTop;a=a.offsetParent;)b+=a.offsetTop;return b},e.prototype.isVisible=function(a){var b,c,d,e,f;return c=a.getAttribute("data-wow-offset")||this.config.offset,f=this.config.scrollContainer&&this.config.scrollContainer.scrollTop||window.pageYOffset,e=f+Math.min(this.element.clientHeight,this.util().innerHeight())-c,d=this.offsetTop(a),b=d+a.clientHeight,e>=d&&b>=f},e.prototype.util=function(){return null!=this._util?this._util:this._util=new b},e.prototype.disabled=function(){return!this.config.mobile&&this.util().isMobile(navigator.userAgent)},e}()}).call(this);

$(document).ready(function () {
    new WOW().init();
    activateDoctor();
    advantageTextChacge();





    jQuery(document).ready(function () {
        jQuery('#scrollup').mouseover(function () {
            jQuery(this).animate({opacity: 0.65}, 100);
        }).mouseout(function () {
            jQuery(this).animate({opacity: 1}, 100);
        }).click(function () {
            window.scroll(0, 0);
            return false;
        });

        jQuery(window).scroll(function () {
            if (jQuery(document).scrollTop() > 0) {
                jQuery('#scrollup').fadeIn('fast');
            } else {
                jQuery('#scrollup').fadeOut('fast');
            }
        });
        jQuery(document).on('click', '.head .lang .active,.head .lang .arrow-lang', function (event) {
            event.preventDefault();
            jQuery('.head .lang').toggleClass('active');
        });
        jQuery(document).on('click', '.head-mob .lang .active,.head-mob .lang .arrow-lang', function (event) {
            event.preventDefault();
            jQuery('.head-mob .lang').toggleClass('active');
        });


    });
    aboutVideo();
    aboutText();

    openPopUp();
    closePopUp();

    openVacancy();
    ckeckVacancy();

    openTime();

    openMenuMob();

/*    $('.advice_form input[type="radio"]').on('change',function () {
        if($('input[type="radio"]:checked').val() == 'Кардіолог'){
            $('.cardiology').addClass('active');
            $('.proctology').removeClass('active');
        }else if($('input[type="radio"]:checked').val() == 'Флеболог'){
            $('.proctology').addClass('active');
            $('.cardiology').removeClass('active');
        }
    });*/

    function setAcivePiceFromUrl() {
        _gets =window.location.search.substring(1).split('=');
       if (_gets[0]=='data'){
        items = $('.priceBlock');
        for (var key in items) {
            if(items[key].id == _gets[1]){

                if (!($('#'+_gets[1]).find('.heades').hasClass('showed'))) {
                    $('.heades').removeClass('showed');
                    var table = $('#'+_gets[1]).children('.table').clone();
                    var activeTable = $('.activeTable');
                    $('#'+_gets[1]).find('.heades').addClass('showed');
                    activeTable.html(table);
                }
             }
        }
    }
    }
    if($(document).width() <=641) {
        $('.last_articles_slider').owlCarousel({
            autoPlay: true,
            navigation: false,
            itemElement: 'last_articles_slider',
            loop: true,
            items: 1,
            dots: true
        });
    }
    setAcivePiceFromUrl();
    $('.priceBlock').on('click', function () {
        if (!($(this).find('.heades').hasClass('showed'))) {
            $('.heades').removeClass('showed');
            var table = $(this).children('.table').clone();
            var activeTable = $('.activeTable');
            $(this).find('.heades').addClass('showed');
            activeTable.html(table);
        }
    });

    $('.ars1').on('click', function () {
        $('.other-text-surgery.one').slideToggle('slow');
        // $('.two').addClass('closed2');
        $(this).toggleClass('active');
        // $('.ars2').removeClass('active');
    });
    $(document).on('click', '.ars2', function () {
        $('.other-text-surgery.two').slideToggle('slow').toggleClass('closed2');
        // $('.one').addClass('closed1');
        $(this).toggleClass('active');
        // $('.ars1').removeClass('active');
    });
/*    var lastPageNumber = $('#blog-more-button').data('pages'),
        currentPageNumber = 1;

    $('#blog-more-button').on('click', function () {
        if(currentPageNumber < lastPageNumber) {
            currentPageNumber++;
            $("#postLoad").load("http://bazismed.local/wp-content/themes/med/ajax_blog.php", {page: currentPageNumber});
            $(".blog").append($("#postLoad .post_block"));
        }
    });*/
    // $(window).load(windowSize);
    //     function windowSize(){
    //         if ($(window).width() >= '768'){
    //             $('.menu li:first-child a').removeClass('ss');
    //         } else {
    //             $('.menu li:first-child a').addClass('ss');
    //         } 
    //     }

    $('.owl-carousel').owlCarousel({
        itemsCustom: [
            [0, 2],
            [450, 2],
            [600, 2],
            [700, 3],
            [1000, 3],
            [1100, 4],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        navigation: true,
        navigationText: ["<div class='arrow-ser-left'></div>", "<div class='arrow-ser-right'></div>"],
        mouseDrag: false
    });

    $('.main_slider').owlCarousel({
        pagination: false,
        autoPlay: false,
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
        loop:true,
        lazyLoad: true,

        autoHeight: true
    });


        $(window).scroll(function(){
            if ($(this).scrollTop() > 400) {
                $('.back-top').fadeIn();
            } else {
                $('.back-top').fadeOut();
            }
        });

        $('.back-top').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });

    $('.test-carousel ').owlCarousel({
        navigation: true,
        navigationText: ["<div class='test-ser-left' ><img src='/wp-content/themes/med/img/arrow-left.png'></div>", "<div class='test-ser-right'><img src='/wp-content/themes/med/img/arrow-right.png'></div>"],
        loop:true,
        margin:10,
        items:1,
        autoHeight:true
    });
    jQuery( ".testme_answer").change(function () {

        var answered = jQuery('.testme_button').parents(".testme_area").find(":radio:checked").length;
        var questions = jQuery('.testme_button').parents(".testme_area").find(".testme_question").length;

        if (answered==questions ) {
            jQuery( ".testme_button").slideToggle(0);


        }
        else{
            var carousel = jQuery('.test-carousel');
            carousel.trigger('owl.next',[2200]);
        }

    });



    $('.priceSlider').owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 2],
            [1000, 2],
            [1100, 2],
            [1200, 3],
            [1400, 3],
            [1600, 3]
        ],
        navigation: true,
        navigationText: ["<div class='arrow-ser-left'></div>", "<div class='arrow-ser-right'></div>"]
    });

    $('#serv').owlCarousel({

        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 3],
            [1000, 3],
            [1100, 6],
            [1200, 6],
            [1400, 6],
            [1600, 6]
        ],
        navigation: true,
        navigationText: ["<div class='arrow-ser-left'></div>", "<div class='arrow-ser-right'></div>"]
    });

    $('.slid').owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
    });
    $('#datepicker').on('click', function () {
        var offset = $('#datepicker').offset();
         $("#ui-datepicker-div").css('top',offset.top - 15);


    });


    function A(active) {
        if (active == true) {
            var hash = window.location.hash;

            if (hash) {
                $('.menu a , .dropdown-menus a').removeClass('active');
                $('a[href="+hash+"]').addClass('active');
            }
        }

        function B(e) {
            e.preventDefault();
            var link = $(this);
            if (hash) {
                $('.menu a , .dropdown-menus a').removeClass('active');
                link.addClass('active');
            }
            setTimeout(function () {
                $('html, body').animate({
                    scrollTop: $(link.attr('href')).offset().top
                }, 1000);
            }, 600);
            $('.dropdown-menus').slideTop('slow');

        }

        function C(e) {
            e.preventDefault();
        }

        $("a[href^='#']").click(C);
        $(".butons").click(B);
    }

    A(true);
    $(document).on('click', '.dropdown-menus a', function (event) {
        $('.dropdown-menus').slideToggle('slow');
    });
    $('.tabs a').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('.tabs a').removeClass('active');
        $('.tab-content').removeClass('active');
        $('.block').removeClass('opacity');

        $(this).addClass('active');
        $("#" + tab_id).addClass('active');
        $(this).parent('.block').addClass('opacity');
    });

    $('.news-mob .js-btn').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('.news-mob .js-btn').removeClass('active');
        $('.block').removeClass('active');

        $(this).addClass('active');
        $("#" + tab_id).addClass('active');
    });

    setTimeout(function () {
        $('.wp-super-snow-flake').remove();
    }, 7000);
    //Плавна прокрутка по якорям
    $('.menu .menu-item a[href*="#"]').click(function(){
        var id  = $(this).attr('href'),
            top = $("#" + id.split("#")[1]).offset().top;
        $('body, html').animate({scrollTop: top - 80}, 1000);
    });
    //Скріпт з footer.php
    $('#owl-demo').owlCarousel({
        itemsCustom : [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
        navigation : true,
        autoPlay: 3000,
        navigationText: ["<img src='/wp-content/themes/med/img/arrow-left.png'>","<img src='/wp-content/themes/med/img/arrow-right.png'>"]
    });

    $('.hir').owlCarousel({
        itemsCustom : [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
        navigation : true,

        navigationText: ["<div class='div' style='background: url('/wp-content/themes/med/img/hir-left.png');    width: 36px; height: 64px;'></div>","<div class='div' style='background: url('/wp-content/themes/med/img/hir-right.png');    width: 36px; height: 64px;'></div>"]
    });

    $('.advice_doctor_slider').owlCarousel({
        itemsCustom : [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
        navigation : false,

        navigationText: ["<div class='div' style='background: url('/wp-content/themes/med/img/hir-left.png');    width: 36px; height: 64px;'></div>","<div class='div' style='background: url('/wp-content/themes/med/img/hir-right.png');    width: 36px; height: 64px;'></div>"]
    });

    $('a.tab').on('click', function(event) {
        event.preventDefault();
        $(this).children('i').toggleClass('active');
        $('.tab-wrap').removeClass('active');
        $(this).next().addClass('active');
        $('.tab-wrap.active').slideToggle('slow');
    });
    //Скріпт був на усіх сторінках
    $('.rew-cont').owlCarousel({
        items: 1,
        navigation: false,
        pagination: false,
        autoHeight : true,
        singleItem:true
    });
    rewSliderMob = $('.rew-cont').data('owlCarousel');
    $(document).on('click', '.contrl.prev', function(event) {
        console.log('prev clicked');
        rewSliderMob.prev();
    });
    $(document).on('click', '.contrl.nextss', function(event) {
        console.log('nextss clicked');
        rewSliderMob.next();
    });
    //Скріпт з index-ru.php і index-ua.php
    $('.video-link').on('click', function (event) {
        event.preventDefault();
        $('.video-mob-wrap').toggleClass('hide-right');
        setTimeout($('.video-mob-cont').toggleClass('show-left'), 250)
        // $('.video-mob-cont').toggleClass('show-left');
    });
    $('.about-video-link').on('click', function (event) {
        event.preventDefault();
        $('.video-mob-cont').removeClass('show-left');
        setTimeout(function () {
            $('.video-mob-wrap').removeClass('hide-right')
        }, 260);

    });
    $('.about-video-mob .video-mob-wrap .controls .up').on('click', function (event) {
        event.preventDefault();
        var mt;
        mt = $('.about-text p').css('margin-top');
        mt = parseInt(mt);
        if (mt < 0) {
            $(".about-text p").animate({marginTop: '+=20px'}, 0);
        }
    });
    $('.about-video-mob .video-mob-wrap .controls .down').on('click', function (event) {
        event.preventDefault();
        var height;
        height = $(".about-text p").css('height');
        height = parseInt(height);
        var mt;
        mt = $('.about-text p').css('margin-top');
        mt = parseInt(mt);
        mt = -mt;
        var check = height - mt - 170;
        console.log(check);
        if (check > 0) {
            $(".about-text p").animate({marginTop: '-=20px'}, 0);
        }

    });
    //
    if ($(window).width() < 767) {
        var elems = $("#unikal_one .fuc-block");
        var wrapper = $('<div class="item" />');
        var pArrLen = elems.length;
        if (pArrLen > 2) {
            for (var i = 0; i < pArrLen; i += 2) {
                elems.filter(':eq(' + i + '),:eq(' + (i + 1) + ')').wrapAll(wrapper);
            }
            ;
        }
        var elems1 = $("#unikal_two .fuc-block");
        var wrapper1 = $('<div class="item" />');
        var pArrLen1 = elems1.length;
        if (pArrLen1 > 2) {
            for (var i = 0; i < pArrLen; i += 2) {
                elems.filter(':eq(' + i + '),:eq(' + (i + 1) + ')').wrapAll(wrapper);
            }
            ;
        }
        setTimeout(function () {
            $('.owl-slider-mobile').owlCarousel({
                singleItem: true,
                paginationNumbers: true,
                navigation: true,
                navigationText: ["<img src='/wp-content/themes/med/img/prev-mobile-slider.png'>", "<img src='/wp-content/themes/med/img/next-mobile-slider.png'>"]
            });
        }, 250)
    }

    //з хедера
    $( function() {
        $( "#datepicker" ).datepicker();
        $( "#datepicker1" ).datepicker();
        /* Russian (UTF-8) initialisation for the jQuery UI date picker plugin. */
        /* Written by Andrew Stromnov (stromnov@gmail.com). */
        ( function( factory ) {
            if ( typeof define === "function" && define.amd ) {

                // AMD. Register as an anonymous module.
                define( [ "../widgets/datepicker" ], factory );
            } else {

                // Browser globals
                factory( jQuery.datepicker );
            }
        }( function( datepicker ) {

            datepicker.regional.ru = {
                closeText: "Закрыть",
                prevText: "&#x3C;Пред",
                nextText: "След&#x3E;",
                currentText: "Сегодня",
                monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь",
                    "Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
                monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн",
                    "Июл","Авг","Сен","Окт","Ноя","Дек" ],
                dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
                dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
                dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
                weekHeader: "Нед",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: "" };
            datepicker.setDefaults( datepicker.regional.ru );

            return datepicker.regional.ru;

        } ) );
    } );
    /* Partners carousel */
    $('#partners-owl').owlCarousel({
        navigation: true,
        navigationText: ["<div class='arrow-ser-left'></div>", "<div class='arrow-ser-right'></div>"]
    });
    /* Блог Ajax підгрузка контенту */
/*    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() && !inProgress) {
            ajaxBlogLoader();
        }
    });*/
    /*$('#blog-more-button').click(ajaxBlogLoader);*/
    /**/

    $('.filter form input:checkbox:checked').each(function () {
        categories.push($(this).val());
    });

    $('.filter ul li label').click(function () {
        $(this).find('input').click();
    });
    $('.filter .filter-button').click(function () {
        var categoriesNew = [];
        $('.filter form input:checkbox:checked').each(function () {
            categoriesNew.push($(this).val());
        });
        var url = location.href.split('?')[0];
        if (categoriesNew.length > 0) {
            url = url + "?" + categoriesNew.join('_');
        }
        document.location.href = url;

        return false;
    });
    /* Консультація popup */
    $('a#consult-call').fancybox({
        padding : 0,
        autoSize: true,
        wrapCSS: "consult-fancy",
        closeBtn: false,
        afterClose: function () {
            $.cookie('show_consult_popup', "false", { expires: 1 });
        }
    });
    // setTimeout(
    //     function(){
    //         if ($.cookie('show_consult_popup') != "false") {
    //             $('a#consult-call').click();
    //         }
    //     }, 30000
    // );
    $('.popup-close').click(function () {
        $('.fancybox-overlay').click();
    });
    $('#consult-cf-call').fancybox({
        wrapCSS: "consult-cf-fancy",
        closeBtn: false
    });
});


/* Блог Ajax підгрузка контенту */
/*var inProgress = false;
var startFrom = 2;
var iblog = 1;
var nblog = 1;
var categories = [];

function ajaxBlogLoader() {
    $.ajax({
        url: HTTP + '/ajax/blog.loader.php',
        method: 'POST',
        data: {"blog_page" : startFrom, "categories" : categories},
        beforeSend: function() {
            inProgress = true;
        }
    }).done(function(data) {
        data = jQuery.parseJSON(data);
        if (data.length > 0) {
            $.each(data, function(index, data) {
                $el1 = $('<div class="blog-tile">' +
                    '<h4><a href="' + data.url + '">' + data.post_title + '</a></h4>' +
                    '<a href="' + data.url + '"><img src="' + data.image + '" alt="' + data.post_title + '" /></a>' +
                    '<span>' + data.post_date + '</span>' +
                    '</div>');
                $el2 = $('<div class="blog-tile">' +
                    '<h4><a href="' + data.url + '">' + data.post_title + '</a></h4>' +
                    '<a href="' + data.url + '"><img src="' + data.image + '" alt="' + data.post_title + '" /></a>' +
                    '<span>' + data.post_date + '</span>' +
                    '</div>');
                $el3 = $('<div class="blog-tile">' +
                    '<h4><a href="' + data.url + '">' + data.post_title + '</a></h4>' +
                    '<a href="' + data.url + '"><img src="' + data.image + '" alt="' + data.post_title + '" /></a>' +
                    '<span>' + data.post_date + '</span>' +
                    '</div>');

                $el1.hide();
                $el2.hide();
                $el3.hide();
                if (iblog == 1) {
                    $('#col31').append($el1);
                }
                if (iblog == 2) {
                    $('#col32').append($el1);
                }
                if (iblog == 3) {
                    $('#col33').append($el1);
                }
                if (iblog == 3) {
                    iblog = 1;
                } else {
                    iblog += 1;
                }
                if (nblog == 1) {
                    $('#col22').append($el2);
                }
                if (nblog == 2) {
                    $('#col21').append($el2);
                }
                if (nblog == 2) {
                    nblog = 1;
                } else {
                    nblog += 1;
                }
                $('#col11').append($el3);
                $el1.fadeIn('slow');
                $el2.fadeIn('slow');
                $el3.fadeIn('slow');
            });
            inProgress = false;
        }
        startFrom += 1;
    });
}*/

function aboutVideo() {
    $('.about .link').on('click', function () {
        $('.conta1').css('left', '100vw');
        $('.conta2').css('left', '0px');
    });
};

function aboutText() {
    $('.about .conta2 .link').on('click', function () {
        $('.conta1').css('left', '0px');
        $('.conta2').css('left', '-100vw');
    });
};
$(document).on('click', '.arrow-up', function () {
    var time = $('.timeCount').text();
    time = parseInt(time);
    if (time == 23) {
        $('.timeCount').text('00');
        $('.js-time input').val('00:30');
    } else if (time < 23) {
        time++;
        $('.timeCount').text(time);
        $('.js-time input').val(time + ':30');
    }
});

$(document).on('click', '.arrow-down', function () {
    var time = $('.timeCount').text();
    time = parseInt(time);
    if (time == 0) {
        $('.timeCount').text('23');
        $('.js-time input').val('23:30');
    } else if (time > 1) {
        time--;
        $('.timeCount').text(time);
        $('.js-time input').val(time + ':30');
    } else if (time == 1) {
        $('.timeCount').text('00');
        $('.js-time input').val('00:30');
    }
});

$(document).on('click', '.arrow-up', function () {
    var time = $('.timeCounts').text();
    time = parseInt(time);
    if (time == 23) {
        $('.timeCounts').text('00');
        $('.js-time input').val('00:30');
    } else if (time < 23) {
        time++;
        $('.timeCounts').text(time);
        $('.js-time input').val(time + ':30');
    }
});

$(document).on('click', '.arrow-down', function () {
    var time = $('.timeCounts').text();
    time = parseInt(time);
    if (time == 0) {
        $('.timeCounts').text('23');
        $('.js-time input').val('23:30');
    } else if (time > 1) {
        time--;
        $('.timeCounts').text(time);
        $('.js-time input').val(time + ':30');
    } else if (time == 1) {
        $('.timeCounts').text('00');
        $('.js-time input').val('00:30');
    }
});

function openPopUp() {
    $('.review').on('click', function () {
        //   $(window).load(windowSize);
        //     function windowSize(){
        //         if ($(window).width() >= '768'){
        //             $('html, body').animate({
        //                 scrollTop: $(".vacancy").offset().top
        //             }, 1500);
        //         } else {
        //             $('html, body').animate({
        //                 scrollTop: $(".vacancy").offset().top
        //             }, 1500);
        //         }
        //     }
        var windowWidthCheck = $('.windowSizeCheck').css('display');
        if (windowWidthCheck !== 'none') {
            $('html, body').animate({
                scrollTop: $(".vacancy").offset().top
            }, 1500);
        }
        else if (windowWidthCheck == 'none') {
            $('.bk-pop').css('display', 'block');
            $('.popUp').css('display', 'block');
            $('.dropdown-menus').removeClass('opened');
        }

    });
};
function closePopUp() {
    $('.bk-pop , .esc').on('click', function () {
        $('.bk-pop').css('display', 'none');
        $('.popUp').css('display', 'none');
    });
};

function openVacancy() {
    $('.js-vacancy').on('click', function () {
        $('.modal-box').show();
    });
};

function openMenuMob() {
    $(document).on('click', '.ico-menu', function () {
        $('.dropdown-menus').slideToggle('slow');
    });
};

function openTime() {
    $('.js-time').on('click', function () {
        $('.modal-box-time').show();
    });
};

function ckeckVacancy() {
    $(document).on('click', '.js-modal-vacancy a', function (event) {
        event.preventDefault();
        $('.modal-box').hide();
    });
    $('.js-modal-vacancy a').on('click', function () {
        var name = $(this).text();
        $(this).addClass('active');
        $('.js-vacancy input').val(name);
        $('.modal-box').hide();
    });
};
function activateDoctor() {
    // $('#nurs.nurs .col-senter .col-lg-4:first-of-type .block .open').trigger('click').addClass('active').parent('.block').addClass('opacity');
    // tab_id = $('#nurs.nurs .col-senter .col-lg-4:first-of-type .block .open').attr('data-tab');
    // $("#"+tab_id).addClass('active');
};

function advantageTextChacge() {
    $('.about-two .block').on('mouseover', function () {
        text = $(this).children('.text').text();
        $('.about-two .bottomer-text').text(text);
    });
};

$(document).mouseup(function (e) {
    e.preventDefault();
    var popup = $('.js-modal-vacancy');
    var time = $('.modal-box-time');

    if (!popup.is(e.target) && popup.has(e.target).length == 0) {
        popup.hide();
    }
    if (!time.is(e.target) && time.has(e.target).length == 0) {
        time.hide();
    }

});





//Preloader
window.onload = function() {
    $('#system-load').fadeOut('slow').remove();
    $('#main-container').show();

    var vidPl = document.getElementsByClassName('play');
    vidPl.onclick = function () {

    }

};





