

<div id="contact" class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="col-lg-12 ">
                    <div class="cont-1" itemtype="http://schema.org/MedicalOrganization">
                        <span  class="NameOrganization" itemprop="name">БазисМед</span>
                        <a href="/" class="logo"></a>
                        <div class="detail-info">
                            <div class="block">
								<i class="fa fa-map-marker fa-3x" aria-hidden="true" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" ></i>
                                <div class="info" itemprop="streetAddress"><?php echo get_option('addressto'); ?></div>
                            </div>
                        </div>
                        <div class="detail-info">
                            <div class="block">
								<i class="fa fa-mobile fa-3x" aria-hidden="true"></i>
                                <div class="info"><a href="tel:<?php echo get_option('telephone1'); ?>" itemprop="telephone"><?php echo get_option('telephone1'); ?></a><br/><a href="javascript:void(0);"><a href="tel:<?php echo get_option('telephone'); ?>" itemprop="telephone"><?php echo get_option('telephone'); ?></a></div>
                            </div>
                        </div>
                        <div class="detail-info">
                            <div class="block">
								<i class="fa fa-envelope-o fa-2x" aria-hidden="true"></i>
                                <div class="info"><?php echo __('Email address'); ?><br/><a href="mailto:<?php echo get_option('mailto'); ?>" itemprop="email" ><?php echo get_option('mailto'); ?></a></div>
                            </div>
                        </div>
                        <div class="detail-info">
                            <div class="block">
								<a target="_blank" href="https://vk.com/bazismed" class="soc-line"><i class="fa fa-vk fa-lg" aria-hidden="true"></i></a>
								<a target="_blank" href="https://www.facebook.com/bazismed/" class="soc-line"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
								<!--<a target="_blank" href="<?php /*echo get_option('twitter_url');*/ ?>" class="soc-line"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a>-->
								<a target="_blank" href="https://www.instagram.com/bazismed/" class="soc-line"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
								<a target="_blank" href="https://ok.ru/profile/574473292264" class="soc-line"><i class="fa fa-odnoklassniki fa-lg" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <!--
						<div class="detail-info subscribe_form">
						<?php echo do_shortcode('[wysija_form id="2"]');?>
						</div>-->
                        <div class="detail-info Privacy-Policy">
                            <div class="block">
                                <a href="/politika-konfidentsiynosti"><?php echo __('Privacy Policy'); ?></a>
                            </div>
                        </div>
                    </div>
				</div>
				<div class="col-lg-12 mob-background"  style="background: #737373;">

					<div class="cont-2">
						<div class="title"><?php echo __('have questions'); ?></div>
						<div class="descr"><?php echo __('Leave your request for a free consultation'); ?></div>
                        <?php
                        $currentLang = qtrans_getLanguage();
                        if ($currentLang == 'ru'){
                            echo do_shortcode('[contact-form-7 id="883" title="Контактная форма 1ru"]');
                        } elseif ($currentLang == 'ua'){
                            echo do_shortcode('[contact-form-7 id="12" title="Контактная форма 1uk"]');
                        }
                        ?>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<a target="_blank" href="<?php echo get_field('url', $post->ID); ?>" class="maps">
					<img itemprop = "hasMap" src="<?php echo get_option('mapto'); ?>">
				</a>
			</div>
		</div>
	</div>
</div>
<div class="footer-mob">
	<div class="container">
		<div    class="row" itemscope itemtype="http://schema.org/MedicalOrganization">
            <span  class="NameOrganization" itemprop="name">БазисМед</span>
			<div class="col-sm-10 col-xs-12 col-centered">
				<div class="subscribe_form">
					<?php echo do_shortcode('[wysija_form id="2"]');?>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="block"  itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" >
						<div class="image-box">
							<i itemprop = "hasMap" class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
						</div>
						<div class="info"  itemprop="streetAddress" ><?php echo get_option('addressto'); ?></div>
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="block mailto" style="margin-top: 42px;">
						<div class="image-box">
							<i class="fa fa-envelope-o fa-2x" aria-hidden="true"></i>
						</div>
						<div class="info"><?php echo __('Email address'); ?><br/><a  itemprop="email" href="javascript:void(0);"><?php echo get_option('mailto'); ?></a></div>
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="block">
						<div class="image-box">
							<i class="fa fa-mobile fa-3x" aria-hidden="true"></i>
						</div>
						<div class="info"><a  itemprop="telephone" href="tel:<?php echo get_option('telephone1'); ?>"><?php echo get_option('telephone1'); ?></a><br/><a itemprop="telephone" href="tel:<?php echo get_option('telephone'); ?>"><?php echo get_option('telephone'); ?></a></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 mob-fot">
				<a href="/" class="logo"></a>
			</div>
		</div>
	</div>
</div>
<div class="windowSizeCheck"></div>
<div class="windowSizeCheck1"></div>
</div>
<div class="back-top"> </div>
<script type="text/javascript">
    var HTTP = "<?php echo get_template_directory_uri(); ?>";
</script>
<?php wp_footer(); ?>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'KTuvLwzzIM';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->
<script>var telerTrackerWidgetId="38f20c39-8504-4e61-8e01-d5b0ecec8b75"; var telerTrackerDomain="bazismed.phonet.com.ua";</script>
<script src="//bazismed.phonet.com.ua/public/widget/call-tracker/lib.js"></script>

</body>
</html>