<!--?php  /* Template Name: News */ ?-->
<?php get_header(); ?>
<div class="category-page">
	<div class="news-mob">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 col-centered">
					<div class="news">
						<div class="title active js-btn" data-tab="unikal_one"><?php echo __('News'); ?></div>
						<div class="title_two js-btn" data-tab="unikal_two"><?php echo __('Actions'); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="news-mob-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-centered">
                    <div id="unikal_one" class="block active">
                        <?php
                        get_post();
                        if (query_posts('cat=2')):
                            while (have_posts()):
                                the_post();
                        ?>
                                <div class="fuc-block">
                                    <a href="<?php echo get_permalink() ?>" class="img" style="background:url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover; background-repeat:no-repeat; width:100%; height:164px; display:block;"></a>
                                    <div class="time">
                                        <div class="data"><?php echo the_date('Y.m.d'); ?></div>
                                        <a href="<?php echo get_permalink() ?>" class="next">
                                            читати далі
                                            <span class="det-arrow"></span>
                                        </a>
                                    </div>
                                    <a href="<?php echo get_permalink() ?>" class="descript"><?php the_content(); ?></a>
                                </div>
                        <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                        <?php
                        get_post();
                        if (query_posts('cat=2')):
                            while (have_posts()):
                                the_post(); ?>
                                <div class="new">
                                    <a href="<?php echo get_permalink() ?>" class="img" style="background: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover;"></a>
                                    <a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="title"><?php the_title(); ?></a>
                                    <a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="description"><?php the_content(); ?></a>
                                    <a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
                                    <span class="det-arrow"></span>
                                    </a>
                                </div>
                        <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                        <a href="<?php $category_id = 2; $category_link = get_category_link( $category_id ); echo $category_link; ?>" class="all_new">
                            <?php echo __('All news'); ?>
                            <div class="arrow"></div>
                        </a>
                    </div>
                    <div id="unikal_two" class="block">
                        <?php
                        get_post();
                        if (query_posts('cat=3')):
                            while (have_posts()):
                                the_post(); ?>
                                <div class="fuc-block">
                                    <a href="<?php echo get_permalink() ?>" class="img" style="background:url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover; background-repeat:no-repeat; width:100%; height:164px; display:block;"></a>
                                    <div class="time">
                                        <div class="data"><?php echo the_date('Y.m.d'); ?></div>
                                        <a href="<?php echo get_permalink() ?>" class="next">
                                            читати далі
                                            <span class="det-arrow"></span>
                                        </a>
                                    </div>
                                    <a href="<?php echo get_permalink() ?>" class="descript"><?php the_content(); ?></a>
                                </div>
                        <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                        <?php
                        get_post();
                        if (query_posts('cat=3')):
                            while (have_posts()):
                                the_post(); ?>
                                <div class="new">
                                    <a href="<?php echo get_permalink() ?>" class="img" style="background: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover;"></a>
                                    <a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="title"><?php the_title(); ?></a>
                                    <a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="description"><?php the_content(); ?></a>
                                    <a href="<?php echo get_permalink() ?>" class="next">
                                        читати далі
                                        <span class="det-arrow"></span>
                                    </a>
                                </div>
                        <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                        <a href="<?php $category_id = 3; $category_link = get_category_link( $category_id ); echo $category_link; ?>" class="all_new">
                            ><?php echo __('all shares'); ?>
                            <div class="arrow"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="category">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 category-title">
				<div class="title">
					<?php $queried_object = get_queried_object(); echo $queried_object->name ; ?>
				</div>
			</div>
			<div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <?php
                    $args = array( 'cat' => $queried_object->term_id);
                    $lastposts = new WP_Query($args);
                    while ($lastposts->have_posts()):
                        $lastposts->the_post();
                        $check = get_field('view_left');
                        if($check):
                    ?>
                            <div class="new left">
                                <a href="javascript:void(0);" class="img" style="background: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover;"></a>
                                <a href="javascript:void(0);" style="width:calc(100% - 180px);" class="title"><?php the_title(); ?></a>
                                <a href="javascript:void(0);" style="width:calc(100% - 180px);" class="description"><?php the_excerpt('читати далі',TRUE); ?></a>
                                <a href="<?php echo get_permalink() ?>" class="next">
                                    читати далі
                                    <span class="det-arrow"></span>
                                </a>
                            </div>
                    <?php
                        endif;
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <?php
                    $args = array( 'cat' => $queried_object->term_id );
                    $lastposts = new WP_Query($args);
                    while ($lastposts->have_posts()):
                        $lastposts->the_post();
                        $check = get_field('view_right');
                        if($check):
                    ?>
                            <div class="new right">
                                <a href="javascript:void(0);" class="img" style="background: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover;"></a>
                                <a href="javascript:void(0);" style="width:calc(100% - 180px);" class="title"><?php the_title(); ?></a>
                                <a href="javascript:void(0);" style="width:calc(100% - 180px);" class="description"><?php the_excerpt('читати далі',TRUE); ?></a>
                                <a href="<?php echo get_permalink() ?>" class="next">
                                    читати далі
                                    <span class="det-arrow"></span>
                                </a>
                            </div>
                    <?php
                        endif;
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
			    <?php if (function_exists('wp_corenavi')) wp_corenavi(); ?>
		    </div>
	    </div>
    </div>
</div>
<?php get_footer(); ?>	