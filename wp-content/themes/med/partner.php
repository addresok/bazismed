<!--?php  /* Template Name: Partner */ ?-->
<?php get_header(); 
the_post();
?>

<div class="container">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-md-12 col-sx-12">
			<div class="partner-title"><?php echo __('our partners'); ?></div>
		</div>
	</div>
</div>
<div class="patern">
	<div class="">
		<div class="wrap-s">
			<div class="pattrn-block">
				<?php $args = array( 'cat' => 8, 'nopaging' => true );
					$lastposts = new WP_Query($args);
					while ($lastposts->have_posts()): $lastposts->the_post(); 
					
					?>					
				
					<a href="<?php echo get_field('link_to_site', $post->ID); ?>" target="_blank" class="block">
						<img src="<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>" alt="">
					</a>
				<?php endwhile; 
					wp_reset_query();
				?>
			</div>
			<div class="hei-bottomer"></div>
		</div>
	</div>
</div>


<?php get_footer(); ?>	