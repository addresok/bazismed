<?php get_header(); ?>

<?php
global $wp_query;
?>

<div id="blogPage" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <h2>Найсвіжіші</h2>
        <div class="row news_blog">
            <div class="link_news_blog">
                <span class="arrow-ser-left" v-on:click="getPrevPage" v-bind:class="(page == 1) ? 'arrow-disab': ''"></span>
                <span class="arrow-ser-right" v-on:click="getNextPage" v-bind:class="(page == totalBlogItems) ? 'arrow-disab': ''"></span>
            </div>
            <div class=""
                 v-model="blogItems"
                 v-bind:class="!index ? 'col-md-8': 'col-md-4'"
                 v-for="(item, index) in blogItems">
                <div class="blog-item"
                     v-bind:style="{ backgroundImage: 'url(' + (!index ? item.images.large : item.images.medium) + ')' }">
                    <div class="block-overlay"></div>
                    <div class="block-data">
                        <div class="block-data">
                            <div class="body-link" style="cursor: pointer;">
                                <div class="body">
                                    <div class="table">
                                        <a v-bind:href="item.link"  class="name table-cell">
                                            <span class="block-name" v-html="item.title.rendered"></span>
                                        </a>
                                    </div>
                                    <div class="attributes">
                                        <div class="left">
                                            <span  v-if="item.blog_name" class="category">{{item.blog_name}}</span>
                                        </span>
                                        </div>
                                        <div class="right"><span class="time"><span class="icon"></span> {{item.date | formatDate}}</span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div class="left">
                                    <span class="author-name"> {{ item.autorb }} </span>
                                </div>
                                <div class="right">
                                    <span class="icon-views">{{ (item.post_views) ? item.post_views : 0 }}</span>
                                    <!--<span class="icon-like">13</span>-->
                                    <a v-bind:href="(item.link + '#comments')" class="icon-comment">{{ (item.count_comments) ? item.count_comments : 0 }}</a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="loader-tush" v-if="animate">
            <div class="loader">
                <div></div>
                <svg  xmlns="http://www.w3.org/2000/svg" version="1.1">
                    <defs>
                        <filter id="goo">
                            <fegaussianblur in="SourceGraphic" stddeviation="15" result="blur"></fegaussianblur>
                            <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 26 -7" result="goo"></fecolormatrix>
                            <feblend in="SourceGraphic" in2="goo"></feblend>
                        </filter>
                    </defs>
                </svg>
            </div>
            </div>
            
        </div>
    </div>
	<div v-model="blogItems[0]" v-if="blogItems[0]" class="container-fluid current-article current-article-favorite"
         v-bind:style="{ backgroundImage: 'url(' +  blogItems[0].images.large + ')' }">
        <div class="overlay"></div>
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <span class="pin"></span>
                        <div class="info table">
                            <div class="table-cell">
                                <span  v-if="blogItems[0].blog_name" class="category">{{blogItems[0].blog_name}}</span>
                                <a v-bind:href="blogItems[0].link" class="article-name" v-html="blogItems[0].title.rendered"></a>
                            </div>
                        </div>
                        <div class="footer">
                            <span class="author-name">{{ blogItems[0].autorb }}</span>
                            <div class="info">
                                <span class="icon-views">{{ (blogItems[0].post_views) ? blogItems[0].post_views : 0 }}</span>
                                <!--<span class="icon-like">23</span>-->
                                <a v-bind:href="(blogItems[0].link + '#comments')" class="icon-comment">{{ (blogItems[0].count_comments) ? blogItems[0].count_comments : 0 }}</a>
                            </div>
                            <span class="time"><span class="icon"></span> {{blogItems[0].date | formatDate}}</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <div class="subscribe favorite-post-subscribe">
                            <p class="title"><span class="bold">Подписаться</span></p>
	                        <?php echo do_shortcode('[wysija_form id="3"]');?>
                            <p class="stat">Уже підписалися 2 чоловік</p>
                        </div>
                        <div class="subscribe favorite-post-success" style="height:150px; display:none;">
                            <p class="title"><span class="bold">Спасибо за подписку</span></p>
                            <div class="envelope_img"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div id="nayItems" class="info-block most-blocks">
    <div class="container">
        <div class="block-title">
            <h2>Най</h2>
            <span class="sort" v-bind:class="(blogItemsNai.orderby == 'comment_count') ? 'active': ''" @click="sortPageNai('comment_count')">коментоване</span>
            <span class="sort" v-bind:class="(blogItemsNai.orderby == 'post_views') ? 'active': ''" @click="sortPageNai('post_views')">популярне</span>
            <span class="sort"  v-bind:class="(blogItemsNai.orderby == 'date') ? 'active': ''" @click="sortPageNai('date')">нове</span>
        </div>
        <div class="row news_blog">
            <div class="link_news_blog">
                <span class="arrow-ser-left" v-on:click="getPrevPageNai" v-bind:class="(blogItemsNai.page == 1) ? 'arrow-disab': ''"></span>
                <span class="arrow-ser-right" v-on:click="getNextPageNai" v-bind:class="(blogItemsNai.page == blogItemsNai.totalBlogItems) ? 'arrow-disab': ''"></span>
            </div>
            <div class="col-md-4 block" v-for="(item, index) in blogItemsNai.post">
                    <div class="block-image"
                         v-bind:style="{ backgroundImage: 'url(' + item.images.medium + ')' }">
                        <a class="overlay" v-bind:href="item.link"></a>
                        <div class="attributes">
                            <div class="cat left">
                                <span  v-if="item.blog_name" class="category">{{item.blog_name}}</span>
                            </div>
                            <div class="times right"><span class="time"><span class="icon"></span> {{item.date | formatDate}} </span></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="author-block">
                        <span class="author-name left"> {{ item.autorb }} </span>
                        <div class="info right">
                            <span class="icon-views">{{ (item.post_views) ? item.post_views : 0 }}</span>
                            <a v-bind:href="(item.link + '#comments')" class="icon-comment">{{ (item.count_comments) ? item.count_comments : 'Залишити відгук' }}</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <a v-bind:href="item.link" class="article-name" v-html="item.title.rendered"></a>
                    <div class="description" v-html="item.excerpt.rendered"></div>
            </div>
            <div class="loader-tush" v-if="blogItemsNai.animate">
                <div class="loader">
                    <div></div>
                    <svg  xmlns="http://www.w3.org/2000/svg" version="1.1">
                        <defs>
                            <filter id="goo">
                                <fegaussianblur in="SourceGraphic" stddeviation="15" result="blur"></fegaussianblur>
                                <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 26 -7" result="goo"></fecolormatrix>
                                <feblend in="SourceGraphic" in2="goo"></feblend>
                            </filter>
                        </defs>
                    </svg>
                </div>
            </div>

        </div>
    </div>
</div>
    
    
    
    
  

    <div class="container-fluid question-send">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                       <p class="title">Хочеш поставити запитання<br>
                           про ...?</p>
                    </div>
                    <div class="col-md-8 send-form">
	                    <?php echo do_shortcode('[contact-form-7 id="1005" title="Хочеш поставити запитання - блог"]');?>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div id="videoBlog" class="container-fluid videos"  v-if="blogVideos.video[0]">
        <div class="row">
            <div class="container">
                <div class="row" >
                    <div class="block-title">
                        <h2>Відео</h2>
                    </div>
                    <div class="link_news_blog">
                        <span class="arrow-ser-left" v-on:click="getPrevBogVideos()" v-bind:class="(blogVideos.page == 1) ? 'arrow-disab': ''"></span>
                        <span class="arrow-ser-right" v-on:click="getNextBlogVideos()" v-bind:class="(blogVideos.page == blogVideos.totalBlogItems) ? 'arrow-disab': ''"></span>
                    </div>
                        <div class="col-md-8 block big" onclick="ovetrHide(this)">
                            <div class="preview">
                                <div class="overlay"></div>
                                <div class="video-info">
                                    <div class="table">
                                        <div class="table-cell">
                                            <span class="icon play"></span>
                                        </div>
                                    </div>
                                    <div class="name" v-html="blogVideos.video[0].title.rendered"></div>
                                </div>
                            </div>
                            <p class="videoblock" v-html="blogVideos.video[0].content.rendered"></p>
                        </div>
                    
                        <div class="col-md-4 col-sm-12 blocks small">
                            <div class="block" v-if="blogVideos.video[1]" onclick="ovetrHide(this)">
                                <div class="preview">
                                    <div class="overlay"></div>
                                    <div class="video-info">
                                        <div class="table">
                                            <div class="table-cell">
                                                <span class="icon play"></span>
                                            </div>
                                        </div>
                                        <div class="name"  v-model="blogVideos.video[1]" v-html="blogVideos.video[1].title.rendered"></div>
                                    </div>
                                </div>
                                <p class="videoblock" v-html="blogVideos.video[1].content.rendered"></p>
                            </div>
                            <div class="block" v-if="blogVideos.video[2]" onclick="ovetrHide(this)">
                                <div class="preview">
                                    <div class="overlay"></div>
                                    <div class="video-info">
                                        <div class="table">
                                            <div class="table-cell">
                                                <span class="icon play"></span>
                                            </div>
                                        </div>
                                        <div class="name" v-html="blogVideos.video[2].title.rendered"></div>
                                    </div>
                                </div>
                                <p class="videoblock" v-html="blogVideos.video[2].content.rendered"></p>
                            </div>
                        </div>
             
                </div>
            </div>
        </div>

    </div>

    <div class="container"  v-model="blogAllCatItems" style="visibility: hidden">
        
        {{ blogAllCatItems | json }}
        
    </div>
    
    

    <div v-for="(cat, index) in blogAllCatItems.CatItems"
         v-bind:id="'categoty' + index" class="info-block most-blocks"
         v-bind:style="{ backgroundColor: ((index && (index % 2) ) ? '#f5f4ee' : '#fff')  }">
        <div v-if="cat['countAll'] > 0"   class="container">
            <div class="block-title">
                <h2>{{ cat['catname'] }}</h2>
                <span class="sort" v-bind:class="(cat.orderby == 'comment_count') ? 'active': ''" @click="sortItemsToCat(index, 'comment_count')">коментоване</span>
                <span class="sort" v-bind:class="(cat.orderby == 'post_views') ? 'active': ''" @click="sortItemsToCat(index, 'post_views')">популярне</span>
                <span class="sort"  v-bind:class="(cat.orderby == 'date') ? 'active': ''" @click="sortItemsToCat(index, 'date')">нове</span>
            </div>
            <div class="row news_blog">
                <div class="link_news_blog">
                    <span class="arrow-ser-left" v-on:click="getPrevBlogItemsToCat(index)" v-bind:class="(cat.page == 1) ? 'arrow-disab': ''"></span>
                    <span class="arrow-ser-right" v-on:click="getNextBlogItemsToCat(index)" v-bind:class="(cat.page == cat.countAll) ? 'arrow-disab': ''"></span>
                </div>
                <div class="col-md-4 block" v-for="(item, index) in cat['posts']">

                    <div class="block-image"
                         v-bind:style="{ backgroundImage: 'url(' + item.images.medium + ')' }">
                        <a class="overlay" v-bind:href="item.link"></a>
                        <div class="attributes">
                            <div class="cat left">
                                <span  v-if="item.blog_name" class="category">{{item.blog_name}}</span>
                            </div>
                            <div class="times right"><span class="time"><span class="icon"></span> {{item.date | formatDate}} </span></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="author-block">
                        <span class="author-name left"> {{ item.autorb }} </span>
                        <div class="info right">
                            <span class="icon-views">{{ (item.post_views) ? item.post_views : 0 }}</span>
                            <a v-bind:href="(item.link + '#comments')" class="icon-comment">{{ (item.count_comments) ? item.count_comments : 'Залишити відгук' }}</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <a v-bind:href="item.link" class="article-name"  v-html="item.title.rendered"></a>
                    <div class="description" v-html="item.excerpt.rendered"></div>
                </div>
              

            </div>
            
        </div>
        <div v-else   class="container">
            <div class="block-title">
                <h2>{{ cat['catname'] }} - <u> категорія порожня</u></h2>
            </div>
        </div>
        <div class="loader-tush" v-if="cat.animate">
            <div class="loader">
                <div></div>
                <svg  xmlns="http://www.w3.org/2000/svg" version="1.1">
                    <defs>
                        <filter id="goo">
                            <fegaussianblur in="SourceGraphic" stddeviation="15" result="blur"></fegaussianblur>
                            <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 26 -7" result="goo"></fecolormatrix>
                            <feblend in="SourceGraphic" in2="goo"></feblend>
                        </filter>
                    </defs>
                </svg>
            </div>
        </div>
        
    </div>



    <div class="more" v-if="blogAllCatItems.catCoun < blogAllCatItems.catCount" @click="addBlogItemsToCat()">Показати більше ({{blogAllCatItems.catCoun}} з {{blogAllCatItems.catCount}})</div>
    



    
    
    
    
</div>












<style>

    .breadcrumbs {
        display: none !important;
    }

</style>

<?php get_footer(); ?>
