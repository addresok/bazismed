<!--?php  /* Template Name: surgery */ ?-->

<?php get_header(); ?>
<script type="application/ld+json">
{
  "@context":"http://schema.org",
  "@id":"https://bazismed.com/hirurgiya/",
  "@type":"ItemPage",
  "copyrightHolder":{"@id":"#MedicalOrganization"},
  "publisher":{"@id":"#MedicalOrganization"},
  "mainEntity":{
    "@id":"#Product",
    "@type":"Product",
    "name":"<?php echo get_field('other-text-surgery3', $post->ID); ?>",
    "url":"https://bazismed.com/hirurgiya/",
    "offers":{
      "@id":"#Offer",
      "@type":"Offer",
      "priceCurrency":"UAH",
      "availability":"http://schema.org/InStock",
      "itemOffered":{"@id":"#Product"},
        "offeredBy":{
          "@id":"#Organization",
          "@type":"Organization",
          "name":"<?php echo get_field('other-text-surgery3', $post->ID); ?>",
          "url":"https://bazismed.com/"
        }
    }
  }
}
</script>
<div class="usd bk-right" style="background-image: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>');">
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">

			<div class="block-info">
						<h1 class="title"><?php echo get_field('other-text-surgery3', $post->ID); ?></h1>
		<a href="#surgery" class="butons"><?php echo __('Register for reception'); ?></a>
		<div class="clr"></div>
				<a href="/price?data=585" class="price f"><?php echo __('See prices'); ?></a>
	</div>
			</div>
		</div>
	</div>
	

</div>

<div class="bk-uzd">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
			</div>
			<div class="col-lg-6 col-xs-12 col-centerd">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="tit-surgery line"><?php echo get_field('tit-surgery1', $post->ID); ?>
							<div class="ars ars1 active"></div>
						</div>
						<div class="other-text-surgery one"><?php echo get_field('other-text-surgery1', $post->ID); ?></div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="tit-surgery line"><?php echo get_field('tit-surgery2', $post->ID); ?>
							<div class="ars ars2"></div>
						</div>
						<div class="other-text-surgery closed2 two"><?php echo get_field('other-text-surgery2', $post->ID); ?></div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>

<div class="hirurg">
	<div class="container">
		<div class="row">
			<div class="col-lg-9  col-md-9 col-sm-10 col-xs-12 col-centered">
				<div class="hir">	
					<?php $args = array( 'cat' => 6, 'nopaging' => true );
					$lastposts = new WP_Query($args);
					while ($lastposts->have_posts()): $lastposts->the_post(); 
						$checked = get_field('view_in_surgery');
						if($checked){
					?>				
						<div class="box">
							<div class="photo">
								<img src="<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>">
								<div class="name"><?php the_title(); ?></div>
								<div class="post"><?php echo get_field('specialization', $post->ID); ?></div>
							</div>
							<div class="text-ino-h"><?php the_content(); ?></div>
						</div>
					<?php } endwhile;
						wp_reset_query();
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="surgery" class="bk-form">
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="block pos-auto-page">
					<div class="title"><?php echo __('Leave request title text'); ?></div>
					<div class="descr"><?php echo __('Leave request desc text'); ?></div>
                    <?php
                    $currentLang = qtrans_getLanguage();
                    if ($currentLang == 'ru'){
                        echo do_shortcode('[contact-form-7 id="889" title="Хирургия"]');
                    } elseif ($currentLang == 'ua'){
                        echo do_shortcode('[contact-form-7 id="167" title="Хирургія"]');
                    }
                    ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="reviews" class="reviews" style="margin-top: 0;">
		<div class="slider">
			<div id="owl-demo">
			
				<?php $args = array( 'cat' => 4, 'nopaging' => true ); 
					$lastposts = new WP_Query($args);
					while ($lastposts->have_posts()): $lastposts->the_post();  ?>					
						<div>
							<div class="first">
								<div class="face">
									<img src="<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>">
								</div>
								<div class="name"><?php the_title(); ?></div>
								<div class="desc">(<?php echo get_field('position', $post->ID); ?>)</div>
							</div>
							<div class="second">
							<?php the_content(); ?>
							</div>
						</div>
				<?php endwhile;  
					wp_reset_query();
				?>
			</div>
		</div>
</div>
	<div class="rewies-mobile">
		<h6><?php echo __('Reviews'); ?></h6>
		<div class="rew-wrap">
			<a href="javascript:void(0);" class="contrl prev">
				<span></span>
			</a>
			<div class="rew-cont col-xs-12">
			<?php get_post();
					if (query_posts('cat=4')): while (have_posts()): the_post(); ?>	
				<div class="rew-box ">
					<h5>
						<?php the_title(); ?>
						<span>
							(<?php echo get_field('position', $post->ID); ?>)
						</span>
 					</h5>
 					<p>
 						<?php the_content(); ?>
 					</p>
				</div>
			<?php endwhile; endif; 
					wp_reset_query();
				?>
			</div>
			<a  href="javascript:void(0);" class="contrl nextss">
				<span></span>
			</a>
		</div>
	</div>
<div class="str-mob">
	<div class="news-mob">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 col-centered">
					<div class="news">
						<div class="title active js-btn" data-tab="unikal_one"><?php echo __('News'); ?></div>
						<div class="title_two js-btn" data-tab="unikal_two"><?php echo __('Actions'); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="news-mob-container" style="background:#fff;">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-centered">
				<div id="unikal_one" class="block active">
					<div class="owl-slider-mobile">
					<?php get_post();
					if (query_posts('cat=2')): while (have_posts()): the_post(); ?>
						<div class="fuc-block">
							<a href="<?php echo get_permalink() ?>" class="img" style="background:url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover; background-repeat:no-repeat; width:100%; height:164px; display:block;"></a>
							<div class="time">
								<div class="data"><?php echo the_date('Y.m.d'); ?></div>
								<a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
								<span class="det-arrow"></span>
								</a>
							</div>
							<a href="<?php echo get_permalink() ?>" class="descript"><?php the_excerpt('(читати далі)',TRUE); ?></a>
						</div>
					<?php endwhile; endif; 
						wp_reset_query();
					?>
					</div>
					<?php get_post();
					if (query_posts('cat=2')): while (have_posts()): the_post(); ?>
						<div class="new">
							<a href="<?php echo get_permalink() ?>" class="img" style="background: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover;"></a>
							<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="title"><?php the_title(); ?></a>
							<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="description"><?php the_excerpt('(читати далі)',TRUE); ?></a>
							<a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
							<span class="det-arrow"></span>
							</a>
						</div>
					<?php endwhile; endif; 
						wp_reset_query();
					?>
					<div class="clr"></div>
					<a href="<?php $category_id = 2; $category_link = get_category_link( $category_id );
						echo $category_link; ?>" class="all_new"><?php echo __('All news'); ?>
						<div class="arrow"></div>
					</a>
				</div>

				<div id="unikal_two" class="block">
					<div class="owl-slider-mobile">
					<?php get_post();
					if (query_posts('cat=3')): while (have_posts()): the_post(); ?>
						<div class="fuc-block">
							<a href="<?php echo get_permalink() ?>" class="img" style="background:url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover; background-repeat:no-repeat; width:100%; height:164px; display:block;"></a>
							<div class="time">
								<div class="data"><?php echo the_date('Y.m.d'); ?></div>
								<a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
								<span class="det-arrow"></span>
								</a>
							</div>
							<a href="<?php echo get_permalink() ?>" class="descript"><?php the_excerpt('(читати далі)',TRUE); ?></a>
						</div>
					<?php endwhile; endif; 
						wp_reset_query();
					?>
					</div>
					<?php get_post();
					if (query_posts('cat=3')): while (have_posts()): the_post(); ?>
						<div class="new">
							<a href="<?php echo get_permalink() ?>" class="img" style="background: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover;"></a>
							<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="title"><?php the_title(); ?></a>
							<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="description"><?php the_excerpt('(читати далі)',TRUE); ?></a>
							<a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
							<span class="det-arrow"></span>
							</a>
						</div>
					<?php endwhile; endif; 
						wp_reset_query();
					?>	
					<div class="clr"></div>
					<a href="<?php $category_id = 3; $category_link = get_category_link( $category_id );
						echo $category_link; ?>" class="all_new"><?php echo __('all shares'); ?>
						<div class="arrow"></div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>	