<?php
include("../../../../wp-config.php");
/**
 * Created by PhpStorm.
 * User: s.popadyuk
 * Date: 25.05.2017
 * Time: 12:04
 */
$page = $_POST['blog_page'];
if (isset($_POST['categories'])) {
    $categories = $_POST['categories'];
    $tax_query = array(
        array(
            'taxonomy' => 'blog',
            'field' => 'slug',
            'terms' => $categories
        )
    );
}
$args = array(
    'paged' => $page,
    'tax_query' => $tax_query,
    'post_type' => 'statti',
    'posts_per_page' => 3
);
$posts = array();
query_posts($args);
if (have_posts()) {
    while (have_posts()) {
        the_post();
        $post->image = wp_get_attachment_url(get_post_thumbnail_id());
        $post->url = get_permalink($post);
        $posts[] = $post;
    }
}
echo json_encode($posts, JSON_UNESCAPED_UNICODE);
wp_reset_query();