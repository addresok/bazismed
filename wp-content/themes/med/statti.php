<!--?php  /* Template Name: Blog */ ?-->
<?php get_header(); ?>



<section class="page-content">
    <div class="container">
        <div class="row">
            <h1 class="page-content__page-title">
                <?php echo the_title(); ?><span class="page-content__page-title__ribbon"></span>
            </h1>
            <div class="page-content__grid"><!-- article grid" -->
            <?php
            $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = [
                'paged' => $current_page,
                'post_type' => 'article',
                'taxonomy' => 'blog',
                'posts_per_page' => 6
            ];
            $articles = query_posts($args);
            if (count($articles) > 0) : foreach ($articles as $article) :?>
                <div class="wow fadeIn animated page-content__article-wrap <?php echo setColForArticles(get_field('show_big', $article->ID));?>">
                    <a href="<?= get_permalink($article) ?>">
                        <div class="page-content__article-background" style="background: url(<?php echo get_the_post_thumbnail_url( $article->ID); ?>) no-repeat;background-size: cover">
                            <div class="page-content__article">
                            <?php if (get_field('article_category', $article->ID) != ""){ ?>
                                <span class="page-content__article__category"><i class="fa fa-tag" aria-hidden="true"></i><?php echo get_field('article_category', $article->ID); ?></span>
                            <?php } ?>
                                <div class="page-content__article__footer_white">
                                    <h3 class="page-content__article__title"><?= $article->post_title ?></h3>
                                    <div class="page-content__article__author">
                                        <span class="page-content__article__author__name">
                                            <?php echo get_field('article_author', $article->ID) ?>
                                        </span>
                                        <?php if(get_field('article_show_count', $article->ID) !=""){ ?>
                                        <span class="page-content__article_show-count">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                            <?php echo get_field('article_show_count', $article->ID);} ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

             <?php endforeach; endif; ?>
        </div>
        </div><!-- END article grid" -->
        <div class="cos-xs-12"> 
            <div class="page-content__pagination">
                <?php echo paginate_links(array(
                    'prev_text' => ('<i class="fa fa-chevron-left" aria-hidden="true"></i>'),
                    'next_text' => ('<i class="fa fa-chevron-right" aria-hidden="true"></i>')
                ));
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
