<?php get_header(); ?>

<?php
global $wp_query;
?>
<div id="vacanciesPage" class="container">
    <div class="row">

        <div class="col-sm-3 menuvak">
            <h1>Актуальні вакансії</h1>
           <ul>
               <li v-bind:class="(index == activ) ? 'activ': ''"
                   v-for="(item, index) in vacanceList"
                   @click="(index != activ) ? setActiv(index) : ''">
                   {{ item.title.rendered }}
               </li>
           </ul>
        </div>
        <div class="col-sm-9 body">

            <div class="row">
                <div class="col-sm-12"><h2>{{ body.title }}</h2></div>
            </div>
            <div class="row">
                <div class="col-sm-10" v-html="body.text"></div>
                <img class="body-img" v-bind:src="body.img"  alt="">
            </div>
            <div class="loader-tush" v-if="animate">
                <div class="loader">
                    <div></div>
                    <svg  xmlns="http://www.w3.org/2000/svg" version="1.1">
                        <defs>
                            <filter id="goo">
                                <fegaussianblur in="SourceGraphic" stddeviation="15" result="blur"></fegaussianblur>
                                <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 26 -7" result="goo"></fecolormatrix>
                                <feblend in="SourceGraphic" in2="goo"></feblend>
                            </filter>
                        </defs>
                    </svg>
                </div>
            </div>

            <div class="row send-resum">
                <h3>Відправити резюме</h3>
                <div class="col-sm-12" >
	                <?php echo do_shortcode('[contact-form-7 id="1023" title="Відправити резюме"]');?>
                </div>
            </div>
	        
            
        </div>
    </div>
</div>



<style>

    .breadcrumbs {
        display: none !important;
    }

</style>

<?php get_footer(); ?>
