<!--?php  /* Template Name: about */ ?-->
<?php get_header();
the_post();
?>
<div id="homePage">


<div id="about-mob" class="about-video-mob">
    <div class="video-mob-cont">
        <div class="btns">
            <a class="about-video-link">
                <div class="ico"></div>
                <span><?php echo __('about us'); ?></span>
            </a>
        </div>
        <iframe src="<?php echo get_field('link_video', $post->ID); ?>" frameborder="0" allowfullscreen=""></iframe>


    </div>
    <div class="video-mob-wrap">
        <h5><?php echo __('about us'); ?></h5>
        <a class="video-link">
            <span><?php echo __('Video'); ?></span>
            <img src="/wp-content/themes/med/img/youtube-ico.png" alt="">
        </a>
        <div class="about-text">
            <p>
                <?php the_content(); ?>
            </p>
        </div>
        <!-- <div class="controls">
            <a href="javascript:void(0);" class="up">
            </a>
            <a href="javascript:void(0);" class="down">
            </a>
        </div> -->
    </div>
</div>

<div id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="conta1">
                    <div class="title"><?php echo __('about us'); ?></div>
                    <div class="col-md-8 text"><?php the_content(); ?></div>
                    <div class="btns">
                        <a class="link">
                            <div class="hov-efect"><?php echo __('Video'); ?></div>
                            <div class="youtube">
                                <div class="ico"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="conta2">
                    <iframe width="560" height="315" src="<?php echo get_field('link_video', $post->ID); ?>" frameborder="0"
                            allowfullscreen></iframe>
                    <div class="btns">
                        <a class="link">
                            <div class="hov-efect"><?php echo __('about us'); ?></div>
                            <div class="ab">
                                <div class="ico"></div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
















<div id="blogHome" class="info-block most-blocks">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="title">Наш блог</div></div>
        <div class="block-title">
            <h2>Най</h2>
            <span class="sort" v-bind:class="(blogItemsNai.orderby == 'comment_count') ? 'active': ''" @click="sortPageNai('comment_count')">коментоване</span>
            <span class="sort" v-bind:class="(blogItemsNai.orderby == 'post_views') ? 'active': ''" @click="sortPageNai('post_views')">популярне</span>
            <span class="sort"  v-bind:class="(blogItemsNai.orderby == 'date') ? 'active': ''" @click="sortPageNai('date')">нове</span>
        </div>
        <div class="row news_blog">
            <div class="link_news_blog">
                <span class="arrow-ser-left" v-on:click="getPrevPageNai" v-bind:class="(blogItemsNai.page == 1) ? 'arrow-disab': ''"></span>
                <span class="arrow-ser-right" v-on:click="getNextPageNai" v-bind:class="(blogItemsNai.page == blogItemsNai.totalBlogItems) ? 'arrow-disab': ''"></span>
            </div>
            <div class="col-md-4 block" v-for="(item, index) in blogItemsNai.post">
                <div class="block-image"
                     v-bind:style="{ backgroundImage: 'url(' + item.images.medium + ')' }">
                    <a class="overlay" v-bind:href="item.link"></a>
                    <div class="attributes">
                        <div class="cat left">
                            <span  v-if="item.blog_name" class="category">{{item.blog_name}}</span>
                        </div>
                        <div class="times right"><span class="time"><span class="icon"></span> {{item.date | formatDate}} </span></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="author-block">
                    <span class="author-name left"> {{ item.autorb }} </span>
                    <div class="info right">
                        <span class="icon-views">{{ (item.post_views) ? item.post_views : 0 }}</span>
                        <a v-bind:href="(item.link + '#comments')" class="icon-comment">{{ (item.count_comments) ? item.count_comments : 'Залишити відгук' }}</a>
                    </div>
                    <div class="clear"></div>
                </div>
                <a v-bind:href="item.link" class="article-name" v-html="item.title.rendered"></a>
                <div class="description" v-html="item.excerpt.rendered"></div>
            </div>
            <div class="loader-tush" v-if="blogItemsNai.animate">
                <div class="loader">
                    <div></div>
                    <svg  xmlns="http://www.w3.org/2000/svg" version="1.1">
                        <defs>
                            <filter id="goo">
                                <fegaussianblur in="SourceGraphic" stddeviation="15" result="blur"></fegaussianblur>
                                <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 26 -7" result="goo"></fecolormatrix>
                                <feblend in="SourceGraphic" in2="goo"></feblend>
                            </filter>
                        </defs>
                    </svg>
                </div>
            </div>

        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="btn-detal">
                <a href="/blog">Перейти на сторінку блогу ><span class="slide-arr-more "></span></a>
            </div>
        </div>
    </div>

</div>


    <div  id="videoBlog"  class="container-fluid videos"  v-if="blogVideos.video[0]">
        <div class="row">
            <div class="container">
                <div class="row" >
                    <div class="block-title">
                        <h2>Відео</h2>
                    </div>
                    <div class="link_news_blog">
                        <span class="arrow-ser-left" v-on:click="getPrevBogVideos()" v-bind:class="(blogVideos.page == 1) ? 'arrow-disab': ''"></span>
                        <span class="arrow-ser-right" v-on:click="getNextBlogVideos()" v-bind:class="(blogVideos.page == blogVideos.totalBlogItems) ? 'arrow-disab': ''"></span>
                    </div>
                    <div class="col-md-8 block big" onclick="ovetrHide(this)">
                        <div class="preview">
                            <div class="overlay"></div>
                            <div class="video-info">
                                <div class="table">
                                    <div class="table-cell">
                                        <span class="icon play"></span>
                                    </div>
                                </div>
                                <div class="name" v-html="blogVideos.video[0].title.rendered"></div>
                            </div>
                        </div>
                        <p class="videoblock" v-html="blogVideos.video[0].content.rendered"></p>
                    </div>

                    <div class="col-md-4 col-sm-12 blocks small" >
                        <div class="block" v-if="blogVideos.video[1]" onclick="ovetrHide(this)">
                            <div class="preview">
                                <div class="overlay"></div>
                                <div class="video-info">
                                    <div class="table">
                                        <div class="table-cell">
                                            <span class="icon play"></span>
                                        </div>
                                    </div>
                                    <div class="name"  v-model="blogVideos.video[1]" v-html="blogVideos.video[1].title.rendered"></div>
                                </div>
                            </div>
                            <p class="videoblock" v-html="blogVideos.video[1].content.rendered"></p>
                        </div>
                        <div class="block" v-if="blogVideos.video[2]" onclick="ovetrHide(this)">
                            <div class="preview">
                                <div class="overlay"></div>
                                <div class="video-info">
                                    <div class="table">
                                        <div class="table-cell">
                                            <span class="icon play"></span>
                                        </div>
                                    </div>
                                    <div class="name" v-html="blogVideos.video[2].title.rendered"></div>
                                </div>
                            </div>
                            <p class="videoblock" v-html="blogVideos.video[2].content.rendered"></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>


    




</div>
<?php




//$date = date("Y, m, d, H, i, s", strtotime(get_field('clock', $post->ID).' ' .get_field('hours', $post->ID) .'0000'));

//echo $date;



?>
<script>
    var dateCreatedOrg = new Date("<?php echo date("F d, Y G:i:s",strtotime(get_field('clock', $post->ID).' ' .get_field('hours', $post->ID) .'0000')); ?>");;
//    var dateCreatedOrg2 = Date.parse('<?//=$date?>//');
//    console.log(dateCreatedOrg)
//    console.log(dateCreatedOrg2)
</script>

<?php get_footer(); ?>