<?php get_header(); ?>
    <div id="progress-bar-wrapper">
        <div id="my-awesomepage-progress-bar-element"></div>
    </div>
    <section class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="article_wrap">
                        <div class="article_head">
                            <div class="article_head__background white" style="background: url('<?php echo get_the_post_thumbnail_url($post->ID,'full'); ?>') no-repeat;background-size: cover">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <?php if (get_field('article_category') != ""){ ?>
                                        <span class="page-content__article__category black"><i class="fa fa-tag" aria-hidden="true"></i>
                                            <?php echo get_field('article_category'); ?></span>
                                        <?php } ?>
                                        <div class="article__head__wrap">
                                        <h3 class="article__head__title"><?php echo the_title(); ?></h3>
                                            <div class="page-content__article__author">
                                             <span class="page-content__article__author__name">
                                                <?php echo get_field('article_author') ?>
                                             </span>
                                                <?php if (get_field('article_show_count') != ""){ ?>
                                                <span class="page-content__article_show-count">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                    <?php echo get_field('article_show_count');
                                                    } ?>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-7 col-xl-offset-1 col-lg-7 col-lg-offset-1 col-md-8">
                                    <div class="article__body">
                                    <span class="article_date"><?php the_date('m.d.Y');?></span>
                                    <div class="article_text">
                                        <?php the_content(); ?>
                                    </div>
                                        
                                        
                                        
                                       <?php

                                       if ( comments_open() || get_comments_number() ) {
	                                       comments_template();
                                       }
                                       
                                       ?>
                                        
                                        
                                        
                                        
                                    </div>
                                    <?php
                                    $args = [
                                        'orderby' => 'date',
                                        'post_type' => 'article',
                                        'taxonomy' => 'blog',
                                        'post__not_in' => array(get_the_ID()),
                                        'posts_per_page' => 4
                                    ];
                                    $alsoArticles = query_posts($args);
                                    if (count($alsoArticles) > 0) :
                                    ?>
                                        <div class="read_also">
                                            <span class="read_also_title">
                                                <?php echo __('Read also');?>
                                            </span>

                                            <?php foreach ($alsoArticles as $alsoArticle) : ?>
                                                <a class="read_also__item" href="<?= get_permalink($alsoArticle) ?>"><?php echo $alsoArticle->post_title?></a>
                                            <?php endforeach;?>
                                        </div>
                                    <?php endif; wp_reset_query(); ?>
                                </div>
                                <?php
                                $args = [
                                    'orderby' => 'date',
                                    'post_type' => 'article',
                                    'post__not_in' => array(get_the_ID()),
                                    'taxonomy' => 'blog',
                                    'posts_per_page' => 3
                                ];
                                $lastArticles = query_posts($args);
                                if (count($lastArticles) > 0) : ?>
                                <div class="col-xl-3 col-xl-offset-1 col-lg-3 col-lg-offset-1 col-md-4  col-sm-12">
                                    <div class="last_articles">
                                        <span class="last_articles_title"><?php echo __('Last posts');?></span>
                                        <div class="row">
                                            <div class="last_articles_slider">
                                        <?php foreach ($lastArticles as $lastArticle) : ?>
                                            <div class="page-content__article-wrap smaller col-xl-4 col-lg-12 col-md-12 col-sm-4">
                                                <a href="<?= get_permalink($lastArticle) ?>">
                                                        <div class="page-content__article">
                                                            <img class="img-responsive last_article__img" src="<?php echo get_the_post_thumbnail_url( $lastArticle->ID); ?>">
                                                            <?php if (get_field('article_category', $lastArticle->ID) != ""){ ?>
                                                            <span class="page-content__article__category"><i class="fa fa-tag" aria-hidden="true"></i><?php echo get_field('article_category', $lastArticle--) ?></span>
                                                            <?php } ?>
                                                            <div class="page-content__article__footer_white">
                                                                <h3 class="page-content__article__title"><?= $lastArticle->post_title ?></h3>
                                                                <div class="page-content__article__author">
                                        <span class="page-content__article__author__name">
                                            <?php echo get_field('article_author', $lastArticle->ID) ?>
                                        </span>
                                                                    <?php if(get_field('article_show_count', $lastArticle->ID) !=""){ ?>
                                                                    <span class="page-content__article_show-count">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                                        <?php echo get_field('article_show_count', $lastArticle->ID);} ?>
                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </a>
                                            </div>
                                        <?php endforeach; ?>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; wp_reset_query(); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        jQuery(document).ready(function () {
    if(jQuery(document).width() >= 768) {
    scrollProgress.set({
    prefix: 'my-awesome-page-progress-bar',
    color: '#db0302',
    height: '6px',
    bottom: true
    });
    }
        });
    </script>
<?php get_footer(); ?>



