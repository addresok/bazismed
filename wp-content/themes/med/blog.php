<!--?php  /* Template Name: Blog */ ?-->
<?php get_header(); ?>

<?php
global $wp_query;
/** Get categories list */

if (isset($_GET)) {
    $filters = explode('_', key($_GET));
    if (count($filters) > 0 && $filters[0] != "") {
        $tax_query = array(
            array(
                'taxonomy' => 'blog',
                'field' => 'slug',
                'terms' => $filters
            )
        );
    }
}
?>
<?php
$terms = get_terms(array(
    'taxonomy' => 'blog',
    'hide_empty' => false,
    'orderby' => 'name'
));
$categories = array();
foreach ($terms as $term) {
    $categories[] = array('name' => $term->name, 'slug' => $term->slug);
}
?>
<?php if (count($categories) > 0) { ?>
<div class="container">
    <div class="row">
        <div class="filter col-xs-4">
            <h4 class="filter_title">Фільтр по категоріям:</h4>
            <form>
                <ul>
                    <?php foreach ($categories as $category): ?>
                        <li>
                            <input id="<?php echo $category['slug']; ?>" type="checkbox"
                                   value="<?= $category['slug'] ?>" <?php foreach ($filters as $filter) {
                                if ($filter == $category['slug']) {
                                    echo 'checked="checked"';
                                }
                            } ?>><label for="<?php echo $category['slug']; ?>"><span></span><?= $category['name'] ?></label>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <span class="filter-button">Фільтрувати</span>
            </form>
        </div>
        <?php } ?>
        <?php
        $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'paged' => $current_page,
            'tax_query' => $tax_query,
            'post_type' => 'statti',
            'posts_per_page' => 3
        );
        $posts = query_posts($args);
        ?>
            <div class="blog col-xs-8">
                <div class="row">
                    <?php if (count($posts) > 0) { ?>
                    <?php foreach ($posts as $post) {/* setup_postdata($post);*/ ?>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 post_block">
                            <div class="blog-tile">
                                <h4><a href="<?= get_permalink($post) ?>"><?= $post->post_title ?></a></h4>
                                <a href="<?= get_permalink($post) ?>">
                                    <?php if (has_post_thumbnail()) { ?>
                                        <img src="<?= wp_get_attachment_url(get_post_thumbnail_id()) ?>"
                                             alt="<?php the_title(); ?>">
                                    <?php } else { ?>
                                        <img src="<?php bloginfo('template_directory'); ?>/img/default.jpg"
                                             alt="<?php the_title(); ?>"/>
                                    <?php } ?>
                                </a>
                                <span><?= $post->post_date ?></span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div id="postLoad"></div>
            </div>
            <div class="col-xs-12 blog-more">
                <?php echo paginate_links();
                wp_reset_query(); ?>
            </div>
            <?php } else { ?>
                <div class="container">
                    <div class="col-xs-12">
                        <h3 style="text-align: center">В даній категорії немає записів</h3>
                    </div>
                </div>
            <?php } ?>
    </div>
</div>

<?php get_footer(); ?>
