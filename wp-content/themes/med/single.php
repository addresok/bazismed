<!--?php  /* Template Name: New */ ?-->

<?php get_header(); ?>
<div class="not-foot">
<div class="new-one">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<?php if (have_posts()): while (have_posts()): the_post();?>
					<div class="title"><?php the_title(); ?></div>
					<div class="row">
						<div class="col-lg-12">
							<img src="<?php $img = get_field('photo_1', $post->ID);
								echo $img['sizes']['large'];?>" alt="">
							<img src="<?php $img = get_field('photo_2', $post->ID);  echo $img['sizes']['large'];?>" alt="">
							<div class="text"><?php the_content(); ?></div>
						</div>
					</div>
			
				<?php endwhile; endif; wp_reset_query(); ?>
				<a href="<?php $category_id = 2; $category_link = get_category_link( $category_id );
						echo $category_link; ?>" class="over-nexr"><?php echo __('all'); ?> <?php $queried_object = get_the_category();
				 echo $queried_object[0]->name ; ?>
				<div class="arr"></div>
				</a>
			</div>

			<div class="col-lg-4 col-md-4 other-new">
				<div class="title-other-new before-border"><?php echo __('other'); ?> <?php $queried_object = get_the_category();
				 echo $queried_object[0]->name ; ?></div>
				<?php	
				 $args = array( 'cat' =>  $queried_object[0]->cat_ID , 'nopaging' => true );
					$lastposts = new WP_Query($args);
					while ($lastposts->have_posts()): $lastposts->the_post();  ?>
				<div class="new" style="margin-right: -15px;">
					<a href="<?php echo get_permalink() ?>" class="img" style="background: url('<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>'); background-size: cover;"></a>
					<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px);" class="title"><?php the_title(); ?></a>
					<a href="<?php echo get_permalink() ?>" style="width:calc(100% - 180px); margin-bottom:15px;" class="description"><?php the_excerpt('читати далі',TRUE); ?></a>
					<a href="<?php echo get_permalink() ?>" class="next"><?php echo __('Read more'); ?>
					<span class="det-arrow"></span>
					</a>
				</div>

				<?php endwhile; 
					wp_reset_query();
				?>
					<a href="<?php $category_id = $queried_object[0]->cat_ID; $category_link = get_category_link( $category_id );
						echo $category_link; ?>" class="all_new"><?php echo __('all'); ?> <?php $queried_object = get_the_category();
				 echo $queried_object[0]->name ; ?>
						<div class="det-arrow"></div>
					</a>
				
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>	
</div>