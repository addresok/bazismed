<!DOCTYPE html>
<html>
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title(); ?></title>
    <?php /*$directory_uri = get_template_directory_uri();*/?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/med/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/med/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/med/css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/med/css/owl.transitions.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/med/css/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/med/css/animate.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/med/style.css">

	<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/med/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/med/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/med/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/med/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/med/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/med/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/med/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/med/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/med/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/wp-content/themes/med/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/med/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/wp-content/themes/med/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/med/favicon/favicon-16x16.png">
    <link rel="manifest" href="/wp-content/themes/med/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="google-site-verification" content="3DFmr7pbFyVHVuUISY-4f9uYRcUhROKom3MhwBqZBwI" />
    <style>
        @import 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,500,700&subset=cyrillic';
        @import 'https://fonts.googleapis.com/css?family=Arimo:400,700&subset=cyrillic';
    </style>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-84891893-1', 'auto');
        ga('send', 'pageview');
    </script>
	<!--<script src='https://www.google.com/recaptcha/api.js'></script>-->


    <?php wp_head(); ?>
</head>
<body>
<script type="application/ld+json">
{
 "@context" : "http://schema.org",
 "@type" : "MedicalOrganization",
 "name" : "BazisMed",
 "url" : "https://bazismed.com/",
 "logo": "https://bazismed.com/wp-content/themes/med/img/logoBZM.png",
 "sameAs" : [
 "https://vk.com/bazismed",
 "https://www.facebook.com/bazismed/",
 "https://www.instagram.com/bazismed/",
 "https://ok.ru/profile/574473292264"
 ]
}
</script>

<?php $currentLang = qtrans_getLanguage(); ?>

   <div id="system-load" align="center">
        <div class="loader-tush">
           <img src="/img/logoBZM.png" alt="loader" />
           <div class="loader">
               <div></div>
               <svg  xmlns="http://www.w3.org/2000/svg" version="1.1">
                    <defs>
                        <filter id="goo">
                           <fegaussianblur in="SourceGraphic" stddeviation="15" result="blur"></fegaussianblur>
                            <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 26 -7" result="goo"></fecolormatrix>
                           <feblend in="SourceGraphic" in2="goo"></feblend>
                       </filter>
                    </defs>
                </svg>
            </div>
        </div>

   </div>
    <div id="main-container">
        <!--consult-popup-->
        <a id="consult-call" href="#consult"></a>
        <?php $img_background = get_field('Background_image_block', 973) ?>
        <div id="consult" class ="consult-popup" style="background: url('<?php echo $img_background['url']; ?>') no-repeat;
        background-size: cover;
        background-position: center center;">
            <div class="consult-text col-xs-6 pull-right">
            <h3><?php echo get_field('Titel_block', 973); ?></h3>
                <p><?php echo get_field('description_block', 973); ?></p>
            <a id="consult-cf-call" href="#consult-cf"><?=__('Just get')?></a>
                </div>
            <img class="popup-close" src="/wp-content/themes/med/img/btn-esc.png" alt="">
        </div>
    <!--end consult-popup-->
        <div id="consult-cf">
            <?php if($currentLang == 'ru') {
                echo do_shortcode('[contact-form-7 id="940" title="Consult ru"]');
            } else {
                echo do_shortcode('[contact-form-7 id="939" title="Consult ua"]');
            }
            ?>
        </div>
        <!--Consult popup end-->
        <div class="popUp">
            <div class="container">
                <div class="row">
                    <div class="">
                        <div class="block">
                            <div class="esc"></div>
                            <div class="title"><?php echo __('Leave request title text'); ?></div>
                            <div class="descr"><?php echo __('Leave request desc text'); ?></div>
                            <?php
                            if ($currentLang == 'ru'){
                                echo do_shortcode('[contact-form-7 id="895" title="Pop-upru"]');
                            } elseif ($currentLang == 'ua'){
                                echo do_shortcode('[contact-form-7 id="136" title="Pop-up"]');
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bk-pop"></div>
        <div class="head">
            <div class="container-fluid">
                <div class="row">
                <div class="col-lg-12 header">
                    <a href="/" class="logo"></a>
                    <div class="menu">
                    <?php wp_nav_menu( array('menu' => 'Головне меню') ); ?>
                    </div>
                    <a href="javascript:void(0);" class="review">
                        <div class="ico"></div>
                        <div class="text"><?php echo __('register for inspection'); ?></div>
                        <div class="arrow"></div>
                    </a>
        			<div class="lang">
                    <?php
            if(qtranxf_getLanguage() == "ua") {?>
                <a href="/ua"  class="active">UA</a>
                <a href="/ru">RU</a>
        <?php	}
            else { ?>
                <a href="/ru" class="active">RU</a>
                <a href="/ua"  >UA</a>
        <?php } ?>


                        <div class="arrow-lang"></div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="head-mob">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="/" class="logo"></a>
                        <a href="javascript:void(0);" class="ico-menu"></a>
                        <div class="lang">
                            <?php if(qtranxf_getLanguage() == "ua") { ?>
                                    <a href="/ua" class="active">UA</a>
                                    <a href="/ru">RU</a>
                            <?php } else {?>
                                    <a href="/ru" class="active">RU</a>
                                    <a href="/ua">UA</a>
                            <?php } ?>
                            <div class="arrow-lang"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdown-menus">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <?php wp_nav_menu( array('menu' => 'Мобильные меню') ); ?>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="detail-info">
                            <div class="block">
                                <a target="_blank" href="https://vk.com/bazismed" class="soc-line"><i class="fa fa-vk fa-lg" aria-hidden="true"></i></a>
                                <a target="_blank" href="https://www.facebook.com/bazismed/" class="soc-line"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
                                <a target="_blank" href="https://www.instagram.com/bazismed/" class="soc-line"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
                                <a target="_blank" href="https://ok.ru/profile/574473292264" class="soc-line"><i class="fa fa-odnoklassniki fa-lg" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <a href="javascript:void(0);" class="review">
                            <div class="ico"></div>
                            <div class="text"><?php echo __('register for inspection'); ?></div>
                            <div class="arrow"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php
      //  dimox_breadcrumbs();
        ?>
