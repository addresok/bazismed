<!--?php  /* Template Name: advice */ ?-->
<?php get_header(); ?>

<br>
<section class="advice">
    <h2 class="title"><?php echo __('Registration for online consultation'); ?></h2>
    <div class="container">
        <div class="row header_block">
            <div class="col-xs-12">
                <?php

                $currentLang = qtrans_getLanguage();
                if ($currentLang == 'ru'){
                    echo do_shortcode('[contact-form-7 id="885" title="Запись на консультацию"]');
                } elseif ($currentLang == 'ua'){
                    echo do_shortcode('[contact-form-7 id="734" title="Запис на консультацію"]');
                }
                ?>
            </div>
        </div>
    </div>
    <div class="hirurg">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 col-centered">
                    <div class="advice_doctor_slider">
                        <?php $args = array( 'cat' => 6, 'nopaging' => true );
                        $lastposts = new WP_Query($args);
                        while ($lastposts->have_posts()): $lastposts->the_post();
                            $checked = get_field('view_on_advice');
                            if($checked){
                                ?>
                                <div class="box">
                                    <div class="photo">
                                        <img src="<?php echo  wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>">
                                        <div class="name"><?php the_title(); ?></div>
                                        <div class="post"><?php echo get_field('specialization', $post->ID); ?></div>
                                    </div>
                                    <div class="text-ino-h"><?php the_content(); ?></div>
                                </div>
                            <?php } endwhile;
                        wp_reset_query();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>