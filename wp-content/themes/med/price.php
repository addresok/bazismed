<?php  /* Template Name: Price */ ?>
<?php get_header(); the_post() ?>
<div id="pricePage" xmlns="http://www.w3.org/1999/html" xmlns:v-on="http://www.w3.org/1999/xhtml">
    <div class="container">
        <div class="row price_table">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h1><?php echo the_title(); ?></h1>
                    </div>
                    <div class="col-md-4 col-md-offset-2">
                        <div class="search-form"><input v-model="search" placeholder='Пошук послуги - наприклад "УЗД"' v-bind:class="[(searchText !='') ? 'searchTextYes' : '']">
                            <transition name="slide-fade">
                                <span v-model="searchText" class="searchText" v-if="searchText != ''">{{ searchText }}</span>
                            </transition>
                            <transition name="slide-fade">
                                <span class="fa fa-times searchTextClear"  v-if="search != ''" @click="search=''; searchText=''"></span>
                            </transition>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="price-cat">
                    <li @click="getListToCat(0, 0)"
                        v-bind:class="(selectCat == 0) ? 'activ': ''">Всі категорії</li>
                    <li v-for="(cat, idC) in CatPrices"
                        v-model="CatPrices"
                        @click="getListToCat(cat.id, 0)"
                        v-bind:class="(cat.id == selectCat) ? 'activ': ''"><span>{{ cat.name }}</span> <i>{{ cat.count_all }}</i></li>
                </ul>
            </div>
            <div class="col-md-8">
                <transition name="slide-fade">
                    <div class="row" v-if="subCat.length > 0">
                        <div class="col-md-12">
                            <div class="top-panel-price">
                                <ul class="subCat">
                                    <li v-model="subCat"
                                        v-for="(subC, index) in subCat"
                                        @click="(subC.id != selectSubCat) ? getListToCat(selectCat, subC.id) : ''"
                                        v-bind:class="(subC.id == selectSubCat) ? 'activ': ''"><span>{{ subC.name }} <i>{{ subC.count }}</i></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </transition>
                <transition name="slide-fade">
                    <div class="row" v-if="list.length > 0">
                        <div class="col-md-12">
                            <ul class="price-list">
                                <li v-for="(price, index) in list" v-model="list">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <p class="title">{{ price.title.rendered }}</p>
                                            <p class="subtitle" style="font-size: 9px"> {{ price.cat }}</p>
                                        </div>
                                        <div class="col-md-2 priceANDtermin">
                                            <p v-if="price.termin > 0">{{ price.termin }} {{ (price.termin == 1) ? 'день' : 'днів' }}</p>
                                            <p class="price"> {{ price.price }} грн.</p>
                                        </div>
                                        <div class="col-md-1 info-content">
                                            <i class="fa fa-info-circle" aria-hidden="true" @click="selectInfoF(index)"></i>

                                        </div>
                                        <div class="col-md-2 cart">
                                            <span class="switch">
                                              <input type="checkbox" v-bind:id="price.id" v-model="cart" v-bind:value="price"  @change="updateCart">
                                              <label v-bind:for="price.id" data-on="В кошику" data-off="Додати"></label>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row" v-else>
                        <div class="price-list">
                            <p class="title">Порожньо</p>
                        </div>
                    </div>

                </transition>
                <div class="paginPrice" v-if="total > 1">
                    <span class="arrow arrow-ser-left" v-on:click="getPrevPage" v-bind:class="(page == 1) ? 'arrow-disab': ''"></span>
                    Сторінка {{ page }} з {{ total }}
                    <span class="arrow arrow-ser-right" v-on:click="getNextPage" v-bind:class="(page == total) ? 'arrow-disab': ''"></span>

                </div>
            </div>
        </div>
    </div>
    <transition name="slide-fade">
    <div id="cartBlock" v-if="cart.length > 0">
        <div class="infoactiv" @click="cartShow = !cartShow"> {{ (!cartShow) ? cart.length : '-' }} </div>
        <transition name="slide-fade">
        <div class="cartList" v-if="cartShow">
            <div class="title">Кошик <button v-on:click="cart = []; cartShow = false; checkoutShow = false">Очистити все <i class="fa fa-times" aria-hidden="true"></i></button></div>
            <ul>
                <li v-for="(item, index) in cart">
                    {{ item.title.rendered }}
                    <span class="price">{{ item.price }} ₴</span>
                    <span class="delete">
                        <input type="checkbox" v-bind:id="item.id" v-model="cart" v-bind:value="item" >
                        <label v-bind:for="item.id" class="fa fa-times"></label>
                    </span>
                </li>
            </ul>
            <div class="buttons">
                <span class="totalPriceCart"> Сума замовлення: <b>{{ totalPriceCart }} грн.</b></span>
                <button @click="checkout">Замовити</button>
            </div>
        </div>
        </transition>

    </div>
    </transition>
    <transition name="slide-fade">
    <div id="checkout" v-if="checkoutShow">
        <i class="fa fa-times close" aria-hidden="true" @click="checkoutShow = !checkoutShow"></i>
        <div class="title">Оформити замовлення</div>
        <form action="">
            <input type="text" name="name" placeholder="ПІБ" v-model="name">
                <transition name="slide-fade">
                <p class="errorname" v-if="error.name"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Потрібно вказати і'мя</p>
                </transition>
            <input type="text" name="phone" v-model="phone" placeholder="(095) 226-0202">
                <transition name="slide-fade">
                <p class="errorphone" v-if="error.phone"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Потрібно вказати номер</p>
                </transition>
            <input type="text" name="mail" placeholder="Mail" v-model="mail">
            <button @click="order($event)">Замовити</button>
        </form>
    </div>
    </transition>
    <transition name="slide-fade">
    <backfoncomp v-if="backfon"></backfoncomp>
    </transition>
    <transition name="slide-fade">
    <success v-if="successOk">
        <p slot="footer"><button @click="successOk = false">Готово</button></p>
    </success>
    </transition>
    <transition name="slide-left">
    <contentinfo v-if="selectInfo">

        <div slot="header" class="header">
            <h2>{{ selectInfo.title.rendered }} <i class="fa fa-times" aria-hidden="true" @click="selectInfo = false"></i></h2>
            <div class="price">Вартість {{ selectInfo.price }} грн.</div>
        </div>
        <div slot="content" v-html="selectInfo.content.rendered" class="content"></div>
        <div slot="footer" class="footer">
            <input type="checkbox" v-bind:id="selectInfo.id" v-model="cart" v-bind:value="selectInfo" >
            <label v-bind:for="selectInfo.id">До кошику</label>
        </div>

    </contentinfo>
    </transition>
</div>
<?php
get_footer();



?>