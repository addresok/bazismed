<?php /* Template Name: 404 */ ?>
<?php
global $wp;
if ($wp->request == 'index.html') {
    wp_redirect('/');
} else if ($wp->request == 'sitemap.xml' || $wp->request == 'sitemap-image.xml') {
    header("Content-Type: text/xml; charset=utf-8");
    echo simplexml_load_string(@file_get_contents(get_home_path() . '/' . $wp->request));
    die;
}
?>
<?php get_header(); ?>
    <div class="page-404">
        <div class="container">
            <div class="row">
                <div class="single col-md-12 col-sm-12 col-xs-12">
                    <div class="big-message">
                        404
                    </div>
                    <div class="message">
                        <?php wp_title(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_404">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                    <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 footer_block">
                        <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
                        <div class="info"><?php echo get_option('addressto'); ?></div>
                    </div>
                    <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 footer_block">
                            <i class="fa fa-envelope-o fa-2x" aria-hidden="true"></i>
                            <div class="info"><?php echo __('Email address'); ?><a href="javascript:void(0);"><?php echo get_option('mailto'); ?></a></div>
                    </div>
                    <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 footer_block">
                        <i class="fa fa-mobile fa-3x" aria-hidden="true"></i>
                        <div class="info">
                            <a href="tel:<?php echo get_option('telephone1'); ?>"><?php echo get_option('telephone1'); ?></a>
                            <a href="tel:<?php echo get_option('telephone'); ?>"><?php echo get_option('telephone'); ?></a></div>
                        </div>
                </div>
            </div>
        </div>
    </div>

<?php wp_footer(); ?>