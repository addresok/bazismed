<!--?php  /* Template Name: Price-old */ ?-->


<?php get_header(); the_post() ;?>
<!--<script type="text/javascript">
	$(document).ready(function() {
		$('.heades').on('click', function(){
			$('.table').removeClass('open');
       		$(this).siblings('.table ').addClass('open');
			var top = $(this).offset();
			//console.log(top);
/*			setTimeout(function(){
                    $('html, body').animate({
                            scrollTop: top.top - 70
                        }, 1000);
                }, 400);*/

		});	
	});
</script>-->


<div class="Price">
	<div class="container">
		<div class="row header_block">
            <div class="col-lg-12">
            <h1 class="title"><?php echo __('Prices for our services'); ?></h1>
            </div>
                <div class="priceSlider">
                <?php $args = array( 'cat' => 10 , 'nopaging' => true );
                $lastposts = new WP_Query($args);
                $blockCounter = 0;
                while ($lastposts->have_posts()): $lastposts->the_post(); $blockCounter++;?>
                    <?php if($blockCounter == 1 ) echo '<div class="item">';?>
                <div id="<?php the_ID(); ?>" class="priceBlock">
                    <span class="heades"><?php the_title(); ?></span>
                    <div class="table animated fadeIn">
                        <?php the_content(); ?>
                    </div>
                </div>
                    <?php if($blockCounter == 4 ){ echo '</div>';$blockCounter = 0;} ?>
                    <?php endwhile; wp_reset_query(); ?>
                </div>
	    </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="activeTable">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="vacancy">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="block">

					<div class="title"><?php echo __('Leave request title text'); ?></div>
					<div class="descr"><?php echo __('Leave request desc text'); ?></div>
                    <?php
                    $currentLang = qtrans_getLanguage();
                    if ($currentLang == 'ru'){
                        echo do_shortcode('[contact-form-7 id="892" title="обращение"]');
                    } elseif ($currentLang == 'ua'){
                        echo do_shortcode('[contact-form-7 id="134" title="Заявка"]');
                    }
                    ?>
				
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>	