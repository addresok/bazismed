<?php
/*
Plugin Name: clients xemlab
Description: Клієнти
Version: 99.9
Author: xemlab Suplyk
Author URI: https://xemlab.com
*/



function initClientsTaxonomy()
{
	register_taxonomy(
		'clients',
		'client',
		array(
			'labels' => array(
				'name' => "Група",
				'add_new_item' => "Добавити",
				'new_item_name' => "Добавити"
			),
			'show_ui' => true,
			'show_tagcloud' => true,
			'hierarchical' => true,
			'rewrite'               => array( 'slug' => 'clients' ),
			'show_in_rest'          => true,
			'rest_base'             => 'clients',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
		)
	);
	register_post_type('client',
		array(
			'labels' => array(
				'name' => "Клієнт",
				'singular_name' => "Клієнти",
				'add_new' => 'Добавити',
				'add_new_item' => "Добавити",
				'edit' => "Редагувати",
				'edit_item' => 'Редактировать',
				'new_item' => 'Нова',
				'view' => 'Переглянути',
				'view_item' => 'Переглянути',
				'search_items' => 'Пошук',
				'not_found' => 'Обзор',
				'not_found_in_trash' => 'Немає записів',
				'parent' => '..'
			),
			'public' => true,
			'menu_position' => 9,
			'supports' => array('title', 'editor', 'thumbnail'),
			'menu_icon' => 'dashicons-format-quote',
			'capability_type'    => 'post',
			'taxonomies'  => array( 'clients'),
			'has_archive' => true,
			'rewrite'            => array( 'slug' => 'client' ),
			'show_in_rest'       => true,
			'rest_base'          => 'client',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);
	flush_rewrite_rules();
}

add_action('init', 'initClientsTaxonomy');


function prepare_rest_client($data, $post, $request){
    $_data = $data->data;

    $thumbnail_id = get_post_thumbnail_id($post->ID);
    $thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
    $thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
    $thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');

    $_data['images']['medium'] = $thumbnail_medium[0];
    $_data['images']['large'] = $thumbnail_large[0];
    $_data['images']['thumbnail'] = $thumbnail_thumbnail[0];


    $data->data = $_data;
    return $data;
}
add_filter('rest_prepare_client', 'prepare_rest_client',10, 3);
