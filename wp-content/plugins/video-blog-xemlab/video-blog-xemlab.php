<?php
/*
Plugin Name: Video Blog
Description: Відео для блогу
Version: 99.9
Author: xemlab Suplyk
Author URI: https://xemlab.com
*/



function initVideoBlogTaxonomy()
{
	register_taxonomy(
		'video-blog',
		'article',
		array(
			'labels' => array(
				'name' => "Категорії Відео-блогу",
				'add_new_item' => "Добавити відео",
				'new_item_name' => "Добавити нове відео"
			),
			'show_ui' => true,
			'show_tagcloud' => true,
			'hierarchical' => true,
			'rewrite'               => array( 'slug' => 'video-blogs' ),
			'show_in_rest'          => true,
			'rest_base'             => 'video-blogs',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
		)
	);
	register_post_type('video',
		array(
			'labels' => array(
				'name' => "Відео",
				'singular_name' => "Відео",
				'add_new' => 'Добавити відео',
				'add_new_item' => "Добавити нове відео",
				'edit' => "Редагувати",
				'edit_item' => 'Редактировать відео',
				'new_item' => 'Новие відео',
				'view' => 'Дивитися',
				'view_item' => 'Дивитися відео',
				'search_items' => 'Пошук відео',
				'not_found' => 'Обзор відео блогу',
				'not_found_in_trash' => 'Немає записів',
				'parent' => 'Батьківська відео'
			),
			'public' => true,
			'menu_position' => 7,
			'supports' => array('title', 'editor'),
			'taxonomies' => array('video-blog'),
			'menu_icon' => 'dashicons-video-alt3',
			'capability_type'    => 'post',
		
			'has_archive' => true,
			'rewrite'            => array( 'slug' => 'video' ),
			'show_in_rest'       => true,
			'rest_base'          => 'video',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);
	flush_rewrite_rules();
}

add_action('init', 'initVideoBlogTaxonomy');