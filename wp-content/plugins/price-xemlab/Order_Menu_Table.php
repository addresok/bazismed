<?php
/**
 * Created by IntelliJ IDEA.
 * User: sonet
 * Date: 23.09.17
 * Time: 19:56
 */

class Order_Menu_Table {
	/**
	 * Подготавливаем колонки таблицы для их отображения
	 *
	 */
	public function prepare_items()
	{
		//Sets
		$per_page = 5;

		/* Получаем данные для формирования таблицы */
		$data = $this -> table_data();

		/* Устанавливаем данные для пагинации */
		$this -> set_pagination_args( array(
			'total_items' => count($data),
			'per_page'    => $per_page
		));

		/* Делим массив на части для пагинации */
		$data = array_slice(
			$data,
			(($this -> get_pagenum() - 1) * $per_page),
			$per_page
		);

		/* Устанавливаем данные колонок */
		$this -> _column_headers = array(
			$this -> get_columns(), /* Получаем массив названий колокнок */
			$this -> get_hidden_columns(), /* Получаем массив названий колонок которые нужно скрыть */
			$this -> get_sortable_columns() /* Получаем массив названий колонок которые можно сортировать */
		);

		/* Устанавливаем данные таблицы */
		$this -> items = $data;
	}

	/**
	 * Название колонок таблицы
	 *
	 * @return array
	 */
	public function get_columns()
	{
		return array(
			'ID'			    => 'ID',
			'order_name'		=> 'ПІБ',
			'order_phone'		=> 'Телефон',
			'order_mail'        => 'Пошта',
			'order_status'		=> 'Статус',
			'order_date'		=> 'Створено',
		);
	}

	/**
	 * 				'order_name'     => $_POST['name'],
				'order_phone'    => $_POST['phone'],
				'order_mail'     => $_POST['mail'],
				'order_total'    => $_POST['total'],
				'order_status'   => 1,
				'order_date'     => current_time('mysql', 1),
				'order_modified' => current_time('mysql', 1),
	 */


	/**
	 * Массив колонок которые нужно скрыть
	 *
	 * @return array
	 */
	public function get_hidden_columns()
	{
		return array();
	}

	/**
	 * Массив названий колонок по которым выполняется сортировка
	 *
	 * @return array
	 */
	public function get_sortable_columns()
	{
		return array(
			'ID' => array('ID', false),
			'order_name' => array('order_name', true),
			'order_mail' => array('order_mail', false),
			'order_status' => array('order_status', false),
		);
	}

	/**
	 * Данные таблицы
	 *
	 * @return array
	 */
	private function table_data()
	{
		return array(
			array(
				'ex_id'			=> 1,
				'ex_title'		=> 'Уличный кот по имени Боб',
				'ex_author'		=> 'Боуэн Джеймс',
				'ex_description'=> 'Мировой бестселлер!Готовится экранизация! Он был одиноким бездомным музыкантом, но увидел на',
				'ex_price'		=> '61,00',
			),
			array(
				'ex_id'			=> 2,
				'ex_title'		=> 'Святые иконы. Как правильно просить',
				'ex_author'		=> '',
				'ex_description'=> 'Когда человек в отчаянии, он перед иконами обращает свои молитвы к святым заступникам. Но не все знают...',
				'ex_price'		=> '38,80',
			),
			array(
				'ex_id'			=> 3,
				'ex_title'		=> 'До встречи с тобой',
				'ex_author'		=> 'Мойес Джоджо',
				'ex_description'=> 'Впервые на русском языке! Книга вошла в список бестселлеров «Нью-Йорк таймс», переведена на 31 язык...',
				'ex_price'		=> '60,50',
			),
			array(
				'ex_id'			=> 4,
				'ex_title'		=> 'Искусство войны',
				'ex_author'		=> 'Сунь Цзы',
				'ex_description'=> 'Один из самых известных трудов древности! Этот трактат не утратил своей актуальности за 2500 лет!..',
				'ex_price'		=> '111,00',
			),
			array(
				'ex_id'			=> 5,
				'ex_title'		=> 'Дім дивних дітей',
				'ex_author'		=> 'Риггз Ренсом',
				'ex_description'=> 'Інтригуюча історія, що зацікавила кращих режисерів Голлівуду!Таємничий острів. Покинутий притулок. П...',
				'ex_price'		=> '35,40',
			),
			array(
				'ex_id'			=> 6,
				'ex_title'		=> 'Я вижу вас насквозь. Научитесь читать человека к',
				'ex_author'		=> 'Мессинджер Жозеф',
				'ex_description'=> 'Авторы с многолетним стажем изучения жестов человека раскрывают тайны языка тела! Собеседник не смо...',
				'ex_price'		=> '44,30',
			),
			array(
				'ex_id'			=> 7,
				'ex_title'		=> 'Салаты. Овощные. Мясные',
				'ex_author'		=> 'Гагарина А',
				'ex_description'=> 'Коллекция из более чем 400 вкусных салатов, легких в приготовлении!Разнообразные по вкусу и сочетани...',
				'ex_price'		=> '39,90',
			),
			array(
				'ex_id'			=> 8,
				'ex_title'		=> 'Зона покриття',
				'ex_author'		=> 'Кинг Стивен',
				'ex_description'=> 'Якщо хтось і здатний написати Великий Містичний Роман-Катастрофу, то це — Стівен Кінг, найуспішніший...',
				'ex_price'		=> '44,30',
			),
			array(
				'ex_id'			=> 9,
				'ex_title'		=> 'Українська вишивка: Рушники',
				'ex_author'		=> '',
				'ex_description'=> 'Українське — це модно! Українська вишивка зачаровує і привертає до себе увагу в усьому світі...',
				'ex_price'		=> '55,40',
			),
			array(
				'ex_id'			=> 10,
				'ex_title'		=> 'Під Куполом',
				'ex_author'		=> 'Кинг Стивен',
				'ex_description'=> 'Осінньої днини на містечко Честер Мілл опустився прозорий Купол, через який неможливо пройти...',
				'ex_price'		=> '89,90',
			),
			array(
				'ex_id'			=> 11,
				'ex_title'		=> 'Втрачений символ',
				'ex_author'		=> 'Браун Дэн',
				'ex_description'=> 'Світовий бестселер! На межі життя та смерті Ленґдон розгадує секрет масонів. Але чи потрібно людям знати його?',
				'ex_price'		=> '44,30',
			),
			array(
				'ex_id'			=> 12,
				'ex_title'		=> 'КАРТИ. Ілюстрована мандрівка',
				'ex_author'		=> 'Мізелінські Олександра і...',
				'ex_description'=> 'Ця книга запрошує у захоплюючу навколосвітню подорож. Ви побачите могутні гейзери Ісландії, довгі ка...',
				'ex_price'		=> '241,00',
			),
			array(
				'ex_id'			=> 13,
				'ex_title'		=> 'Кульбабове вино',
				'ex_author'		=> 'Брэдбери Рэй',
				'ex_description'=> 'Події повісті Рея Бредбері «Кульбабове вино» розгортаються влітку 1928 року у вигаданому містечку Гр… ',
				'ex_price'		=> '45,90',
			),
		);
	}

	/**
	 * Возвращает содержимое колонки
	 *
	 * @param  array $item массив данных таблицы
	 * @param  string $column_name название текущей колонки
	 *
	 * @return mixed
	 */
	public function column_default($item, $column_name )
	{
		switch($column_name)
		{
			case 'ID':
			case 'order_name':
			case 'order_phone':
			case 'order_mail':
			case 'order_status':
			case 'order_date':
				return $item[$column_name];
			default:
				return print_r($item, true);
		}
	}
}

/**
 * 			'ID'			    => 'ID',
'order_name'		=> 'ПІБ',
'order_phone'		=> 'Телефон',
'order_mail'        => 'Пошта',
'order_status'		=> 'Статус',
'order_date'		=> 'Створено',
 */