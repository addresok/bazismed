<?php
/*
Plugin Name: Price
Description: Ціни
Version: 99.9
Author: xemlab Suplyk
Author URI: https://xemlab.com
*/



function initPricesTaxonomy()
{
	register_taxonomy(
		'prices',
		array('price', 'doctor'),
		array(
			'labels' => array(
				'name' => "Категорії Послуг",
				'add_new_item' => "Добавити послугу",
				'new_item_name' => "Добавити нову послугу"
			),
			'show_ui' => true,
			'show_tagcloud' => true,
			'hierarchical' => true,
			'rewrite'               => array( 'slug' => 'prices' ),
			'show_in_rest'          => true,
			'rest_base'             => 'prices',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
		)
	);
	register_post_type('price',
		array(
			'labels' => array(
				'name' => "Послуги",
				'singular_name' => "Послуги",
				'add_new' => 'Добавити послугу',
				'add_new_item' => "Добавити нову послугу",
				'edit' => "Редагувати",
				'edit_item' => 'Редагувати послугу',
				'new_item' => 'Новиа послуга',
				'view' => 'Переглянути',
				'view_item' => 'Переглянути послугу',
				'search_items' => 'Пошук',
				'not_found' => 'Обзор послуг',
				'not_found_in_trash' => 'Немає записів',
				'parent' => 'Основна послуга'
			),
			'public' => true,
			'menu_position' => 7,
			'supports' => array('title','thumbnail', 'editor'),
			'menu_icon' => 'dashicons-list-view',
			'capability_type'    => 'post',
			'taxonomies'  => array( 'prices'),
			'has_archive' => true,
			'rewrite'            => array( 'slug' => 'price' ),
			'show_in_rest'       => true,
			'rest_base'          => 'price',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);
	flush_rewrite_rules();
}

add_action('init', 'initPricesTaxonomy');



function prepare_rest_price($data, $post, $request){
	$_data = $data->data;


    $thumbnail_id = get_post_thumbnail_id($post->ID);

    $thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
    $thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
    $thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');

    $_data['images']['medium'] = $thumbnail_medium[0];
    $_data['images']['large'] = $thumbnail_large[0];
    $_data['images']['thumbnail'] = $thumbnail_thumbnail[0];

	$filds = get_post_custom($post->ID);

    //$_data['fil'] = $filds;
    $_data['excerpt'] = '';



	$price = ( isset($filds['_price'][0]) && trim($filds['_price'][0])) ? $filds['_price'][0] : '0';
	$termin = ( isset($filds['_time'][0]) && trim($filds['_time'][0])) ? $filds['_time'][0] : '0';
	$symptom = ( isset($filds['_symptom'][0]) && trim($filds['_symptom'][0])) ? $filds['_symptom'][0] : '';
	$diagnostics = ( isset($filds['_diagnostics'][0]) && trim($filds['_diagnostics'][0])) ? $filds['_diagnostics'][0] : '';
	$healing = ( isset($filds['_healing'][0]) && trim($filds['_healing'][0])) ? $filds['_healing'][0] : '';

	//$_data['filds'] = $filds;
	$_data['price'] = $price;
	$_data['termin'] = $termin;
	$_data['symptom'] = $symptom;
	$_data['diagnostics'] = $diagnostics;
	$_data['healing'] = $healing;
	$_data['select'] = 0;

    $main_tab_t_1 = trim((isset($filds['_main_teb_t_1_title'][0]) && trim($filds['_main_teb_t_1_title'][0])) ? $filds['_main_teb_t_1_title'][0] : '');
    $_data['main_tab_t_1'] = false;
    if($main_tab_t_1){
        $main_teb_t_1_icon_id = carbon_get_post_meta( $post->ID, 'main_teb_t_1_icon');
        $main_teb_t_1_bg_id     = carbon_get_post_meta( $post->ID, 'main_teb_t_1_bg');



        $_data['main_tab_t_1'] = [
            'title' => trim($main_tab_t_1),
            'icon'  => wp_get_attachment_image_url( $main_teb_t_1_icon_id, 'full' ),
            'bg'    => wp_get_attachment_image_url( $main_teb_t_1_bg_id, 'full' ),
            'list'  => carbon_get_post_meta( $post->ID, 'main_teb_t_1'),


        ];

    }



	$_data['cat'] = (isset($_data['prices'][0])) ? get_term( $_data['prices'][0], 'prices' )->name : 'не задано';
	$data->data = $_data;



	return $data;
}
add_filter('rest_prepare_price', 'prepare_rest_price',10, 3);

function wp_get_cat_postnum($id) {
	$cat = get_category($id);
	$count = (int) $cat->count;
	$taxonomy = 'prices';
	$args = array(
		'child_of' => $id,
	);
	$tax_terms = get_terms($taxonomy,$args);
	foreach ($tax_terms as $tax_term) {


		$count +=$tax_term->count;
	}
	return $count;
}


function prepare_rest_prices($data, $cat, $request){
	$_data = $data->data;




	$_data['count_all'] = wp_get_cat_postnum($cat->term_id);
	$_data['img'] = get_the_category_data($cat->term_id)->url;
	$data->data = $_data;



	return $data;
}
add_filter('rest_prepare_prices', 'prepare_rest_prices',10, 3);