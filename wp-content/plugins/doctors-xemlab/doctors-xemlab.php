<?php
/*
Plugin Name: Doctors
Description: Лікарі
Version: 99.9
Author: xemlab Suplyk
Author URI: https://xemlab.com
*/



function initDoctorsTaxonomy()
{
    register_taxonomy(
        'specialization',
        'doctor',
        array(
            'labels' => array(
                'name' => "Спеціалізації",
                'add_new_item' => "Добавити",
                'new_item_name' => "Добавити"
            ),
            'show_ui' => true,
            'show_tagcloud' => true,
            'hierarchical' => true,
            'rewrite'               => array( 'slug' => 'specialization' ),
            'show_in_rest'          => true,
            'rest_base'             => 'specialization',
            'rest_controller_class' => 'WP_REST_Terms_Controller',
        )
    );
    register_post_type('doctor',
        array(
            'labels' => array(
                'name' => "Лікарі",
                'singular_name' => "Лікарі",
                'add_new' => 'Добавити лікаря',
                'add_new_item' => "Добавити",
                'edit' => "Редагувати",
                'edit_item' => 'Редагувати ',
                'new_item' => 'Новиа послуга',
                'view' => 'Переглянути',
                'view_item' => 'Переглянути',
                'search_items' => 'Пошук',
                'not_found' => 'Обзор',
                'not_found_in_trash' => 'Немає записів',
                'parent' => 'Основна послуга'
            ),
            'public' => true,
            'menu_position' => 7,
            'supports' => array('title','thumbnail', 'editor'),
            'menu_icon' => 'dashicons-businessman',
            'capability_type'    => 'post',
            'taxonomies'  => array( 'prices', 'specialization'),
            'has_archive' => true,
            'rewrite'            => array( 'slug' => 'doctor' ),
            'show_in_rest'       => true,
            'rest_base'          => 'doctor',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
        )
    );
    flush_rewrite_rules();
}

add_action('init', 'initDoctorsTaxonomy');



function prepare_rest_doctor($data, $post, $request){
    $_data = $data->data;


    $filds = get_post_custom($post->ID);


    $thumbnail_id = get_post_thumbnail_id($post->ID);

    $thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
    $thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
    $thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');

    $_data['images']['medium'] = $thumbnail_medium[0];
    $_data['images']['large'] = $thumbnail_large[0];
    $_data['images']['thumbnail'] = $thumbnail_thumbnail[0];

    /*$price = ( isset($filds['pruce'][0]) && trim($filds['pruce'][0])) ? $filds['pruce'][0] : '0';
    $termin = ( isset($filds['termin'][0]) && trim($filds['termin'][0])) ? $filds['termin'][0] : '0';

    //$_data['filds'] = $filds;
    $_data['price'] = $price;
    $_data['termin'] = $termin;
    $_data['select'] = 0;
    $_data['cat'] = (isset($_data['prices'][0])) ? get_term( $_data['prices'][0], 'prices' )->name : 'не задано';

    */

    $blog_name = wp_get_object_terms($post->ID, 'specialization');
    $_data['specialization_name'] = $blog_name[0]->name;

    $practik = ( isset($filds['_doctor_start_practik'][0]) && trim($filds['_doctor_start_practik'][0])) ? $filds['_doctor_start_practik'][0] : '0';


    $_data['practik'] = (($practik) ? (int) date('Y') - (int) date('Y',  strtotime($practik)) : 0);
    $_data['teaching'] = carbon_get_post_meta($post->ID, 'teaching');
    $_data['experience'] = carbon_get_post_meta($post->ID, 'experience');



    $data->data = $_data;



    return $data;
}
add_filter('rest_prepare_doctor', 'prepare_rest_doctor',10, 3);

//function wp_get_cat_postnum($id) {
//	$cat = get_category($id);
//	$count = (int) $cat->count;
//	$taxonomy = 'doctors';
//	$args = array(
//		'child_of' => $id,
//	);
//	$tax_terms = get_terms($taxonomy,$args);
//	foreach ($tax_terms as $tax_term) {
//
//
//		$count +=$tax_term->count;
//	}
//	return $count;
//}


