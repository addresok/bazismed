<?php
/*
Plugin Name: compex prog
Description: compex
Version: 99.9
Author: xemlab Suplyk
Author URI: https://xemlab.com
*/



function initCompexTaxonomy()
{

	register_post_type('compex',
		array(
			'labels' => array(
				'name' => "Комплексні программи",
				'singular_name' => "Комплексні программи",
				'add_new' => 'Добавити',
				'add_new_item' => "Добавити",
				'edit' => "Редагувати",
				'edit_item' => 'Редактировать',
				'new_item' => 'Нова',
				'view' => 'Переглянути',
				'view_item' => 'Переглянути',
				'search_items' => 'Пошук',
				'not_found' => 'Обзор',
				'not_found_in_trash' => 'Немає записів',
				'parent' => '..'
			),
			'public' => true,
			'menu_position' => 9,
			'supports' => array('title', 'editor', 'thumbnail','excerpt'),
			'menu_icon' => 'dashicons-format-gallery',
			'capability_type'    => 'post',
			'taxonomies'  => array( 'compexx'),
			'has_archive' => true,
			'rewrite'            => array( 'slug' => 'compex' ),
			'show_in_rest'       => true,
			'rest_base'          => 'complex',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);
	flush_rewrite_rules();
}

add_action('init', 'initCompexTaxonomy');

function prepare_rest_compex($data, $post, $request){
    $_data = $data->data;


    $filds = get_post_custom($post->ID);
    $thumbnail_id = get_post_thumbnail_id($post->ID);
    $thumbnail_medium = wp_get_attachment_image_src($thumbnail_id, 'medium');
    $thumbnail_large = wp_get_attachment_image_src($thumbnail_id, 'large');
    $thumbnail_thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');
    $thumbnail_full = wp_get_attachment_image_src($thumbnail_id, 'full');
    $_data['images']['medium'] = $thumbnail_medium[0];
    $_data['images']['large'] = $thumbnail_large[0];
    $_data['images']['thumbnail'] = $thumbnail_thumbnail[0];
    $_data['images']['full'] = $thumbnail_thumbnail[0];



     $data->data = $_data;



    return $data;
}
add_filter('rest_prepare_compex', 'prepare_rest_compex',10, 3);