<?php
/*
Plugin Name: Vacancies
Description: Вакансії
Version: 99.9
Author: xemlab Suplyk
Author URI: https://xemlab.com
*/



function initVacanciesTaxonomy()
{
	register_taxonomy(
		'vacancies',
		'vacance',
		array(
			'labels' => array(
				'name' => "Категорії",
				'add_new_item' => "Добавити вакансію",
				'new_item_name' => "Добавити нова вакансія"
			),
			'show_ui' => true,
			'show_tagcloud' => true,
			'hierarchical' => true,
			'rewrite'               => array( 'slug' => 'vacancies' ),
			'show_in_rest'          => true,
			'rest_base'             => 'vacancies',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
		)
	);
	register_post_type('vacance',
		array(
			'labels' => array(
				'name' => "Вакансія",
				'singular_name' => "Вакансія",
				'add_new' => 'Добавити Вакансію',
				'add_new_item' => "Добавити нову Вакансію",
				'edit' => "Редагувати",
				'edit_item' => 'Редактировать Вакансію',
				'new_item' => 'Новиа Вакансія',
				'view' => 'Дивитися',
				'view_item' => 'Дивитися Вакансію',
				'search_items' => 'Пошук Вакансії',
				'not_found' => 'Обзор Вакансій',
				'not_found_in_trash' => 'Немає записів',
				'parent' => 'Батьківська Вакансія'
			),
			'public' => true,
			'menu_position' => 7,
			'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields'),
			'menu_icon' => 'dashicons-forms',
			'capability_type'    => 'post',
			'taxonomies'  => array( 'vacancies'),
			'has_archive' => true,
			'rewrite'            => array( 'slug' => 'vacance' ),
			'show_in_rest'       => true,
			'rest_base'          => 'vacance',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);
	flush_rewrite_rules();
}

add_action('init', 'initVacanciesTaxonomy');