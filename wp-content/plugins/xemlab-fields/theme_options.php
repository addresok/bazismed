<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
    Container::make( 'theme_options', __( 'Настройка темы', 'theme' ) )
        ->add_tab( 'Основное', array(
		        Field::make('text', 'title', 'Заголовок'),
		        Field::make('text', 'sub_title', 'Підзаголовок'),
		        Field::make("rich_text", "text", "текст")
        ) )

        ->add_tab( 'Контакты', array(

            Field::make( 'text', 'cont_otdel_name', 'Название отдела' ),
            Field::make( 'text', 'cont_number', 'Номер' )->set_width( 50 ),
            Field::make( 'text', 'cont_email', 'Email' )->set_width( 50 ),



            Field::make( "map", "crb_company_location", "Местоположение - офис" )
                ->help_text( 'Перетащите указатель на карту, чтобы выбрать местоположение' ),


            Field::make( "map", "crb_company_location_dev", "Местоположение - производство" )
                ->help_text( 'Перетащите указатель на карту, чтобы выбрать местоположение' ),



            Field::make( 'text', 'url_fb', 'Фейсбук' )->set_width( 50 ),
            Field::make( 'text', 'url_vk', 'вКонтакте' )->set_width( 50 ),
            Field::make( 'text', 'url_tw', 'Твиттер' )->set_width( 50 ),
            Field::make( 'text', 'url_in', 'Инстаграм' )->set_width( 50 ),

        ) );

}