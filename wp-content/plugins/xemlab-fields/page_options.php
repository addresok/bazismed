<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;




add_action( 'carbon_fields_register_fields', 'crb_attach_price_options' );
function crb_attach_price_options() {
	Container::make( 'post_meta', __( 'Налаштування', 'price' ) )
			 ->show_on_post_type('price')

		->add_tab( 'Описи', array(
			Field::make("rich_text", "symptom", "Симптоми"),
			Field::make("rich_text", "diagnostics", "Діагностика"),
			Field::make("rich_text", "healing", "Лікування"),
			Field::make("rich_text", "risk_factors", "Фактори ризику"),
            )
        )
        ->add_tab( 'Головна вкладка (тип 1)', array(
                Field::make("image", "main_teb_t_1_icon", "Іконка")->set_width( 20 ),
                Field::make("image", "main_teb_t_1_bg", "Тло")->set_width( 20 ),
                Field::make( 'text', 'main_teb_t_1_title', 'Заголовок' )->set_width( 60 ),
                Field::make( 'complex', 'main_teb_t_1', 'Головна вкладка (тип 1)' )
                    ->add_fields( array(
                        Field::make( 'text', 'title', 'Підзаголовок' )->set_width( 100 ),
                        Field::make( 'rich_text', 'text', 'Опис' )->set_width( 100 )
                    ) )
            )
        );

    Container::make( 'post_meta', 'Інформація' )
        ->show_on_post_type('price')
        ->set_context('side')
        ->add_fields(array(

            Field::make("text", "price", "Вартість")
                ->set_default_value(0),
            Field::make("text", "time", "Термін")
                ->set_default_value(0),
        ));

}

add_action( 'carbon_fields_register_fields', 'crb_attach_doctor_options' );
function crb_attach_doctor_options() {
    Container::make( 'post_meta', __( 'Описи', 'doctor' ) )
        ->show_on_post_type('doctor')
        ->add_tab( 'Навчання', array(
            Field::make( 'complex', 'teaching', 'Навчання' )
                ->add_fields( array(
                    Field::make( 'text', 'term', 'Термін' )->set_width( 20 ),
                    Field::make( 'text', 'title', 'Назва' )->set_width( 80 )
                ) )
            )
        )
        ->add_tab( 'Досвід роботи', array(
                Field::make( 'complex', 'experience', 'Досвід роботи' )
                    ->add_fields( array(
                        Field::make( 'text', 'term', 'Термін' )->set_width( 20 ),
                        Field::make( 'text', 'title', 'Назва' )->set_width( 80 )
                    ) )
            )
        )
        ->add_tab( 'Соц. мережі', array(
                Field::make( 'complex', 'social', 'Соц. мережі' )
                    ->add_fields( array(
                        Field::make( 'text', 'link', 'Посилання' )->set_width( 80 ),
                        Field::make("select", "social", "Сой. мережа")
                            ->add_options(array(
                                'vk' => 'VK',
                                'fb' => 'facebook',
                                'tv' => 'twitter',
                            ))->set_width( 20 ),
                    ) )
            )
        );

    Container::make( 'post_meta', 'Інформація' )
        ->show_on_post_type('doctor')
        ->set_context('side')
        ->add_fields(array(

            Field::make("date", "doctor_start_practik", "Початок практики")
                ->help_text('Від цієї дати вираховуємо стаж')

        ));

}







/*
add_action( 'carbon_fields_register_fields', 'crb_attach_portfolio_texts' );
function crb_attach_portfolio_texts() {
	Container::make( 'term_meta', __( 'Тексти..', 'portfolio_text_header' ) )
	         ->show_on_taxonomy( 'portfolio' )
	         ->add_tab( 'Тексти для шапки', array(
		         Field::make('text', 'sub_title', 'Підзаголовок'),
		         Field::make("rich_text", "text", "текст"),


	         ) );

}*/