<?php
/**
 * Created by IntelliJ IDEA.
 * User: sonet
 * Date: 23.09.17
 * Time: 20:07
 */

class Order_Menu_Table_Create {

	public function __construct()
	{
		add_action('admin_menu', array($this, 'createMenu'));
	}


	public function createMenu()
	{
		add_menu_page('Замовлення',
			'Замовлення',
			'manage_options',
			'order_table',
			array(
				$this, 'createTable'
			));


		add_submenu_page('order_table',
			'Статуси',
			'Статуси',
			'manage_options',
			'statuses',
			array(
				$this, 'statuses'
			));
	}

	public function statuses() {
		require_once WP_PLUGIN_DIR . '/bm-orders/tpl/status.php';
		add_action('admin_print_footer_scripts', 'my_action_javascript', 99);
	}

	public function createTable()
	{
		$Table = new Order_Menu_Table();
		$Table -> prepare_items();

		?>
		<div class="wrap" id="orders">
			<h2>Замовлення</h2>
			<?php $Table -> display(); ?>

            <div  class="orderinfo" v-if="item != 1">
                <p style="font-size: 18px;">Замовлення № {{ item.ID }}</p>
                <ul class="menuedit">
                    <li @click="updateItem(item.ID, item.order_status)">Зберегти</li>
                    <li @click="item = 1">Закрити</li>
                </ul>
                <hr>
                <div class="info">
                    <p><span>ПІБ</span> {{ item.order_name }}</p>
                    <p><span>Телефон</span> {{ item.order_phone }}</p>
                    <p><span>Mail</span> {{ item.order_mail }}</p>
                    <p><span>Створено</span> {{ item.order_date }}</p>
                    <p><span>Змінено</span> {{ item.order_modified }}</p>
                </div>
                <div class="spatus"> Статус
                    <select v-model="selected">
                        <option v-for="item in options" v-bind:value="item.ID">
                            {{ item.title }}
                        </option>
                    </select>
                </div>
                <div class="products" >
                    <div class="head" style="font-size: 14px;font-weight: bold;border: none;margin-top: 30px;"><span class="id">#</span><span class="price">Цена</span> Назва</div>
                </div>
                <ul class="products" >
                    <li class="head" v-for="(prod, index) in item.product">
                        <span class="id">{{index + 1}}</span>
                        <span class="price">{{prod.price}}</span>
                        <a v-bind:href="'/wp-admin/post.php?post='+prod.ID_price+'&action=edit'" target="_blank">{{ prod.title }}</a></li>
                </ul>


            </div>
		</div>

        <style>
            .orderinfo {
                position: fixed;
                top: 0;
                right: -4px;
                padding: 60px 30px 30px 30px;
                background-color: #fbfbfb;
                border: 3px solid #f7d2d0;
                width: 50vw;
                height: 100vh;
                z-index: 1;
            }

            .orderinfo .info {     }
            .orderinfo .info p {   font-weight: 600;   }
            .orderinfo .info p span {
                display: inline-block;
                width: 150px;
                font-weight: 400;
            }

            .menuedit {
                position: absolute;
                top: 50px;
                right: 30px;
            }

            .menuedit li span {

            }
            .menuedit li {
                display: inline-block;
                margin-left: 20px;
                border: 1px solid red;
                font-size: 18px;
                padding: 5px 22px;
                cursor: pointer;
            }
            ul.products {
                list-style: none;
                height: 40vh;
                overflow-y: auto;
            }
            .products .head .id {
                width: 30px;
                position: absolute;
                left: 0;
                text-align: center;
            }
            .products .head a { text-decoration: none}
            .products .head {
                position: relative;
                display: block;
                padding-left: 110px;
                border-top: 1px solid;
                margin-bottom: 0;
                padding-top: 2px;
                padding-bottom: 2px;
            }

            .products .head .price {
                width: 80px;
                position: absolute;
                left: 30px;
                text-align: center;
            }

        </style>

		<?php
		add_action('admin_print_footer_scripts', 'my_action_javascript', 99);
	}
}