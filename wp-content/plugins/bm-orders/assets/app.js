jQuery(document).ready(function($) {


});
Vue.use(VueResource);
var httpP = {
    root: '/wp-admin/admin-ajax.php',
    headers: {
        Authorization: 'Basic YXBpOnBhc3N3b3Jk'
    }
};


var satusesEl = document.getElementById('satuses');
var ordersEl = document.getElementById('orders');

if(ordersEl) var satuses = new Vue({
    el: '#orders',
    http: httpP,
    data: {
        item: 1,
        isTrue: false,
        selected: 0,
        options: [

        ]

    },


    mounted: function () {

        this.getList();
        //this.getItem();
    },

    methods: {

        getList: function () {
            this.$http.get('?action=statuses').then(function (r) {
                this.options = r.body;

            }, function (r) {
                console.log(r);
            });
        },

        start: function () {
            console.log('start');
        },

        getItem: function (id) {

            var that = this,
                data = { action: 'getorder', 'id': id };
            jQuery.post( '/wp-admin/admin-ajax.php', data, function(response) {
                //console.log('Success!:', response);
                that.item = jQuery.parseJSON(response);
                that.selected = that.item.order_status;


            });

        },
        updateItem: function (id, status) {

            var that = this,
                data = {
                action: 'updateorder',
                    'id': id ,
                    'status':this.selected
            };
            jQuery.post( '/wp-admin/admin-ajax.php', data, function(response) {
                console.log('Success!:', response);
                that.item = 1;
                location.reload();



            });

        },


    },

});


if(satusesEl) var satuses = new Vue({
    el: '#satuses',
    http: httpP,
    data: {
        message: 'Hello Vue!',
        list: '',
        newItem:'',
        error: false,


    },


    mounted: function () {
        this.start();

        this.getList();


    },

    methods: {
        start: function () {
            console.log('start');
        },

        getList: function () {
            this.$http.get('?action=statuses').then(function (r) {
                this.list = r.body;

            }, function (r) {
                console.log(r);
            });
        },

        add: function () {
            if (this.newItem.length === 0) return this.error = true;
            this.$http.get('?action=statusesadd&title=' + this.newItem).then(function (r) {
                this.error = false;
                this.newItem = '';
                this.getList();
            }, function (r) {
                console.log(r);
            });
        },
        dell: function (id) {
            var isTrue = confirm("Ви впевнені?");
            if (!isTrue) return false;

            this.$http.get('?action=statusesdell&id=' + id).then(function (r) {
                this.getList();
            }, function (r) {
                console.log(r);
            });
        }

    },

});