<?php

/*
Plugin Name: Orders
Description: Замовлення
Version: 1.0
Author: xemlab Suplyk
Author URI: https://xemlab.com
*/

defined('ABSPATH') or die('No script kiddies please!');

/**
 * Подключаем базовый класс
 */
if(class_exists('WP_List_Table') == FALSE)
{
	require_once(ABSPATH.'wp-admin/includes/class-wp-list-table.php');
}



require_once WP_PLUGIN_DIR . '/bm-orders/Order_Menu_Table.php';
require_once WP_PLUGIN_DIR . '/bm-orders/Order_Menu_Table_Create.php';


/**
 * Отображаем таблицу лишь в том случае, если пользователь администратор
 */
if(is_admin() == TRUE)
{
	new Order_Menu_Table_Create();
}




/**
 * Ajax Callback
 */

function my_action_javascript() { ?>
	<script type='text/javascript' src='/wp-content/themes/med/js/vue.min.js?ver=4.7.1'></script>
	<script type='text/javascript' src='/wp-content/themes/med/js/vue-resource.min.js?ver=4.7.1'></script>
	<script type='text/javascript' src='/wp-content/plugins/bm-orders/assets/app.js?ver=4.7.1'></script>
<?php  }




add_action( 'wp_ajax_statuses', 'statuses' );

function statuses(){
	global $wpdb;
	$data= $wpdb->get_results("SELECT * FROM wp_order_status");
	echo json_encode($data);
	wp_die();
}
add_action( 'wp_ajax_statusesadd', 'statuses_add' );
function statuses_add(){
	global $wpdb;

	if (isset($_GET) && isset($_GET['title']))
	$wpdb->insert(
		'wp_order_status',
		array( 'title' => $_GET['title']),
		array( '%s' )
	);

	wp_die();
}
add_action( 'wp_ajax_statusesdell', 'statuses_dell' );
function statuses_dell(){
	global $wpdb;

	if (isset($_GET) && isset($_GET['id']))
		$wpdb->delete( 'wp_order_status', array( 'ID' => $_GET['id'] ) );

	wp_die();
}

add_action( 'wp_ajax_orderadd', 'order_add' );
add_action('wp_ajax_nopriv_orderadd', 'order_add');
function order_add(){
	global $wpdb;

	if (isset($_POST) && isset($_POST['action']) && $_POST['action'] === 'orderadd') {

        $body = '<h1>Додано неве замовлення!</h1>';

		$wpdb->insert(
			'wp_orders',
			[
				'order_name'     => $_POST['name'],
				'order_phone'    => $_POST['phone'],
				'order_mail'     => $_POST['mail'],
				'order_total'    => $_POST['total'],
				'order_status'   => 1,
				'order_date'     => current_time('mysql', 1),
				'order_modified' => current_time('mysql', 1),
			],[
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
				'%s',
			]

		);

        $body .= '<p><b>Імя:<b>' . $_POST['name'] . '<p>';
        $body .= '<p><b>Телефон:<b>' . $_POST['phone'] . '<p>';
        $body .= '<p><b>Пошта:<b>' . $_POST['mail'] . '<p>';
        $body .= '<p><b>Сума:<b>' . $_POST['total'] . '<p>';
        $body .= '<p><b>Послуги:<b>' . $_POST['total'] . '<p>';
        $body .= '<ul>';

		$lastid = $wpdb->insert_id;
		foreach ($_POST['cart'] as $item){

            $body .= '<p><b>' . $item['title'] . ':<b>' . $item['price'] . '<p>';

            $wpdb->insert(
				'wp_orders_product',
				[
					'ID_order' => $lastid,
					'ID_price' => $item['id'],
					'title'    => $item['title'],
					'price'    => $item['price'],
				],[
					'%d',
					'%d',
					'%s',
					'%d',
				]

			);
		}
        $body .= '</ul>';
        $to = 'info@bazismed.com';
		$subject = 'Замовлення';
		$body = 'Додано неве замовлення!';
		$headers = array('Content-Type: text/html; charset=UTF-8');

		wp_mail( $to, $subject, $body, $headers );
	}
	echo 1;
	wp_die();
}

add_action( 'wp_ajax_getorder', 'get_order' );
function get_order(){

	global $wpdb;
    if(isset($_POST['id'])){

	    $selectOrder = 'SELECT * FROM wp_orders WHERE ID = ' . (int) $_POST['id'];
	    $data = $wpdb->get_row($selectOrder, ARRAY_A);



	    $selectProduct = 'SELECT * FROM wp_orders_product WHERE ID_order = ' . (int) $_POST['id'];
	    $dataProduct= $wpdb->get_results($selectProduct, ARRAY_A);

	    $data['product'] = $dataProduct; // ? $dataProduct : [];
	    echo json_encode($data);
    } else echo 1;
	wp_die();
}
add_action( 'wp_ajax_updateorder', 'update_order' );
function update_order(){

	global $wpdb;
    if(isset($_POST['id'])){
	    $wpdb->update(
		    'wp_orders',
		    array(
			    'order_status' => $_POST['status'],
		    ),
		    array( 'ID' => $_POST['id'] ),
		    array(
			    '%d'
		    ),
		    array( '%d' )
	    );
        echo 1;
    }
	wp_die();
}











add_action( 'wp_ajax_callme', 'call_me' );
add_action('wp_ajax_nopriv_callme', 'call_me');
function call_me(){
    global $wpdb;

    $mail =  $site_title = get_bloginfo( 'admin_email' );


    if (isset($_POST)) {

        $multiple_to_recipients = array(
            $mail,
        );

        $message = '<h1> Передзвоніть клієнту! </h1>';
        $message .= '<div><b>Номер:</b> ' . $_POST['phone'] . '</div>';


        add_filter( 'wp_mail_content_type', 'set_html_content_type' );

        wp_mail( $multiple_to_recipients, 'Передзвоніть клієнту!', $message );


        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

        function set_html_content_type() {
            return 'text/html';
        }


        print_r($_POST);
    }
    echo 1;
    wp_die();
}




add_action( 'wp_ajax_ourform', 'our_form' );
add_action('wp_ajax_nopriv_ourform', 'our_form');
function our_form(){
    global $wpdb;

    $mail =  $site_title = get_bloginfo( 'admin_email' );


    if (isset($_POST)) {

        $multiple_to_recipients = array(
            $mail,
        );

        $message  = '<h1> Заявка з сайту! ' . $_POST['title'] . '</h1>';
        $message .= '<div><b>Номер: </b> ' . $_POST['phone'] . '</div>';
        $message .= '<div><b>Ім\'я: </b> ' . $_POST['name'] . '</div>';
        $message .= '<div><b>Замовив: </b> ' . $_POST['title'] . '</div>';


        add_filter( 'wp_mail_content_type', 'set_html_content_type' );

        wp_mail( $multiple_to_recipients, 'Передзвоніть клієнту!', $message );


        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

        function set_html_content_type() {
            return 'text/html';
        }


        print_r($_POST);
    }
    echo 1;
    wp_die();
}