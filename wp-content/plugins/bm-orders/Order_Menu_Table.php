<?php
/**
 * Created by IntelliJ IDEA.
 * User: sonet
 * Date: 23.09.17
 * Time: 19:56
 */

class Order_Menu_Table extends WP_List_Table
{
	/**
	 * Подготавливаем колонки таблицы для их отображения
	 *
	 */
	public function prepare_items()
	{
		//Sets
		$per_page = 20;

		/* Получаем данные для формирования таблицы */
		$data = $this -> table_data();

		/* Устанавливаем данные для пагинации */
		$this -> set_pagination_args( array(
			'total_items' => count($data),
			'per_page'    => $per_page
		));

		/* Делим массив на части для пагинации */
		$data = array_slice(
			$data,
			(($this -> get_pagenum() - 1) * $per_page),
			$per_page
		);

		/* Устанавливаем данные колонок */
		$this -> _column_headers = array(
			$this -> get_columns(), /* Получаем массив названий колокнок */
			$this -> get_hidden_columns(), /* Получаем массив названий колонок которые нужно скрыть */
			$this -> get_sortable_columns() /* Получаем массив названий колонок которые можно сортировать */
		);

		/* Устанавливаем данные таблицы */
		$this -> items = $data;
	}

	/**
	 * Название колонок таблицы
	 *
	 * @return array
	 */
	public function get_columns()
	{
		return array(
			'cb' => '<input type="checkbox" />',
			'ID'			    => 'ID',

			'order_name'		=> 'ПІБ',
			'order_phone'		=> 'Телефон',
			'order_mail'        => 'Пошта',
			'order_total'       => 'Загальна сума',
			'status'		    => 'Статус',
			'order_date'		=> 'Створено',
			'edit'		        => 'Переглянути',

		);
	}

	function get_bulk_actions() {
		$actions = array(
			'delete'    => 'Видалити (напевно не варто давати цю можливість?)'
		);
		return $actions;
	}

	function column_cb($item) {
		return sprintf(
			'<input type="checkbox" name="book[]" value="%s" />',
			$this->_args['singular'],
			$item->id
		);
	}


	/**
	 * Массив колонок которые нужно скрыть
	 *
	 * @return array
	 */
	public function get_hidden_columns()
	{
		return array();
	}

	/**
	 * Массив названий колонок по которым выполняется сортировка
	 *
	 * @return array
	 */
	public function get_sortable_columns()
	{
		return array(
			'ID' => array('ID', false),
			'order_name' => array('order_name', true),
			'order_mail' => array('order_mail', false),
			'status' => array('order_status', false),
			'order_date' => array('order_date', false),
		);
	}

	/**
	 * Данные таблицы
	 *
	 * @return array
	 */

	private function table_data()
	{

		global $wpdb;
		$select = 'SELECT wp_orders.*, wp_order_status.title AS status FROM wp_orders
					LEFT JOIN wp_order_status ON wp_orders.order_status = wp_order_status.ID';

		if (isset($_GET['orderby'])){
			$select .= ' ORDER BY wp_orders.' . $_GET['orderby'];
			if(isset($_GET['order'])) $select .= ' '. $_GET['order'];
		}


		$data = $wpdb->get_results($select, ARRAY_A);
		return $data;
	}

	/**
	 * Возвращает содержимое колонки
	 *
	 * @param  array $item массив данных таблицы
	 * @param  string $column_name название текущей колонки
	 *
	 * @return mixed
	 */
	function column_order_total($item){
		return sprintf(
			'%s грн.',
			$item['order_total']
		);
	}
	function column_order_date($item)
	{
		return date(' F j, Y H:i', strtotime($item['order_date']));
	}
	function column_edit($item)
	{


	 return sprintf(
			'<input class="button action" value="Переглянути" type="button" @click="getItem(%s)">',
			 $item['ID']
		);

	}
	public function column_default($item, $column_name )
	{
		switch($column_name)
		{
			case 'ID':
			case 'order_name':
			case 'order_phone':
			case 'order_mail':
			case 'order_total':
			case 'status':
			case 'order_date':
			case 'edit':

				return $item[$column_name];
			default:
				return print_r($item, true);
		}
	}


}